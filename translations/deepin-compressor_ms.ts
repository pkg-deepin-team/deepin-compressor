<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.1" language="ms">
<context>
    <name>ArchiveModel</name>
    <message>
        <location filename="../deepin-compressor/source/src/archivemodel.cpp" line="119"/>
        <location filename="../deepin-compressor/source/src/archivemodel.cpp" line="121"/>
        <source>item(s)</source>
        <translation>item</translation>
    </message>
    <message>
        <location filename="../deepin-compressor/source/src/archivemodel.cpp" line="132"/>
        <source>yyyy/MM/dd hh:mm:ss</source>
        <translation>yyyy/MM/dd hh:mm:ss</translation>
    </message>
    <message>
        <location filename="../deepin-compressor/source/src/archivemodel.cpp" line="200"/>
        <source>Name</source>
        <translation>Nama</translation>
    </message>
    <message>
        <location filename="../deepin-compressor/source/src/archivemodel.cpp" line="202"/>
        <source>Size</source>
        <translation>Saiz</translation>
    </message>
    <message>
        <location filename="../deepin-compressor/source/src/archivemodel.cpp" line="204"/>
        <source>Type</source>
        <translation>Jenis</translation>
    </message>
    <message>
        <location filename="../deepin-compressor/source/src/archivemodel.cpp" line="206"/>
        <source>Time modified</source>
        <translation>Masa ubah suai</translation>
    </message>
</context>
<context>
    <name>CompressPage</name>
    <message>
        <location filename="../deepin-compressor/source/src/compresspage.cpp" line="48"/>
        <source>Next</source>
        <translation>Berikutnya</translation>
    </message>
    <message>
        <location filename="../deepin-compressor/source/src/compresspage.cpp" line="93"/>
        <source>Please add files</source>
        <translation>Sila tambah fail</translation>
    </message>
    <message>
        <location filename="../deepin-compressor/source/src/compresspage.cpp" line="94"/>
        <location filename="../deepin-compressor/source/src/compresspage.cpp" line="116"/>
        <source>OK</source>
        <translation>OK</translation>
    </message>
    <message>
        <location filename="../deepin-compressor/source/src/compresspage.cpp" line="118"/>
        <source>Please add files to the top-level directory</source>
        <translation type="unfinished">Sila tambah fail ke dalam direktori aras-tertinggi</translation>
    </message>
</context>
<context>
    <name>CompressSetting</name>
    <message>
        <location filename="../deepin-compressor/source/src/compresssetting.cpp" line="236"/>
        <source>Compress</source>
        <translation>Mampat</translation>
    </message>
    <message>
        <location filename="../deepin-compressor/source/src/compresssetting.cpp" line="164"/>
        <source>Advanced Options</source>
        <translation>Pilihan Lanjutan</translation>
    </message>
    <message>
        <location filename="../deepin-compressor/source/src/compresssetting.cpp" line="158"/>
        <source>Name</source>
        <translation>Nama</translation>
    </message>
    <message>
        <location filename="../deepin-compressor/source/src/compresssetting.cpp" line="159"/>
        <source>Save to</source>
        <translation>Simpan ke</translation>
    </message>
    <message>
        <location filename="../deepin-compressor/source/src/compresssetting.cpp" line="170"/>
        <source>Encrypt the archive</source>
        <translation>Sulitkan arkib</translation>
    </message>
    <message>
        <location filename="../deepin-compressor/source/src/compresssetting.cpp" line="171"/>
        <source>Support zip, 7z type only</source>
        <translation>Sokong jenis zip, 7z sahaja</translation>
    </message>
    <message>
        <location filename="../deepin-compressor/source/src/compresssetting.cpp" line="176"/>
        <source>Password</source>
        <translation>Kata laluan</translation>
    </message>
    <message>
        <location filename="../deepin-compressor/source/src/compresssetting.cpp" line="178"/>
        <source>Encrypt the file list too</source>
        <translation>Sulitkan senarai fail juga</translation>
    </message>
    <message>
        <location filename="../deepin-compressor/source/src/compresssetting.cpp" line="179"/>
        <location filename="../deepin-compressor/source/src/compresssetting.cpp" line="187"/>
        <source>Support 7z type only</source>
        <translation>Sokong jenis 7z sahaja</translation>
    </message>
    <message>
        <location filename="../deepin-compressor/source/src/compresssetting.cpp" line="185"/>
        <source>Split to volumes</source>
        <translation>Pisah mengikut volum</translation>
    </message>
    <message>
        <location filename="../deepin-compressor/source/src/compresssetting.cpp" line="335"/>
        <location filename="../deepin-compressor/source/src/compresssetting.cpp" line="830"/>
        <location filename="../deepin-compressor/source/src/compresssetting.cpp" line="966"/>
        <location filename="../deepin-compressor/source/src/compresssetting.cpp" line="1099"/>
        <source>%1 was changed on the disk, please import it again.</source>
        <translation type="unfinished">%1 telah berubah dalam cakera, sila import ia sekali lagi.</translation>
    </message>
    <message>
        <location filename="../deepin-compressor/source/src/compresssetting.cpp" line="353"/>
        <location filename="../deepin-compressor/source/src/compresssetting.cpp" line="359"/>
        <location filename="../deepin-compressor/source/src/compresssetting.cpp" line="364"/>
        <location filename="../deepin-compressor/source/src/compresssetting.cpp" line="837"/>
        <location filename="../deepin-compressor/source/src/compresssetting.cpp" line="843"/>
        <location filename="../deepin-compressor/source/src/compresssetting.cpp" line="848"/>
        <location filename="../deepin-compressor/source/src/compresssetting.cpp" line="973"/>
        <location filename="../deepin-compressor/source/src/compresssetting.cpp" line="979"/>
        <location filename="../deepin-compressor/source/src/compresssetting.cpp" line="984"/>
        <location filename="../deepin-compressor/source/src/compresssetting.cpp" line="1106"/>
        <location filename="../deepin-compressor/source/src/compresssetting.cpp" line="1112"/>
        <location filename="../deepin-compressor/source/src/compresssetting.cpp" line="1117"/>
        <source>You do not have permission to compress %1</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../deepin-compressor/source/src/compresssetting.cpp" line="373"/>
        <location filename="../deepin-compressor/source/src/compresssetting.cpp" line="857"/>
        <location filename="../deepin-compressor/source/src/compresssetting.cpp" line="993"/>
        <source>You do not have permission to save files here, please change and retry</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../deepin-compressor/source/src/compresssetting.cpp" line="378"/>
        <source>Too many volumes, please change and retry</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../deepin-compressor/source/src/compresssetting.cpp" line="478"/>
        <source>Files that begin with &apos;@&apos; cannot be compressed</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../deepin-compressor/source/src/compresssetting.cpp" line="702"/>
        <source>Total size: %1</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../deepin-compressor/source/src/compresssetting.cpp" line="801"/>
        <location filename="../deepin-compressor/source/src/compresssetting.cpp" line="937"/>
        <location filename="../deepin-compressor/source/src/compresssetting.cpp" line="1070"/>
        <source>You cannot add the archive to itself</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../deepin-compressor/source/src/compresssetting.cpp" line="1279"/>
        <source>Another file with the same name already exists, replace it?</source>
        <translation type="unfinished">Ada fail lain dengan nama yang serupa telah wujud, gantikannya?</translation>
    </message>
    <message>
        <location filename="../deepin-compressor/source/src/compresssetting.cpp" line="800"/>
        <source>Close</source>
        <translation type="unfinished">Tutup</translation>
    </message>
    <message>
        <location filename="../deepin-compressor/source/src/compresssetting.cpp" line="936"/>
        <location filename="../deepin-compressor/source/src/compresssetting.cpp" line="1203"/>
        <source>OK</source>
        <translation>OK</translation>
    </message>
    <message>
        <location filename="../deepin-compressor/source/src/compresssetting.cpp" line="1281"/>
        <source>Cancel</source>
        <translation type="unfinished">Batal</translation>
    </message>
    <message>
        <location filename="../deepin-compressor/source/src/compresssetting.cpp" line="1282"/>
        <source>Replace</source>
        <translation type="unfinished">Ganti</translation>
    </message>
    <message>
        <location filename="../deepin-compressor/source/src/compresssetting.cpp" line="322"/>
        <location filename="../deepin-compressor/source/src/compresssetting.cpp" line="817"/>
        <location filename="../deepin-compressor/source/src/compresssetting.cpp" line="953"/>
        <location filename="../deepin-compressor/source/src/compresssetting.cpp" line="1086"/>
        <source>Please enter the path</source>
        <translation>Sila masukkan laluan</translation>
    </message>
    <message>
        <location filename="../deepin-compressor/source/src/compresssetting.cpp" line="149"/>
        <source>New Archive</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../deepin-compressor/source/src/compresssetting.cpp" line="317"/>
        <location filename="../deepin-compressor/source/src/compresssetting.cpp" line="812"/>
        <location filename="../deepin-compressor/source/src/compresssetting.cpp" line="948"/>
        <location filename="../deepin-compressor/source/src/compresssetting.cpp" line="1081"/>
        <source>Invalid file name</source>
        <translation>Nama fail tidak sah</translation>
    </message>
    <message>
        <location filename="../deepin-compressor/source/src/compresssetting.cpp" line="327"/>
        <location filename="../deepin-compressor/source/src/compresssetting.cpp" line="822"/>
        <location filename="../deepin-compressor/source/src/compresssetting.cpp" line="958"/>
        <location filename="../deepin-compressor/source/src/compresssetting.cpp" line="1091"/>
        <source>The path does not exist, please retry</source>
        <translation>Laluan tidak wujud, cuba sekali lagi</translation>
    </message>
</context>
<context>
    <name>Compressor_Fail</name>
    <message>
        <location filename="../deepin-compressor/source/src/compressor_fail.cpp" line="35"/>
        <source>Extraction failed</source>
        <translation>Pengekstrakan gagal</translation>
    </message>
    <message>
        <location filename="../deepin-compressor/source/src/compressor_fail.cpp" line="36"/>
        <source>Damaged file, unable to extract</source>
        <translation>Fail rosak, tidak boleh diekstrak</translation>
    </message>
    <message>
        <location filename="../deepin-compressor/source/src/compressor_fail.cpp" line="61"/>
        <source>Retry</source>
        <translation>Cuba lagi</translation>
    </message>
    <message>
        <location filename="../deepin-compressor/source/src/compressor_fail.cpp" line="63"/>
        <source>Back</source>
        <translation>Undur</translation>
    </message>
</context>
<context>
    <name>Compressor_Success</name>
    <message>
        <location filename="../deepin-compressor/source/src/compressor_success.cpp" line="40"/>
        <source>Compression successful</source>
        <translation>Pemampatan berjaya</translation>
    </message>
    <message>
        <location filename="../deepin-compressor/source/src/compressor_success.cpp" line="58"/>
        <source>View</source>
        <translation>Lihat</translation>
    </message>
    <message>
        <location filename="../deepin-compressor/source/src/compressor_success.cpp" line="60"/>
        <source>Back</source>
        <translation>Undur</translation>
    </message>
</context>
<context>
    <name>EncodingPage</name>
    <message>
        <location filename="../deepin-compressor/source/src/encodingpage.cpp" line="65"/>
        <source>Filename Encoding</source>
        <translation>Pengekodan Nama Fail</translation>
    </message>
    <message>
        <location filename="../deepin-compressor/source/src/encodingpage.cpp" line="78"/>
        <source>Select an encoding to continue</source>
        <translation>Pilih satu pengekodan untuk teruskan</translation>
    </message>
    <message>
        <location filename="../deepin-compressor/source/src/encodingpage.cpp" line="82"/>
        <source>Cancel</source>
        <translation>Batal</translation>
    </message>
    <message>
        <location filename="../deepin-compressor/source/src/encodingpage.cpp" line="83"/>
        <source>Select Character Encoding</source>
        <translation>Pilih Pengekodan Aksara</translation>
    </message>
</context>
<context>
    <name>EncryptionPage</name>
    <message>
        <location filename="../deepin-compressor/source/src/encryptionpage.cpp" line="47"/>
        <source>Encrypted file, please enter the password</source>
        <translation>Fail disulitkan, sila masukkan kata laluan</translation>
    </message>
    <message>
        <location filename="../deepin-compressor/source/src/encryptionpage.cpp" line="50"/>
        <source>Next</source>
        <translation>Berikutnya</translation>
    </message>
    <message>
        <location filename="../deepin-compressor/source/src/encryptionpage.cpp" line="55"/>
        <source>Password</source>
        <translation>Kata laluan</translation>
    </message>
    <message>
        <location filename="../deepin-compressor/source/src/encryptionpage.cpp" line="135"/>
        <source>Wrong password</source>
        <translation>Kata laluan salah</translation>
    </message>
</context>
<context>
    <name>ExtractPauseDialog</name>
    <message>
        <location filename="../deepin-compressor/source/src/extractpausedialog.cpp" line="67"/>
        <source>Are you sure you want to stop the extraction?</source>
        <translation>Anda pasti mahu menghentikan pengekstrakan?</translation>
    </message>
    <message>
        <location filename="../deepin-compressor/source/src/extractpausedialog.cpp" line="69"/>
        <source>Cancel</source>
        <translation>Batal</translation>
    </message>
    <message>
        <location filename="../deepin-compressor/source/src/extractpausedialog.cpp" line="70"/>
        <source>Confirm</source>
        <translation>Sah</translation>
    </message>
</context>
<context>
    <name>HomePage</name>
    <message>
        <location filename="../deepin-compressor/source/src/homepage.cpp" line="40"/>
        <source>Drag file or folder here</source>
        <translation>Seret fail atau folder di sini</translation>
    </message>
    <message>
        <location filename="../deepin-compressor/source/src/homepage.cpp" line="42"/>
        <source>Select File</source>
        <translation>Pilih Fail</translation>
    </message>
</context>
<context>
    <name>LogViewHeaderView</name>
    <message>
        <location filename="../deepin-compressor/source/src/logviewheaderview.cpp" line="160"/>
        <source>Back</source>
        <translation>Undur</translation>
    </message>
</context>
<context>
    <name>Main</name>
    <message>
        <location filename="../deepin-compressor/main.cpp" line="145"/>
        <location filename="../tests/UnitTest/deepin-compressor/source/src/test_main.cpp" line="150"/>
        <source>Archive Manager</source>
        <translation>Pengurus Arkib</translation>
    </message>
    <message>
        <location filename="../deepin-compressor/main.cpp" line="146"/>
        <location filename="../tests/UnitTest/deepin-compressor/source/src/test_main.cpp" line="151"/>
        <source>Archive Manager is a fast and lightweight application for creating and extracting archives.</source>
        <translation>Pengursu Arkib ialah sebuah aplikasi yang ringan dan pantas untuk mencipta dan mengekstrak arkib.</translation>
    </message>
</context>
<context>
    <name>MainWindow</name>
    <message>
        <location filename="../deepin-compressor/source/src/mainwindow.cpp" line="564"/>
        <source>Close</source>
        <translation>Tutup</translation>
    </message>
    <message>
        <location filename="../deepin-compressor/source/src/mainwindow.cpp" line="568"/>
        <source>Help</source>
        <translation>Bantuan</translation>
    </message>
    <message>
        <location filename="../deepin-compressor/source/src/mainwindow.cpp" line="572"/>
        <source>Select the file</source>
        <translation>Buka</translation>
    </message>
    <message>
        <location filename="../deepin-compressor/source/src/mainwindow.cpp" line="576"/>
        <source>Delete</source>
        <translation>Padam</translation>
    </message>
    <message>
        <location filename="../deepin-compressor/source/src/mainwindow.cpp" line="596"/>
        <source>Shortcuts</source>
        <translation>Pintasan</translation>
    </message>
    <message>
        <location filename="../deepin-compressor/source/src/mainwindow.cpp" line="781"/>
        <source>Open file</source>
        <translation>Buka fail</translation>
    </message>
    <message>
        <location filename="../deepin-compressor/source/src/mainwindow.cpp" line="822"/>
        <source>Settings</source>
        <translation>Tetapan</translation>
    </message>
    <message>
        <location filename="../deepin-compressor/source/src/mainwindow.cpp" line="1284"/>
        <location filename="../deepin-compressor/source/src/mainwindow.cpp" line="1297"/>
        <location filename="../deepin-compressor/source/src/mainwindow.cpp" line="2961"/>
        <source>Create New Archive</source>
        <translation>Cipta Arkib Baharu</translation>
    </message>
    <message>
        <location filename="../deepin-compressor/source/src/mainwindow.cpp" line="1338"/>
        <source>Opening</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../deepin-compressor/source/src/mainwindow.cpp" line="1366"/>
        <source>Converting</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../deepin-compressor/source/src/mainwindow.cpp" line="1375"/>
        <source>Compression successful</source>
        <translation>Pemampatan berjaya</translation>
    </message>
    <message>
        <location filename="../deepin-compressor/source/src/mainwindow.cpp" line="5119"/>
        <source>Confirm</source>
        <translation type="unfinished">Sah</translation>
    </message>
    <message>
        <location filename="../deepin-compressor/source/src/mainwindow.cpp" line="5122"/>
        <source>Do you want to delete the archive?</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../deepin-compressor/source/src/mainwindow.cpp" line="2573"/>
        <location filename="../deepin-compressor/source/src/mainwindow.cpp" line="2669"/>
        <location filename="../deepin-compressor/source/src/mainwindow.cpp" line="2703"/>
        <source>Skip all files</source>
        <translation>Langkau semua fail</translation>
    </message>
    <message>
        <location filename="../deepin-compressor/source/src/mainwindow.cpp" line="2546"/>
        <location filename="../deepin-compressor/source/src/mainwindow.cpp" line="2705"/>
        <source>Extraction successful</source>
        <translation>Pengekstrakan berjaya</translation>
    </message>
    <message>
        <location filename="../deepin-compressor/source/src/mainwindow.cpp" line="469"/>
        <source>%1 was changed on the disk, please import it again.</source>
        <translation>%1 telah berubah dalam cakera, sila import ia sekali lagi.</translation>
    </message>
    <message>
        <location filename="../deepin-compressor/source/src/mainwindow.cpp" line="1631"/>
        <source>Do you want to add the archive to the list or open it in new window?</source>
        <translation>Anda mahu menambah arkib ke dalam senarai atau buka ia dengan tetingkap baharu?</translation>
    </message>
    <message>
        <location filename="../deepin-compressor/source/src/mainwindow.cpp" line="584"/>
        <source>Display shortcuts</source>
        <translation>Papar pintasan</translation>
    </message>
    <message>
        <location filename="../deepin-compressor/source/src/mainwindow.cpp" line="242"/>
        <location filename="../deepin-compressor/source/src/mainwindow.cpp" line="1632"/>
        <location filename="../deepin-compressor/source/src/mainwindow.cpp" line="5118"/>
        <source>Cancel</source>
        <translation>Batal</translation>
    </message>
    <message>
        <location filename="../deepin-compressor/source/src/mainwindow.cpp" line="114"/>
        <source>Archive Manager</source>
        <translation>Pengurus Arkib</translation>
    </message>
    <message>
        <location filename="../deepin-compressor/source/src/mainwindow.cpp" line="475"/>
        <location filename="../deepin-compressor/source/src/mainwindow.cpp" line="2246"/>
        <location filename="../deepin-compressor/source/src/mainwindow.cpp" line="4916"/>
        <source>OK</source>
        <translation>OK</translation>
    </message>
    <message>
        <location filename="../deepin-compressor/source/src/mainwindow.cpp" line="1316"/>
        <source>Adding files to %1</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../deepin-compressor/source/src/mainwindow.cpp" line="1319"/>
        <source>Compressing</source>
        <translation>Memampat</translation>
    </message>
    <message>
        <location filename="../deepin-compressor/source/src/mainwindow.cpp" line="1340"/>
        <source>Extracting</source>
        <translation>Mengekstrak</translation>
    </message>
    <message>
        <location filename="../deepin-compressor/source/src/mainwindow.cpp" line="1354"/>
        <source>Deleting</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../deepin-compressor/source/src/mainwindow.cpp" line="1392"/>
        <source>Compression failed</source>
        <translation>Pemampatan gagal</translation>
    </message>
    <message>
        <location filename="../deepin-compressor/source/src/mainwindow.cpp" line="1424"/>
        <source>Extraction failed</source>
        <translation>Pengekstrakan gagal</translation>
    </message>
    <message>
        <location filename="../deepin-compressor/source/src/mainwindow.cpp" line="1437"/>
        <source>Conversion successful</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../deepin-compressor/source/src/mainwindow.cpp" line="1633"/>
        <source>Add</source>
        <translation>Tambah</translation>
    </message>
    <message>
        <location filename="../deepin-compressor/source/src/mainwindow.cpp" line="1634"/>
        <source>Open in new window</source>
        <translation>Buka dalam tetingkap baharu</translation>
    </message>
    <message>
        <location filename="../deepin-compressor/source/src/mainwindow.cpp" line="1841"/>
        <source>Find directory</source>
        <translation>Cari direktori</translation>
    </message>
    <message>
        <location filename="../deepin-compressor/source/src/mainwindow.cpp" line="2167"/>
        <location filename="../deepin-compressor/source/src/mainwindow.cpp" line="2598"/>
        <source>Failed to open the archive: %1</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../deepin-compressor/source/src/mainwindow.cpp" line="2481"/>
        <location filename="../deepin-compressor/source/src/mainwindow.cpp" line="2600"/>
        <source>Wrong password</source>
        <translation>Kata laluan salah</translation>
    </message>
    <message>
        <location filename="../deepin-compressor/source/src/mainwindow.cpp" line="2687"/>
        <source>Select default program</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../deepin-compressor/source/src/mainwindow.cpp" line="4918"/>
        <source>Please check the file association type in the settings of Archive Manager</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../deepin-compressor/source/src/mainwindow.cpp" line="1982"/>
        <location filename="../deepin-compressor/source/src/mainwindow.cpp" line="2005"/>
        <location filename="../deepin-compressor/source/src/mainwindow.cpp" line="2173"/>
        <location filename="../deepin-compressor/source/src/mainwindow.cpp" line="2593"/>
        <source>Damaged file, unable to extract</source>
        <translation>Fail rosak, tidak boleh diekstrak</translation>
    </message>
    <message>
        <location filename="../deepin-compressor/source/src/mainwindow.cpp" line="241"/>
        <source>Are you sure you want to stop the ongoing task?</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../deepin-compressor/source/src/mainwindow.cpp" line="2245"/>
        <source>The archive was changed on the disk, please import it again.</source>
        <translation>Arkib ini telah berubah dalam cakera, sila import ia sekali lagi.</translation>
    </message>
    <message>
        <location filename="../deepin-compressor/source/src/mainwindow.cpp" line="2589"/>
        <location filename="../deepin-compressor/source/src/mainwindow.cpp" line="2591"/>
        <location filename="../deepin-compressor/source/src/mainwindow.cpp" line="4028"/>
        <location filename="../deepin-compressor/source/src/mainwindow.cpp" line="4034"/>
        <location filename="../deepin-compressor/source/src/mainwindow.cpp" line="4085"/>
        <location filename="../deepin-compressor/source/src/mainwindow.cpp" line="4091"/>
        <source>Insufficient space, please clear and retry</source>
        <translation>Ruang tidak mencukupi, sila kosongkan dan cuba lagi</translation>
    </message>
    <message>
        <location filename="../deepin-compressor/source/src/mainwindow.cpp" line="2596"/>
        <source>File name too long, unable to extract</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../deepin-compressor/source/src/mainwindow.cpp" line="2600"/>
        <source>Unable to extract</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../deepin-compressor/source/src/mainwindow.cpp" line="4030"/>
        <location filename="../deepin-compressor/source/src/mainwindow.cpp" line="4036"/>
        <location filename="../deepin-compressor/source/src/mainwindow.cpp" line="4087"/>
        <location filename="../deepin-compressor/source/src/mainwindow.cpp" line="4093"/>
        <source>Damaged file</source>
        <translation>Fail rosak</translation>
    </message>
</context>
<context>
    <name>MimeTypeDisplayManager</name>
    <message>
        <location filename="../deepin-compressor/source/src/mimetypedisplaymanager.cpp" line="46"/>
        <source>Directory</source>
        <translation>Direktori</translation>
    </message>
    <message>
        <location filename="../deepin-compressor/source/src/mimetypedisplaymanager.cpp" line="47"/>
        <source>Application</source>
        <translation>Aplikasi</translation>
    </message>
    <message>
        <location filename="../deepin-compressor/source/src/mimetypedisplaymanager.cpp" line="48"/>
        <source>Video</source>
        <translation>Video</translation>
    </message>
    <message>
        <location filename="../deepin-compressor/source/src/mimetypedisplaymanager.cpp" line="49"/>
        <source>Audio</source>
        <translation>Audio</translation>
    </message>
    <message>
        <location filename="../deepin-compressor/source/src/mimetypedisplaymanager.cpp" line="50"/>
        <source>Image</source>
        <translation>Imej</translation>
    </message>
    <message>
        <location filename="../deepin-compressor/source/src/mimetypedisplaymanager.cpp" line="51"/>
        <source>Archive</source>
        <translation>Arkib</translation>
    </message>
    <message>
        <location filename="../deepin-compressor/source/src/mimetypedisplaymanager.cpp" line="53"/>
        <source>Executable</source>
        <translation>Bolehlaku</translation>
    </message>
    <message>
        <location filename="../deepin-compressor/source/src/mimetypedisplaymanager.cpp" line="52"/>
        <source>Document</source>
        <translation>Dokumen</translation>
    </message>
    <message>
        <location filename="../deepin-compressor/source/src/mimetypedisplaymanager.cpp" line="54"/>
        <source>Backup file</source>
        <translation>Fail sandar</translation>
    </message>
    <message>
        <location filename="../deepin-compressor/source/src/mimetypedisplaymanager.cpp" line="55"/>
        <source>Unknown</source>
        <translation>Tidak diketahui</translation>
    </message>
</context>
<context>
    <name>MyFileSystemModel</name>
    <message>
        <location filename="../deepin-compressor/source/src/myfilesystemmodel.cpp" line="63"/>
        <source>Name</source>
        <translation>Nama</translation>
    </message>
    <message>
        <location filename="../deepin-compressor/source/src/myfilesystemmodel.cpp" line="65"/>
        <source>Size</source>
        <translation>Saiz</translation>
    </message>
    <message>
        <location filename="../deepin-compressor/source/src/myfilesystemmodel.cpp" line="67"/>
        <source>Type</source>
        <translation>Jenis</translation>
    </message>
    <message>
        <location filename="../deepin-compressor/source/src/myfilesystemmodel.cpp" line="69"/>
        <source>Time modified</source>
        <translation>Masa ubah suai</translation>
    </message>
    <message>
        <location filename="../deepin-compressor/source/src/myfilesystemmodel.cpp" line="127"/>
        <source>item(s)</source>
        <translation>item</translation>
    </message>
    <message>
        <location filename="../deepin-compressor/source/src/myfilesystemmodel.cpp" line="138"/>
        <source>yyyy/MM/dd hh:mm:ss</source>
        <translation>yyyy/MM/dd hh:mm:ss</translation>
    </message>
</context>
<context>
    <name>OpenLoadingPage</name>
    <message>
        <location filename="../deepin-compressor/source/src/openloadingpage.cpp" line="52"/>
        <source>Loading, please wait...</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>OpenWithDialog</name>
    <message>
        <location filename="../deepin-compressor/source/src/openwithdialog/openwithdialog.cpp" line="308"/>
        <source>Open with</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../deepin-compressor/source/src/openwithdialog/openwithdialog.cpp" line="327"/>
        <source>Add other programs</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../deepin-compressor/source/src/openwithdialog/openwithdialog.cpp" line="328"/>
        <source>Set as default</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../deepin-compressor/source/src/openwithdialog/openwithdialog.cpp" line="330"/>
        <source>Cancel</source>
        <translation type="unfinished">Batal</translation>
    </message>
    <message>
        <location filename="../deepin-compressor/source/src/openwithdialog/openwithdialog.cpp" line="331"/>
        <source>Confirm</source>
        <translation type="unfinished">Sah</translation>
    </message>
    <message>
        <location filename="../deepin-compressor/source/src/openwithdialog/openwithdialog.cpp" line="335"/>
        <source>Recommended Applications</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../deepin-compressor/source/src/openwithdialog/openwithdialog.cpp" line="337"/>
        <source>Other Applications</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>PasswordNeededQuery</name>
    <message>
        <location filename="../3rdparty/interface/queries.cpp" line="359"/>
        <source>Encrypted file, please enter the password</source>
        <translation type="unfinished">Fail disulitkan, sila masukkan kata laluan</translation>
    </message>
</context>
<context>
    <name>Progress</name>
    <message>
        <location filename="../deepin-compressor/source/src/progress.cpp" line="89"/>
        <location filename="../deepin-compressor/source/src/progress.cpp" line="415"/>
        <source>Cancel</source>
        <translation>Batal</translation>
    </message>
    <message>
        <location filename="../deepin-compressor/source/src/progress.cpp" line="85"/>
        <location filename="../deepin-compressor/source/src/progress.cpp" line="148"/>
        <location filename="../deepin-compressor/source/src/progress.cpp" line="150"/>
        <location filename="../deepin-compressor/source/src/progress.cpp" line="152"/>
        <location filename="../deepin-compressor/source/src/progress.cpp" line="154"/>
        <location filename="../deepin-compressor/source/src/progress.cpp" line="156"/>
        <location filename="../deepin-compressor/source/src/progress.cpp" line="159"/>
        <location filename="../deepin-compressor/source/src/progress.cpp" line="442"/>
        <source>Calculating...</source>
        <translation>Mengira...</translation>
    </message>
    <message>
        <location filename="../deepin-compressor/source/src/progress.cpp" line="93"/>
        <location filename="../deepin-compressor/source/src/progress.cpp" line="191"/>
        <location filename="../deepin-compressor/source/src/progress.cpp" line="468"/>
        <source>Pause</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../deepin-compressor/source/src/progress.cpp" line="152"/>
        <location filename="../deepin-compressor/source/src/progress.cpp" line="275"/>
        <location filename="../deepin-compressor/source/src/progress.cpp" line="277"/>
        <source>Speed</source>
        <comment>delete</comment>
        <translation type="unfinished">Kelajuan</translation>
    </message>
    <message>
        <location filename="../deepin-compressor/source/src/progress.cpp" line="156"/>
        <location filename="../deepin-compressor/source/src/progress.cpp" line="299"/>
        <location filename="../deepin-compressor/source/src/progress.cpp" line="301"/>
        <location filename="../deepin-compressor/source/src/progress.cpp" line="303"/>
        <source>Speed</source>
        <comment>convert</comment>
        <translation type="unfinished">Kelajuan</translation>
    </message>
    <message>
        <location filename="../deepin-compressor/source/src/progress.cpp" line="403"/>
        <source>Are you sure you want to stop the update?</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../deepin-compressor/source/src/progress.cpp" line="405"/>
        <source>Are you sure you want to stop the conversion?</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../deepin-compressor/source/src/progress.cpp" line="342"/>
        <source>Converting</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../deepin-compressor/source/src/progress.cpp" line="345"/>
        <source>Opening</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../deepin-compressor/source/src/progress.cpp" line="401"/>
        <source>Are you sure you want to stop the compression?</source>
        <translation>Anda pasti mahu menghentikan pemampatan?</translation>
    </message>
    <message>
        <location filename="../deepin-compressor/source/src/progress.cpp" line="416"/>
        <source>Confirm</source>
        <translation>Sah</translation>
    </message>
    <message>
        <location filename="../deepin-compressor/source/src/progress.cpp" line="462"/>
        <source>Continue</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../deepin-compressor/source/src/progress.cpp" line="411"/>
        <source>Are you sure you want to stop the extraction?</source>
        <translation>Anda pasti mahu menghentikan pengekstrakan?</translation>
    </message>
    <message>
        <location filename="../deepin-compressor/source/src/progress.cpp" line="148"/>
        <location filename="../deepin-compressor/source/src/progress.cpp" line="154"/>
        <location filename="../deepin-compressor/source/src/progress.cpp" line="267"/>
        <location filename="../deepin-compressor/source/src/progress.cpp" line="269"/>
        <location filename="../deepin-compressor/source/src/progress.cpp" line="271"/>
        <location filename="../deepin-compressor/source/src/progress.cpp" line="283"/>
        <location filename="../deepin-compressor/source/src/progress.cpp" line="285"/>
        <location filename="../deepin-compressor/source/src/progress.cpp" line="287"/>
        <source>Speed</source>
        <comment>compress</comment>
        <translation>Kelajuan</translation>
    </message>
    <message>
        <location filename="../deepin-compressor/source/src/progress.cpp" line="159"/>
        <location filename="../deepin-compressor/source/src/progress.cpp" line="307"/>
        <source>Time left</source>
        <translation>Masa berbaki</translation>
    </message>
    <message>
        <location filename="../deepin-compressor/source/src/progress.cpp" line="150"/>
        <location filename="../deepin-compressor/source/src/progress.cpp" line="291"/>
        <location filename="../deepin-compressor/source/src/progress.cpp" line="293"/>
        <location filename="../deepin-compressor/source/src/progress.cpp" line="295"/>
        <source>Speed</source>
        <comment>uncompress</comment>
        <translation>Kelajuan</translation>
    </message>
    <message>
        <location filename="../deepin-compressor/source/src/progress.cpp" line="338"/>
        <source>Compressing</source>
        <translation>Memampat</translation>
    </message>
    <message>
        <location filename="../deepin-compressor/source/src/progress.cpp" line="340"/>
        <source>Deleting</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../deepin-compressor/source/src/progress.cpp" line="347"/>
        <source>Extracting</source>
        <translation>Mengekstrak</translation>
    </message>
</context>
<context>
    <name>ProgressDialog</name>
    <message>
        <location filename="../deepin-compressor/source/src/progressdialog.cpp" line="50"/>
        <source>%1 task(s) in progress</source>
        <translation>%1 tugas masih berlansung</translation>
    </message>
    <message>
        <location filename="../deepin-compressor/source/src/progressdialog.cpp" line="62"/>
        <location filename="../deepin-compressor/source/src/progressdialog.cpp" line="115"/>
        <source>Task</source>
        <translation>Tugas</translation>
    </message>
    <message>
        <location filename="../deepin-compressor/source/src/progressdialog.cpp" line="63"/>
        <location filename="../deepin-compressor/source/src/progressdialog.cpp" line="121"/>
        <location filename="../deepin-compressor/source/src/progressdialog.cpp" line="140"/>
        <source>Extracting</source>
        <translation>Mengekstrak</translation>
    </message>
    <message>
        <location filename="../deepin-compressor/source/src/progressdialog.cpp" line="136"/>
        <source>Extraction successful</source>
        <translation>Pengekstrakan berjaya</translation>
    </message>
    <message>
        <location filename="../deepin-compressor/source/src/progressdialog.cpp" line="136"/>
        <source>Extract to</source>
        <translation>Ekstrak ke</translation>
    </message>
    <message>
        <location filename="../deepin-compressor/source/src/progressdialog.cpp" line="141"/>
        <source>Extraction successful</source>
        <comment>progressdialog</comment>
        <translation type="unfinished">Pengekstrakan berjaya</translation>
    </message>
</context>
<context>
    <name>QInstaller</name>
    <message>
        <location filename="../deepin-compressor/source/src/utils.cpp" line="178"/>
        <source>B</source>
        <translation>B</translation>
    </message>
    <message>
        <location filename="../deepin-compressor/source/src/utils.cpp" line="179"/>
        <source>KB</source>
        <translation>KB</translation>
    </message>
    <message>
        <location filename="../deepin-compressor/source/src/utils.cpp" line="180"/>
        <source>MB</source>
        <translation>MB</translation>
    </message>
    <message>
        <location filename="../deepin-compressor/source/src/utils.cpp" line="181"/>
        <source>GB</source>
        <translation>GB</translation>
    </message>
    <message>
        <location filename="../deepin-compressor/source/src/utils.cpp" line="182"/>
        <source>TB</source>
        <translation>TB</translation>
    </message>
    <message>
        <location filename="../deepin-compressor/source/src/utils.cpp" line="183"/>
        <source>PB</source>
        <translation>PB</translation>
    </message>
    <message>
        <location filename="../deepin-compressor/source/src/utils.cpp" line="184"/>
        <source>EB</source>
        <translation>EB</translation>
    </message>
    <message>
        <location filename="../deepin-compressor/source/src/utils.cpp" line="185"/>
        <source>ZB</source>
        <translation>ZB</translation>
    </message>
    <message>
        <location filename="../deepin-compressor/source/src/utils.cpp" line="186"/>
        <source>YB</source>
        <translation>YB</translation>
    </message>
</context>
<context>
    <name>QObject</name>
    <message>
        <location filename="../deepin-compressor/source/src/fileViewer.cpp" line="640"/>
        <source>Name</source>
        <translation>Nama</translation>
    </message>
    <message>
        <location filename="../deepin-compressor/source/src/fileViewer.cpp" line="644"/>
        <source>Time modified</source>
        <translation>Masa ubah suai</translation>
    </message>
    <message>
        <location filename="../deepin-compressor/source/src/fileViewer.cpp" line="648"/>
        <source>Type</source>
        <translation>Jenis</translation>
    </message>
    <message>
        <location filename="../deepin-compressor/source/src/fileViewer.cpp" line="652"/>
        <source>Size</source>
        <translation>Saiz</translation>
    </message>
    <message>
        <location filename="../deepin-compressor/source/src/fileViewer.cpp" line="1555"/>
        <source>Update</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../deepin-compressor/source/src/settings_translation.cpp" line="5"/>
        <source>General</source>
        <translation>Am</translation>
    </message>
    <message>
        <location filename="../deepin-compressor/source/src/settings_translation.cpp" line="6"/>
        <source>Extraction</source>
        <translation>Pengekstrakan</translation>
    </message>
    <message>
        <location filename="../deepin-compressor/source/src/settings_translation.cpp" line="7"/>
        <source>Auto create a folder for multiple extracted files</source>
        <translation>Auto cipta satu folder untuk fail teresktrak berbilang</translation>
    </message>
    <message>
        <location filename="../deepin-compressor/source/src/settings_translation.cpp" line="8"/>
        <source>Show extracted files when completed</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../deepin-compressor/source/src/settings_translation.cpp" line="9"/>
        <source>File Management</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../deepin-compressor/source/src/settings_translation.cpp" line="10"/>
        <source>Delete files after compression</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../deepin-compressor/source/src/settings_translation.cpp" line="11"/>
        <source>Files Associated</source>
        <translation>Fail-fail Berkaitan</translation>
    </message>
    <message>
        <location filename="../deepin-compressor/source/src/settings_translation.cpp" line="12"/>
        <source>File Type</source>
        <translation>Jenis Fail</translation>
    </message>
    <message>
        <location filename="../deepin-compressor/source/src/myfileitem.cpp" line="50"/>
        <location filename="../deepin-compressor/source/src/myfileitem.cpp" line="51"/>
        <location filename="../deepin-compressor/source/src/myfileitem.cpp" line="53"/>
        <location filename="../deepin-compressor/source/src/myfileitem.cpp" line="54"/>
        <location filename="../deepin-compressor/source/src/myfileitem.cpp" line="56"/>
        <location filename="../deepin-compressor/source/src/myfileitem.cpp" line="57"/>
        <location filename="../deepin-compressor/source/src/myfileitem.cpp" line="59"/>
        <location filename="../deepin-compressor/source/src/myfileitem.cpp" line="61"/>
        <source>item(s)</source>
        <translation>item</translation>
    </message>
    <message>
        <location filename="../deepin-compressor/source/src/mainwindow.cpp" line="243"/>
        <source>Confirm</source>
        <translation type="unfinished">Sah</translation>
    </message>
    <message>
        <location filename="../deepin-compressor/source/src/mainwindow.cpp" line="715"/>
        <source>%1 changed. Do you want to save changes to the archive?</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../deepin-compressor/source/src/mainwindow.cpp" line="729"/>
        <source>Discard</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../deepin-compressor/source/src/fileViewer.cpp" line="1554"/>
        <source>Cancel</source>
        <translation type="unfinished">Batal</translation>
    </message>
    <message>
        <location filename="../deepin-compressor/source/src/mainwindow.cpp" line="730"/>
        <source>Save</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../3rdparty/interface/queries.cpp" line="188"/>
        <source>Another file with the same name already exists, replace it?</source>
        <translation type="unfinished">Ada fail lain dengan nama yang serupa telah wujud, gantikannya?</translation>
    </message>
    <message>
        <location filename="../3rdparty/interface/queries.cpp" line="190"/>
        <source>Skip</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../3rdparty/interface/queries.cpp" line="191"/>
        <source>Replace</source>
        <translation type="unfinished">Ganti</translation>
    </message>
    <message>
        <location filename="../3rdparty/interface/queries.cpp" line="196"/>
        <source>Apply to all</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../3rdparty/interface/queries.cpp" line="367"/>
        <location filename="../3rdparty/interface/queries.cpp" line="456"/>
        <source>OK</source>
        <translation type="unfinished">OK</translation>
    </message>
    <message>
        <location filename="../3rdparty/interface/queries.cpp" line="454"/>
        <source>Wrong password</source>
        <translation type="unfinished">Kata laluan salah</translation>
    </message>
</context>
<context>
    <name>SettingDialog</name>
    <message>
        <location filename="../deepin-compressor/source/src/settingdialog.cpp" line="133"/>
        <source>Select All</source>
        <translation>Pilih Semua</translation>
    </message>
    <message>
        <location filename="../deepin-compressor/source/src/settingdialog.cpp" line="135"/>
        <source>Recommended</source>
        <translation>Disarankan</translation>
    </message>
    <message>
        <location filename="../deepin-compressor/source/src/settingdialog.cpp" line="172"/>
        <location filename="../deepin-compressor/source/src/settingdialog.cpp" line="201"/>
        <source>Current directory</source>
        <translation>Direktori semasa</translation>
    </message>
    <message>
        <location filename="../deepin-compressor/source/src/settingdialog.cpp" line="134"/>
        <source>Clear All</source>
        <translation>Kosongkan Semua</translation>
    </message>
    <message>
        <location filename="../deepin-compressor/source/src/settingdialog.cpp" line="166"/>
        <source>Extract archives to</source>
        <translation>Ekstrak arkib ke</translation>
    </message>
    <message>
        <location filename="../deepin-compressor/source/src/settingdialog.cpp" line="172"/>
        <location filename="../deepin-compressor/source/src/settingdialog.cpp" line="211"/>
        <source>Other directory</source>
        <translation>Lain-lain direktori</translation>
    </message>
    <message>
        <location filename="../deepin-compressor/source/src/settingdialog.cpp" line="172"/>
        <location filename="../deepin-compressor/source/src/settingdialog.cpp" line="206"/>
        <source>Desktop</source>
        <translation>Atas Meja</translation>
    </message>
    <message>
        <location filename="../deepin-compressor/source/src/settingdialog.cpp" line="275"/>
        <source>Delete archives after extraction</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../deepin-compressor/source/src/settingdialog.cpp" line="281"/>
        <location filename="../deepin-compressor/source/src/settingdialog.cpp" line="302"/>
        <source>Never</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../deepin-compressor/source/src/settingdialog.cpp" line="281"/>
        <location filename="../deepin-compressor/source/src/settingdialog.cpp" line="305"/>
        <source>Ask for confirmation</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../deepin-compressor/source/src/settingdialog.cpp" line="281"/>
        <location filename="../deepin-compressor/source/src/settingdialog.cpp" line="308"/>
        <source>Always</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../deepin-compressor/source/src/settingdialog.cpp" line="355"/>
        <source>The default extraction path does not exist, please retry</source>
        <translation>Laluan pengekstrakan lalai tidak wujud, cuba sekali lagi</translation>
    </message>
</context>
<context>
    <name>UnCompressPage</name>
    <message>
        <location filename="../deepin-compressor/source/src/uncompresspage.cpp" line="53"/>
        <source>Extract</source>
        <translation>Ekstrak</translation>
    </message>
    <message>
        <location filename="../deepin-compressor/source/src/uncompresspage.cpp" line="59"/>
        <location filename="../deepin-compressor/source/src/uncompresspage.cpp" line="150"/>
        <location filename="../deepin-compressor/source/src/uncompresspage.cpp" line="161"/>
        <source>Extract to:</source>
        <translation>Ekstrak ke:</translation>
    </message>
    <message>
        <location filename="../deepin-compressor/source/src/uncompresspage.cpp" line="113"/>
        <source>The default extraction path does not exist, please retry</source>
        <translation type="unfinished">Laluan pengekstrakan lalai tidak wujud, cuba sekali lagi</translation>
    </message>
    <message>
        <location filename="../deepin-compressor/source/src/uncompresspage.cpp" line="115"/>
        <source>You do not have permission to save files here, please change and retry</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../deepin-compressor/source/src/uncompresspage.cpp" line="134"/>
        <source>Find directory</source>
        <translation>Cari direktori</translation>
    </message>
    <message>
        <location filename="../deepin-compressor/source/src/uncompresspage.cpp" line="187"/>
        <source>OK</source>
        <translation type="unfinished">OK</translation>
    </message>
    <message>
        <location filename="../deepin-compressor/source/src/uncompresspage.cpp" line="561"/>
        <source>Cancel</source>
        <translation type="unfinished">Batal</translation>
    </message>
    <message>
        <location filename="../deepin-compressor/source/src/uncompresspage.cpp" line="562"/>
        <source>Convert</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../deepin-compressor/source/src/uncompresspage.cpp" line="572"/>
        <source>Changes to archives in this file type are not supported. Please convert the archive format to save the changes.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../deepin-compressor/source/src/uncompresspage.cpp" line="588"/>
        <source>Convert the format to:</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>fileViewer</name>
    <message>
        <location filename="../deepin-compressor/source/src/fileViewer.cpp" line="1739"/>
        <source>Extract</source>
        <translation>Ekstrak</translation>
    </message>
    <message>
        <location filename="../deepin-compressor/source/src/fileViewer.cpp" line="606"/>
        <location filename="../deepin-compressor/source/src/fileViewer.cpp" line="1747"/>
        <location filename="../deepin-compressor/source/src/fileViewer.cpp" line="1762"/>
        <source>Delete</source>
        <translation>Padam</translation>
    </message>
    <message>
        <location filename="../deepin-compressor/source/src/fileViewer.cpp" line="589"/>
        <location filename="../deepin-compressor/source/src/fileViewer.cpp" line="607"/>
        <location filename="../deepin-compressor/source/src/fileViewer.cpp" line="1745"/>
        <location filename="../deepin-compressor/source/src/fileViewer.cpp" line="1758"/>
        <source>Open</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../deepin-compressor/source/src/fileViewer.cpp" line="685"/>
        <source>item(s)</source>
        <translation>item</translation>
    </message>
    <message>
        <location filename="../deepin-compressor/source/src/fileViewer.cpp" line="707"/>
        <source>yyyy/MM/dd hh:mm:ss</source>
        <translation>yyyy/MM/dd hh:mm:ss</translation>
    </message>
    <message>
        <location filename="../deepin-compressor/source/src/fileViewer.cpp" line="1080"/>
        <location filename="../deepin-compressor/source/src/fileViewer.cpp" line="1346"/>
        <source>Confirm</source>
        <translation type="unfinished">Sah</translation>
    </message>
    <message>
        <location filename="../deepin-compressor/source/src/fileViewer.cpp" line="1122"/>
        <source>It will permanently delete the file(s). Are you sure you want to continue?</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../deepin-compressor/source/src/fileViewer.cpp" line="1078"/>
        <location filename="../deepin-compressor/source/src/fileViewer.cpp" line="1345"/>
        <source>Cancel</source>
        <translation type="unfinished">Batal</translation>
    </message>
    <message>
        <location filename="../deepin-compressor/source/src/fileViewer.cpp" line="590"/>
        <location filename="../deepin-compressor/source/src/fileViewer.cpp" line="1747"/>
        <source>Delete</source>
        <comment>slotDecompressRowDelete</comment>
        <translation type="unfinished">Padam</translation>
    </message>
    <message>
        <location filename="../deepin-compressor/source/src/fileViewer.cpp" line="592"/>
        <location filename="../deepin-compressor/source/src/fileViewer.cpp" line="609"/>
        <source>Open with</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../deepin-compressor/source/src/fileViewer.cpp" line="744"/>
        <location filename="../deepin-compressor/source/src/fileViewer.cpp" line="1776"/>
        <location filename="../deepin-compressor/source/src/fileViewer.cpp" line="1796"/>
        <source>Select default program</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../deepin-compressor/source/src/fileViewer.cpp" line="956"/>
        <location filename="../deepin-compressor/source/src/fileViewer.cpp" line="1749"/>
        <source>Do you want to delete the selected file(s)?</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../deepin-compressor/source/src/fileViewer.cpp" line="1082"/>
        <source>Update</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../deepin-compressor/source/src/fileViewer.cpp" line="1534"/>
        <source>Files have been changed. Do you want to update the changes to %1?</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../deepin-compressor/source/src/fileViewer.cpp" line="587"/>
        <location filename="../deepin-compressor/source/src/fileViewer.cpp" line="1739"/>
        <source>Extract</source>
        <comment>slotDecompressRowDoubleClicked</comment>
        <translation>Ekstrak</translation>
    </message>
    <message>
        <location filename="../deepin-compressor/source/src/fileViewer.cpp" line="588"/>
        <location filename="../deepin-compressor/source/src/fileViewer.cpp" line="1741"/>
        <source>Extract to current directory</source>
        <translation>Ekstrak ke direktori semasa</translation>
    </message>
</context>
</TS>
