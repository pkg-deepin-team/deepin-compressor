<?xml version="1.0" ?><!DOCTYPE TS><TS language="ug" version="2.1">
<context>
    <name>ArchiveModel</name>
    <message>
        <location filename="../deepin-compressor/source/src/archivemodel.cpp" line="119"/>
        <location filename="../deepin-compressor/source/src/archivemodel.cpp" line="121"/>
        <source>item(s)</source>
        <translation>تۈر(s)</translation>
    </message>
    <message>
        <location filename="../deepin-compressor/source/src/archivemodel.cpp" line="132"/>
        <source>yyyy/MM/dd hh:mm:ss</source>
        <translation>yyyy/MM/dd hh:mm:ss</translation>
    </message>
    <message>
        <location filename="../deepin-compressor/source/src/archivemodel.cpp" line="200"/>
        <source>Name</source>
        <translation>ئىسمى</translation>
    </message>
    <message>
        <location filename="../deepin-compressor/source/src/archivemodel.cpp" line="202"/>
        <source>Size</source>
        <translation>سىغىمى</translation>
    </message>
    <message>
        <location filename="../deepin-compressor/source/src/archivemodel.cpp" line="204"/>
        <source>Type</source>
        <translation>تىپى</translation>
    </message>
    <message>
        <location filename="../deepin-compressor/source/src/archivemodel.cpp" line="206"/>
        <source>Time modified</source>
        <translation>ۋاقىت ئۆزگەرتىلدى</translation>
    </message>
</context>
<context>
    <name>CompressPage</name>
    <message>
        <location filename="../deepin-compressor/source/src/compresspage.cpp" line="48"/>
        <source>Next</source>
        <translation>كېيىنكىسى</translation>
    </message>
    <message>
        <location filename="../deepin-compressor/source/src/compresspage.cpp" line="93"/>
        <source>Please add files</source>
        <translation>ھۆججەت قوشۇش</translation>
    </message>
    <message>
        <location filename="../deepin-compressor/source/src/compresspage.cpp" line="94"/>
        <location filename="../deepin-compressor/source/src/compresspage.cpp" line="116"/>
        <source>OK</source>
        <translation>تاماملاندى</translation>
    </message>
    <message>
        <location filename="../deepin-compressor/source/src/compresspage.cpp" line="118"/>
        <source>Please add files to the top-level directory</source>
        <translation>يۇقىرى دەرىجىلىك مۇندەرىجىگە ھۆججەت قوشۇڭ</translation>
    </message>
</context>
<context>
    <name>CompressSetting</name>
    <message>
        <location filename="../deepin-compressor/source/src/compresssetting.cpp" line="236"/>
        <source>Compress</source>
        <translation>پىرسلاش</translation>
    </message>
    <message>
        <location filename="../deepin-compressor/source/src/compresssetting.cpp" line="164"/>
        <source>Advanced Options</source>
        <translation>ئالىي تاللاش</translation>
    </message>
    <message>
        <location filename="../deepin-compressor/source/src/compresssetting.cpp" line="158"/>
        <source>Name</source>
        <translation>ئىسمى</translation>
    </message>
    <message>
        <location filename="../deepin-compressor/source/src/compresssetting.cpp" line="159"/>
        <source>Save to</source>
        <translation>ساقلاندى</translation>
    </message>
    <message>
        <location filename="../deepin-compressor/source/src/compresssetting.cpp" line="170"/>
        <source>Encrypt the archive</source>
        <translation>ئارخىپنى مەخپىلەشتۈرۈش</translation>
    </message>
    <message>
        <location filename="../deepin-compressor/source/src/compresssetting.cpp" line="171"/>
        <source>Support zip, 7z type only</source>
        <translation>قوللايدۇ zip ، پەقەت 7z تىپى</translation>
    </message>
    <message>
        <location filename="../deepin-compressor/source/src/compresssetting.cpp" line="176"/>
        <source>Password</source>
        <translation>مەخپىي نومۇر</translation>
    </message>
    <message>
        <location filename="../deepin-compressor/source/src/compresssetting.cpp" line="178"/>
        <source>Encrypt the file list too</source>
        <translation>ھۆججەت تىزىملىكىنىمۇ مەخپىيلەشتۈرۈڭ</translation>
    </message>
    <message>
        <location filename="../deepin-compressor/source/src/compresssetting.cpp" line="179"/>
        <location filename="../deepin-compressor/source/src/compresssetting.cpp" line="187"/>
        <source>Support 7z type only</source>
        <translation>پەقەت 7z تىپنىلا قوللايدۇ</translation>
    </message>
    <message>
        <location filename="../deepin-compressor/source/src/compresssetting.cpp" line="185"/>
        <source>Split to volumes</source>
        <translation>ھەجىمگە بۆلۈش</translation>
    </message>
    <message>
        <location filename="../deepin-compressor/source/src/compresssetting.cpp" line="335"/>
        <location filename="../deepin-compressor/source/src/compresssetting.cpp" line="830"/>
        <location filename="../deepin-compressor/source/src/compresssetting.cpp" line="966"/>
        <location filename="../deepin-compressor/source/src/compresssetting.cpp" line="1099"/>
        <source>%1 was changed on the disk, please import it again.</source>
        <translation>دىسكىدا% 1 ئۆزگەرتىلدى ، قايتا ئەكىرىڭ.</translation>
    </message>
    <message>
        <location filename="../deepin-compressor/source/src/compresssetting.cpp" line="353"/>
        <location filename="../deepin-compressor/source/src/compresssetting.cpp" line="359"/>
        <location filename="../deepin-compressor/source/src/compresssetting.cpp" line="364"/>
        <location filename="../deepin-compressor/source/src/compresssetting.cpp" line="837"/>
        <location filename="../deepin-compressor/source/src/compresssetting.cpp" line="843"/>
        <location filename="../deepin-compressor/source/src/compresssetting.cpp" line="848"/>
        <location filename="../deepin-compressor/source/src/compresssetting.cpp" line="973"/>
        <location filename="../deepin-compressor/source/src/compresssetting.cpp" line="979"/>
        <location filename="../deepin-compressor/source/src/compresssetting.cpp" line="984"/>
        <location filename="../deepin-compressor/source/src/compresssetting.cpp" line="1106"/>
        <location filename="../deepin-compressor/source/src/compresssetting.cpp" line="1112"/>
        <location filename="../deepin-compressor/source/src/compresssetting.cpp" line="1117"/>
        <source>You do not have permission to compress %1</source>
        <translation>% 1 نى پىرىسلاش ھوقۇقىڭىز يوق</translation>
    </message>
    <message>
        <location filename="../deepin-compressor/source/src/compresssetting.cpp" line="373"/>
        <location filename="../deepin-compressor/source/src/compresssetting.cpp" line="857"/>
        <location filename="../deepin-compressor/source/src/compresssetting.cpp" line="993"/>
        <source>You do not have permission to save files here, please change and retry</source>
        <translation>بۇ يەردە ھۆججەتلەرنى ساقلاش ھوقۇقىڭىز يوق ، ئۆزگەرتىپ قايتا سىناڭ</translation>
    </message>
    <message>
        <location filename="../deepin-compressor/source/src/compresssetting.cpp" line="378"/>
        <source>Too many volumes, please change and retry</source>
        <translation>توم بەك كۆپ ، ئۆزگەرتىپ قايتا سىناڭ</translation>
    </message>
    <message>
        <location filename="../deepin-compressor/source/src/compresssetting.cpp" line="478"/>
        <source>Files that begin with &apos;@&apos; cannot be compressed</source>
        <translation>@ بىلەن باشلانغان ھۆججەتلەرنى پىرىسلاشقا بولمايدۇ</translation>
    </message>
    <message>
        <location filename="../deepin-compressor/source/src/compresssetting.cpp" line="702"/>
        <source>Total size: %1</source>
        <translation>جەمئىي چوڭلۇقى:1%</translation>
    </message>
    <message>
        <location filename="../deepin-compressor/source/src/compresssetting.cpp" line="801"/>
        <location filename="../deepin-compressor/source/src/compresssetting.cpp" line="937"/>
        <location filename="../deepin-compressor/source/src/compresssetting.cpp" line="1070"/>
        <source>You cannot add the archive to itself</source>
        <translation>ئارخىپنى ئۆزىگە قوشالمايسىز</translation>
    </message>
    <message>
        <location filename="../deepin-compressor/source/src/compresssetting.cpp" line="1279"/>
        <source>Another file with the same name already exists, replace it?</source>
        <translation>ئوخشاش ئىسىمدىكى باشقا بىر ھۆججەت بۇرۇنلا مەۋجۇت ، ئۇنى ئالماشتۇرامسىز؟</translation>
    </message>
    <message>
        <location filename="../deepin-compressor/source/src/compresssetting.cpp" line="800"/>
        <source>Close</source>
        <translation>تاقاش</translation>
    </message>
    <message>
        <location filename="../deepin-compressor/source/src/compresssetting.cpp" line="936"/>
        <location filename="../deepin-compressor/source/src/compresssetting.cpp" line="1203"/>
        <source>OK</source>
        <translation>تاماملاندى</translation>
    </message>
    <message>
        <location filename="../deepin-compressor/source/src/compresssetting.cpp" line="1281"/>
        <source>Cancel</source>
        <translation>ئەمەلدىن قالدۇرۇش</translation>
    </message>
    <message>
        <location filename="../deepin-compressor/source/src/compresssetting.cpp" line="1282"/>
        <source>Replace</source>
        <translation>ئالماشتۇرۇڭ</translation>
    </message>
    <message>
        <location filename="../deepin-compressor/source/src/compresssetting.cpp" line="322"/>
        <location filename="../deepin-compressor/source/src/compresssetting.cpp" line="817"/>
        <location filename="../deepin-compressor/source/src/compresssetting.cpp" line="953"/>
        <location filename="../deepin-compressor/source/src/compresssetting.cpp" line="1086"/>
        <source>Please enter the path</source>
        <translation>مۇندەرىجىنى كىرگۈزۈڭ</translation>
    </message>
    <message>
        <location filename="../deepin-compressor/source/src/compresssetting.cpp" line="149"/>
        <source>New Archive</source>
        <translation>يىڭى ئارخىپ</translation>
    </message>
    <message>
        <location filename="../deepin-compressor/source/src/compresssetting.cpp" line="317"/>
        <location filename="../deepin-compressor/source/src/compresssetting.cpp" line="812"/>
        <location filename="../deepin-compressor/source/src/compresssetting.cpp" line="948"/>
        <location filename="../deepin-compressor/source/src/compresssetting.cpp" line="1081"/>
        <source>Invalid file name</source>
        <translation>ھۆججەت ئىسمى ئىناۋەتسىز</translation>
    </message>
    <message>
        <location filename="../deepin-compressor/source/src/compresssetting.cpp" line="327"/>
        <location filename="../deepin-compressor/source/src/compresssetting.cpp" line="822"/>
        <location filename="../deepin-compressor/source/src/compresssetting.cpp" line="958"/>
        <location filename="../deepin-compressor/source/src/compresssetting.cpp" line="1091"/>
        <source>The path does not exist, please retry</source>
        <translation>بۇ مۇندەرىجە مەۋجۇت ئەمەس ، قايتا سىناڭ</translation>
    </message>
</context>
<context>
    <name>Compressor_Fail</name>
    <message>
        <location filename="../deepin-compressor/source/src/compressor_fail.cpp" line="35"/>
        <source>Extraction failed</source>
        <translation>ئېلىش مەغلۇب بولدى</translation>
    </message>
    <message>
        <location filename="../deepin-compressor/source/src/compressor_fail.cpp" line="36"/>
        <source>Damaged file, unable to extract</source>
        <translation>بۇزۇلغان ھۆججەت ، چىقىرىشقا ئامالسىز</translation>
    </message>
    <message>
        <location filename="../deepin-compressor/source/src/compressor_fail.cpp" line="61"/>
        <source>Retry</source>
        <translation>قايتا سىناش</translation>
    </message>
    <message>
        <location filename="../deepin-compressor/source/src/compressor_fail.cpp" line="63"/>
        <source>Back</source>
        <translation>قايتىش</translation>
    </message>
</context>
<context>
    <name>Compressor_Success</name>
    <message>
        <location filename="../deepin-compressor/source/src/compressor_success.cpp" line="40"/>
        <source>Compression successful</source>
        <translation>مۇۋەپپەقىيەتلىك پىرىسلاندى</translation>
    </message>
    <message>
        <location filename="../deepin-compressor/source/src/compressor_success.cpp" line="58"/>
        <source>View</source>
        <translation>كۆرۈش</translation>
    </message>
    <message>
        <location filename="../deepin-compressor/source/src/compressor_success.cpp" line="60"/>
        <source>Back</source>
        <translation>قايتىش</translation>
    </message>
</context>
<context>
    <name>EncodingPage</name>
    <message>
        <location filename="../deepin-compressor/source/src/encodingpage.cpp" line="65"/>
        <source>Filename Encoding</source>
        <translation>ھۆججەت ئىسمىنى كودلاش</translation>
    </message>
    <message>
        <location filename="../deepin-compressor/source/src/encodingpage.cpp" line="78"/>
        <source>Select an encoding to continue</source>
        <translation>داۋاملاشتۇرۇش ئۈچۈن كودلاشنى تاللاڭ</translation>
    </message>
    <message>
        <location filename="../deepin-compressor/source/src/encodingpage.cpp" line="82"/>
        <source>Cancel</source>
        <translation>ئەمەلدىن قالدۇرۇش</translation>
    </message>
    <message>
        <location filename="../deepin-compressor/source/src/encodingpage.cpp" line="83"/>
        <source>Select Character Encoding</source>
        <translation>ھەرپ كودلاشنى تاللاڭ</translation>
    </message>
</context>
<context>
    <name>EncryptionPage</name>
    <message>
        <location filename="../deepin-compressor/source/src/encryptionpage.cpp" line="47"/>
        <source>Encrypted file, please enter the password</source>
        <translation>شىفىرلانغان ھۆججەت ، مەخپىي نومۇرنى كىرگۈزۈڭ</translation>
    </message>
    <message>
        <location filename="../deepin-compressor/source/src/encryptionpage.cpp" line="50"/>
        <source>Next</source>
        <translation>كېيىنكىسى</translation>
    </message>
    <message>
        <location filename="../deepin-compressor/source/src/encryptionpage.cpp" line="55"/>
        <source>Password</source>
        <translation>مەخپىي نومۇر</translation>
    </message>
    <message>
        <location filename="../deepin-compressor/source/src/encryptionpage.cpp" line="135"/>
        <source>Wrong password</source>
        <translation>مەخپىي نومۇر خاتا</translation>
    </message>
</context>
<context>
    <name>ExtractPauseDialog</name>
    <message>
        <location filename="../deepin-compressor/source/src/extractpausedialog.cpp" line="67"/>
        <source>Are you sure you want to stop the extraction?</source>
        <translation>ئېلىشنى توختاتماقچى بولغانلىقىڭىزغا جەزىم قىلامسىز؟</translation>
    </message>
    <message>
        <location filename="../deepin-compressor/source/src/extractpausedialog.cpp" line="69"/>
        <source>Cancel</source>
        <translation>ئەمەلدىن قالدۇرۇش</translation>
    </message>
    <message>
        <location filename="../deepin-compressor/source/src/extractpausedialog.cpp" line="70"/>
        <source>Confirm</source>
        <translation>جەزىملەشتۈرۈڭ</translation>
    </message>
</context>
<context>
    <name>HomePage</name>
    <message>
        <location filename="../deepin-compressor/source/src/homepage.cpp" line="40"/>
        <source>Drag file or folder here</source>
        <translation>ھۆججەت ياكى ھۆججەت قىسقۇچنى بۇ يەرگە سۆرۈڭ</translation>
    </message>
    <message>
        <location filename="../deepin-compressor/source/src/homepage.cpp" line="42"/>
        <source>Select File</source>
        <translation>ھۆججەتنى تاللاڭ</translation>
    </message>
</context>
<context>
    <name>LogViewHeaderView</name>
    <message>
        <location filename="../deepin-compressor/source/src/logviewheaderview.cpp" line="160"/>
        <source>Back</source>
        <translation>قايتىش</translation>
    </message>
</context>
<context>
    <name>Main</name>
    <message>
        <location filename="../deepin-compressor/main.cpp" line="145"/>
        <location filename="../tests/UnitTest/deepin-compressor/source/src/test_main.cpp" line="150"/>
        <source>Archive Manager</source>
        <translation>ئارخىپ باشقۇرغۇچى</translation>
    </message>
    <message>
        <location filename="../deepin-compressor/main.cpp" line="146"/>
        <location filename="../tests/UnitTest/deepin-compressor/source/src/test_main.cpp" line="151"/>
        <source>Archive Manager is a fast and lightweight application for creating and extracting archives.</source>
        <translation>ئارخىپ باشقۇرغۇچى ئارخىپ قۇرۇش ۋە چىقىرىش ئۈچۈن تېز ھەم يېنىك قوللىنىشچان پروگرامما.</translation>
    </message>
</context>
<context>
    <name>MainWindow</name>
    <message>
        <location filename="../deepin-compressor/source/src/mainwindow.cpp" line="564"/>
        <source>Close</source>
        <translation>تاقاش</translation>
    </message>
    <message>
        <location filename="../deepin-compressor/source/src/mainwindow.cpp" line="568"/>
        <source>Help</source>
        <translation>ياردەم</translation>
    </message>
    <message>
        <location filename="../deepin-compressor/source/src/mainwindow.cpp" line="572"/>
        <source>Select the file</source>
        <translation>ئېچىش</translation>
    </message>
    <message>
        <location filename="../deepin-compressor/source/src/mainwindow.cpp" line="576"/>
        <source>Delete</source>
        <translation>ئۆچۈرۈش</translation>
    </message>
    <message>
        <location filename="../deepin-compressor/source/src/mainwindow.cpp" line="596"/>
        <source>Shortcuts</source>
        <translation>تېزلەتمە</translation>
    </message>
    <message>
        <location filename="../deepin-compressor/source/src/mainwindow.cpp" line="781"/>
        <source>Open file</source>
        <translation>ھۆججەتنى ئېچىڭ</translation>
    </message>
    <message>
        <location filename="../deepin-compressor/source/src/mainwindow.cpp" line="822"/>
        <source>Settings</source>
        <translation>تەڭشەك</translation>
    </message>
    <message>
        <location filename="../deepin-compressor/source/src/mainwindow.cpp" line="1284"/>
        <location filename="../deepin-compressor/source/src/mainwindow.cpp" line="1297"/>
        <location filename="../deepin-compressor/source/src/mainwindow.cpp" line="2961"/>
        <source>Create New Archive</source>
        <translation>يىڭى ئارخىپ قۇرۇش</translation>
    </message>
    <message>
        <location filename="../deepin-compressor/source/src/mainwindow.cpp" line="1338"/>
        <source>Opening</source>
        <translation>ئېچىۋاتىدۇ</translation>
    </message>
    <message>
        <location filename="../deepin-compressor/source/src/mainwindow.cpp" line="1366"/>
        <source>Converting</source>
        <translation>ئايلاندۇرۋاتىدۇ</translation>
    </message>
    <message>
        <location filename="../deepin-compressor/source/src/mainwindow.cpp" line="1375"/>
        <source>Compression successful</source>
        <translation>مۇۋەپپەقىيەتلىك پىرىسلاندى</translation>
    </message>
    <message>
        <location filename="../deepin-compressor/source/src/mainwindow.cpp" line="5119"/>
        <source>Confirm</source>
        <translation>جەزىملەشتۈرۈڭ</translation>
    </message>
    <message>
        <location filename="../deepin-compressor/source/src/mainwindow.cpp" line="5122"/>
        <source>Do you want to delete the archive?</source>
        <translation>تاللانغان ئارخىپىنى يۇيۇۋەتمەكچىمۇ؟</translation>
    </message>
    <message>
        <location filename="../deepin-compressor/source/src/mainwindow.cpp" line="2573"/>
        <location filename="../deepin-compressor/source/src/mainwindow.cpp" line="2669"/>
        <location filename="../deepin-compressor/source/src/mainwindow.cpp" line="2703"/>
        <source>Skip all files</source>
        <translation>ھەممە ھۆججەتتىن ئاتلاش</translation>
    </message>
    <message>
        <location filename="../deepin-compressor/source/src/mainwindow.cpp" line="2546"/>
        <location filename="../deepin-compressor/source/src/mainwindow.cpp" line="2705"/>
        <source>Extraction successful</source>
        <translation>ئېلىش مۇۋەپپەقىيەتلىك بولدى</translation>
    </message>
    <message>
        <location filename="../deepin-compressor/source/src/mainwindow.cpp" line="469"/>
        <source>%1 was changed on the disk, please import it again.</source>
        <translation>دىسكىدا% 1 ئۆزگەرتىلدى ، قايتا ئەكىرىڭ.</translation>
    </message>
    <message>
        <location filename="../deepin-compressor/source/src/mainwindow.cpp" line="1631"/>
        <source>Do you want to add the archive to the list or open it in new window?</source>
        <translation>ئارخىپنى تىزىملىككە قوشماقچىمۇ ياكى يېڭى كۆزنەكتە ئاچماقچىمۇ؟</translation>
    </message>
    <message>
        <location filename="../deepin-compressor/source/src/mainwindow.cpp" line="584"/>
        <source>Display shortcuts</source>
        <translation>تېزلەتمە كۆرسىتىش</translation>
    </message>
    <message>
        <location filename="../deepin-compressor/source/src/mainwindow.cpp" line="242"/>
        <location filename="../deepin-compressor/source/src/mainwindow.cpp" line="1632"/>
        <location filename="../deepin-compressor/source/src/mainwindow.cpp" line="5118"/>
        <source>Cancel</source>
        <translation>ئەمەلدىن قالدۇرۇش</translation>
    </message>
    <message>
        <location filename="../deepin-compressor/source/src/mainwindow.cpp" line="114"/>
        <source>Archive Manager</source>
        <translation>ئارخىپ باشقۇرغۇچى</translation>
    </message>
    <message>
        <location filename="../deepin-compressor/source/src/mainwindow.cpp" line="475"/>
        <location filename="../deepin-compressor/source/src/mainwindow.cpp" line="2246"/>
        <location filename="../deepin-compressor/source/src/mainwindow.cpp" line="4916"/>
        <source>OK</source>
        <translation>تاماملاندى</translation>
    </message>
    <message>
        <location filename="../deepin-compressor/source/src/mainwindow.cpp" line="1316"/>
        <source>Adding files to %1</source>
        <translation>%1 غا ھۆججەت قوشۇۋاتىدۇ</translation>
    </message>
    <message>
        <location filename="../deepin-compressor/source/src/mainwindow.cpp" line="1319"/>
        <source>Compressing</source>
        <translation>پىرىسلاش</translation>
    </message>
    <message>
        <location filename="../deepin-compressor/source/src/mainwindow.cpp" line="1340"/>
        <source>Extracting</source>
        <translation>ئېلىش</translation>
    </message>
    <message>
        <location filename="../deepin-compressor/source/src/mainwindow.cpp" line="1354"/>
        <source>Deleting</source>
        <translation>ئۆچۈرلىۋاتىدۇ</translation>
    </message>
    <message>
        <location filename="../deepin-compressor/source/src/mainwindow.cpp" line="1392"/>
        <source>Compression failed</source>
        <translation>پىرىسلاش مەغلۇپ بولدى</translation>
    </message>
    <message>
        <location filename="../deepin-compressor/source/src/mainwindow.cpp" line="1424"/>
        <source>Extraction failed</source>
        <translation>ئېلىش مەغلۇب بولدى</translation>
    </message>
    <message>
        <location filename="../deepin-compressor/source/src/mainwindow.cpp" line="1437"/>
        <source>Conversion successful</source>
        <translation>ئۆزگەرتىش مۇۋەپپەقىيەتلىك</translation>
    </message>
    <message>
        <location filename="../deepin-compressor/source/src/mainwindow.cpp" line="1633"/>
        <source>Add</source>
        <translation>قوشۇش</translation>
    </message>
    <message>
        <location filename="../deepin-compressor/source/src/mainwindow.cpp" line="1634"/>
        <source>Open in new window</source>
        <translation>يىڭى كۆزنەك ئېچىش</translation>
    </message>
    <message>
        <location filename="../deepin-compressor/source/src/mainwindow.cpp" line="1841"/>
        <source>Find directory</source>
        <translation>مۇندەرىجىنى تېپىڭ</translation>
    </message>
    <message>
        <location filename="../deepin-compressor/source/src/mainwindow.cpp" line="2167"/>
        <location filename="../deepin-compressor/source/src/mainwindow.cpp" line="2598"/>
        <source>Failed to open the archive: %1</source>
        <translation>ئارخىپ %1 نى ئاچالمىدى</translation>
    </message>
    <message>
        <location filename="../deepin-compressor/source/src/mainwindow.cpp" line="2481"/>
        <location filename="../deepin-compressor/source/src/mainwindow.cpp" line="2600"/>
        <source>Wrong password</source>
        <translation>مەخپىي نومۇر خاتا</translation>
    </message>
    <message>
        <location filename="../deepin-compressor/source/src/mainwindow.cpp" line="2687"/>
        <source>Select default program</source>
        <translation>سۈكۈتتىكى پىروگراممىنى تاللاش</translation>
    </message>
    <message>
        <location filename="../deepin-compressor/source/src/mainwindow.cpp" line="4918"/>
        <source>Please check the file association type in the settings of Archive Manager</source>
        <translation>ئارخىپ باشقۇرغۇچىنىڭ تەڭشەكلىرىدىكى ھۆججەت بىرلەشمىسىنىڭ تۈرىنى تەكشۈرۈپ بېقىڭ</translation>
    </message>
    <message>
        <location filename="../deepin-compressor/source/src/mainwindow.cpp" line="1982"/>
        <location filename="../deepin-compressor/source/src/mainwindow.cpp" line="2005"/>
        <location filename="../deepin-compressor/source/src/mainwindow.cpp" line="2173"/>
        <location filename="../deepin-compressor/source/src/mainwindow.cpp" line="2593"/>
        <source>Damaged file, unable to extract</source>
        <translation>بۇزۇلغان ھۆججەت ، چىقىرىشقا ئامالسىز</translation>
    </message>
    <message>
        <location filename="../deepin-compressor/source/src/mainwindow.cpp" line="241"/>
        <source>Are you sure you want to stop the ongoing task?</source>
        <translation>داۋاملىشىۋاتقان ۋەزىپىنى توختاتماقچىمۇ؟</translation>
    </message>
    <message>
        <location filename="../deepin-compressor/source/src/mainwindow.cpp" line="2245"/>
        <source>The archive was changed on the disk, please import it again.</source>
        <translation>ئارخىپ دىسكىدا ئۆزگەرتىلدى ، ئۇنى قايتا ئەكىرىڭ.</translation>
    </message>
    <message>
        <location filename="../deepin-compressor/source/src/mainwindow.cpp" line="2589"/>
        <location filename="../deepin-compressor/source/src/mainwindow.cpp" line="2591"/>
        <location filename="../deepin-compressor/source/src/mainwindow.cpp" line="4028"/>
        <location filename="../deepin-compressor/source/src/mainwindow.cpp" line="4034"/>
        <location filename="../deepin-compressor/source/src/mainwindow.cpp" line="4085"/>
        <location filename="../deepin-compressor/source/src/mainwindow.cpp" line="4091"/>
        <source>Insufficient space, please clear and retry</source>
        <translation>بوشلۇق يېتەرلىك ئەمەس ، ئېنىقلاپ قايتا سىناڭ</translation>
    </message>
    <message>
        <location filename="../deepin-compressor/source/src/mainwindow.cpp" line="2596"/>
        <source>File name too long, unable to extract</source>
        <translation>ھۆججەت نامى بەك ئۇزۇن،چىقىرىشقا ئامالسىز</translation>
    </message>
    <message>
        <location filename="../deepin-compressor/source/src/mainwindow.cpp" line="2600"/>
        <source>Unable to extract</source>
        <translation>چىقىرىشقا ئامالسىز</translation>
    </message>
    <message>
        <location filename="../deepin-compressor/source/src/mainwindow.cpp" line="4030"/>
        <location filename="../deepin-compressor/source/src/mainwindow.cpp" line="4036"/>
        <location filename="../deepin-compressor/source/src/mainwindow.cpp" line="4087"/>
        <location filename="../deepin-compressor/source/src/mainwindow.cpp" line="4093"/>
        <source>Damaged file</source>
        <translation>بۇزۇلغان ھۆججەت</translation>
    </message>
</context>
<context>
    <name>MimeTypeDisplayManager</name>
    <message>
        <location filename="../deepin-compressor/source/src/mimetypedisplaymanager.cpp" line="46"/>
        <source>Directory</source>
        <translation>مۇندەرىجە</translation>
    </message>
    <message>
        <location filename="../deepin-compressor/source/src/mimetypedisplaymanager.cpp" line="47"/>
        <source>Application</source>
        <translation>ئىلتىماس</translation>
    </message>
    <message>
        <location filename="../deepin-compressor/source/src/mimetypedisplaymanager.cpp" line="48"/>
        <source>Video</source>
        <translation>فىلىم</translation>
    </message>
    <message>
        <location filename="../deepin-compressor/source/src/mimetypedisplaymanager.cpp" line="49"/>
        <source>Audio</source>
        <translation>مۇزىكا</translation>
    </message>
    <message>
        <location filename="../deepin-compressor/source/src/mimetypedisplaymanager.cpp" line="50"/>
        <source>Image</source>
        <translation>رەسىم</translation>
    </message>
    <message>
        <location filename="../deepin-compressor/source/src/mimetypedisplaymanager.cpp" line="51"/>
        <source>Archive</source>
        <translation>ئارخىپ</translation>
    </message>
    <message>
        <location filename="../deepin-compressor/source/src/mimetypedisplaymanager.cpp" line="53"/>
        <source>Executable</source>
        <translation>ئىجرا قىلىشقا بولىدۇ</translation>
    </message>
    <message>
        <location filename="../deepin-compressor/source/src/mimetypedisplaymanager.cpp" line="52"/>
        <source>Document</source>
        <translation>پۈتۈك</translation>
    </message>
    <message>
        <location filename="../deepin-compressor/source/src/mimetypedisplaymanager.cpp" line="54"/>
        <source>Backup file</source>
        <translation>زاپاس ھۆججەت</translation>
    </message>
    <message>
        <location filename="../deepin-compressor/source/src/mimetypedisplaymanager.cpp" line="55"/>
        <source>Unknown</source>
        <translation>نامەلۇم</translation>
    </message>
</context>
<context>
    <name>MyFileSystemModel</name>
    <message>
        <location filename="../deepin-compressor/source/src/myfilesystemmodel.cpp" line="63"/>
        <source>Name</source>
        <translation>ئىسمى</translation>
    </message>
    <message>
        <location filename="../deepin-compressor/source/src/myfilesystemmodel.cpp" line="65"/>
        <source>Size</source>
        <translation>سىغىمى</translation>
    </message>
    <message>
        <location filename="../deepin-compressor/source/src/myfilesystemmodel.cpp" line="67"/>
        <source>Type</source>
        <translation>تىپى</translation>
    </message>
    <message>
        <location filename="../deepin-compressor/source/src/myfilesystemmodel.cpp" line="69"/>
        <source>Time modified</source>
        <translation>ۋاقىت ئۆزگەرتىلدى</translation>
    </message>
    <message>
        <location filename="../deepin-compressor/source/src/myfilesystemmodel.cpp" line="127"/>
        <source>item(s)</source>
        <translation>item(s)</translation>
    </message>
    <message>
        <location filename="../deepin-compressor/source/src/myfilesystemmodel.cpp" line="138"/>
        <source>yyyy/MM/dd hh:mm:ss</source>
        <translation>yyyy/MM/dd hh:mm:ss</translation>
    </message>
</context>
<context>
    <name>OpenLoadingPage</name>
    <message>
        <location filename="../deepin-compressor/source/src/openloadingpage.cpp" line="52"/>
        <source>Loading, please wait...</source>
        <translation>يۈكلەۋاتىدۇ،كۈتۈپ تۇرۇڭ...</translation>
    </message>
</context>
<context>
    <name>OpenWithDialog</name>
    <message>
        <location filename="../deepin-compressor/source/src/openwithdialog/openwithdialog.cpp" line="308"/>
        <source>Open with</source>
        <translation>بىلەن ئېچىش</translation>
    </message>
    <message>
        <location filename="../deepin-compressor/source/src/openwithdialog/openwithdialog.cpp" line="327"/>
        <source>Add other programs</source>
        <translation>باشقا پىروگراممىلارنى قوشۇش</translation>
    </message>
    <message>
        <location filename="../deepin-compressor/source/src/openwithdialog/openwithdialog.cpp" line="328"/>
        <source>Set as default</source>
        <translation>سۈكۈتتىكى قىلىپ تەڭشەش</translation>
    </message>
    <message>
        <location filename="../deepin-compressor/source/src/openwithdialog/openwithdialog.cpp" line="330"/>
        <source>Cancel</source>
        <translation>ئەمەلدىن قالدۇرۇش</translation>
    </message>
    <message>
        <location filename="../deepin-compressor/source/src/openwithdialog/openwithdialog.cpp" line="331"/>
        <source>Confirm</source>
        <translation>جەزىملەشتۈرۈڭ</translation>
    </message>
    <message>
        <location filename="../deepin-compressor/source/src/openwithdialog/openwithdialog.cpp" line="335"/>
        <source>Recommended Applications</source>
        <translation>تەۋسىيەلىك ئەپلەر</translation>
    </message>
    <message>
        <location filename="../deepin-compressor/source/src/openwithdialog/openwithdialog.cpp" line="337"/>
        <source>Other Applications</source>
        <translation>باشقا ئەپلەر</translation>
    </message>
</context>
<context>
    <name>PasswordNeededQuery</name>
    <message>
        <location filename="../3rdparty/interface/queries.cpp" line="359"/>
        <source>Encrypted file, please enter the password</source>
        <translation>شىفىرلانغان ھۆججەت ، مەخپىي نومۇرنى كىرگۈزۈڭ</translation>
    </message>
</context>
<context>
    <name>Progress</name>
    <message>
        <location filename="../deepin-compressor/source/src/progress.cpp" line="89"/>
        <location filename="../deepin-compressor/source/src/progress.cpp" line="415"/>
        <source>Cancel</source>
        <translation>ئەمەلدىن قالدۇرۇش</translation>
    </message>
    <message>
        <location filename="../deepin-compressor/source/src/progress.cpp" line="85"/>
        <location filename="../deepin-compressor/source/src/progress.cpp" line="148"/>
        <location filename="../deepin-compressor/source/src/progress.cpp" line="150"/>
        <location filename="../deepin-compressor/source/src/progress.cpp" line="152"/>
        <location filename="../deepin-compressor/source/src/progress.cpp" line="154"/>
        <location filename="../deepin-compressor/source/src/progress.cpp" line="156"/>
        <location filename="../deepin-compressor/source/src/progress.cpp" line="159"/>
        <location filename="../deepin-compressor/source/src/progress.cpp" line="442"/>
        <source>Calculating...</source>
        <translation>ھېسابلاۋاتىدۇ...</translation>
    </message>
    <message>
        <location filename="../deepin-compressor/source/src/progress.cpp" line="93"/>
        <location filename="../deepin-compressor/source/src/progress.cpp" line="191"/>
        <location filename="../deepin-compressor/source/src/progress.cpp" line="468"/>
        <source>Pause</source>
        <translation>توختاش</translation>
    </message>
    <message>
        <location filename="../deepin-compressor/source/src/progress.cpp" line="152"/>
        <location filename="../deepin-compressor/source/src/progress.cpp" line="275"/>
        <location filename="../deepin-compressor/source/src/progress.cpp" line="277"/>
        <source>Speed</source>
        <comment>delete</comment>
        <translation>پىرېستىن يېشىش سۈرئىتى</translation>
    </message>
    <message>
        <location filename="../deepin-compressor/source/src/progress.cpp" line="156"/>
        <location filename="../deepin-compressor/source/src/progress.cpp" line="299"/>
        <location filename="../deepin-compressor/source/src/progress.cpp" line="301"/>
        <location filename="../deepin-compressor/source/src/progress.cpp" line="303"/>
        <source>Speed</source>
        <comment>convert</comment>
        <translation>پىرېستىن يېشىش سۈرئىتى</translation>
    </message>
    <message>
        <location filename="../deepin-compressor/source/src/progress.cpp" line="403"/>
        <source>Are you sure you want to stop the update?</source>
        <translation>يىڭىلاشنى توختاتماقچىمۇ؟</translation>
    </message>
    <message>
        <location filename="../deepin-compressor/source/src/progress.cpp" line="405"/>
        <source>Are you sure you want to stop the conversion?</source>
        <translation>ئايلاندۇرۇشنى توختاتماقچىمۇ؟</translation>
    </message>
    <message>
        <location filename="../deepin-compressor/source/src/progress.cpp" line="342"/>
        <source>Converting</source>
        <translation>ئايلاندۇرۋاتىدۇ</translation>
    </message>
    <message>
        <location filename="../deepin-compressor/source/src/progress.cpp" line="345"/>
        <source>Opening</source>
        <translation>ئېچىۋاتىدۇ</translation>
    </message>
    <message>
        <location filename="../deepin-compressor/source/src/progress.cpp" line="401"/>
        <source>Are you sure you want to stop the compression?</source>
        <translation>پىرىسلاشنى توختاتماقچىمۇ؟</translation>
    </message>
    <message>
        <location filename="../deepin-compressor/source/src/progress.cpp" line="416"/>
        <source>Confirm</source>
        <translation>جەزىملەشتۈرۈڭ</translation>
    </message>
    <message>
        <location filename="../deepin-compressor/source/src/progress.cpp" line="462"/>
        <source>Continue</source>
        <translation>داۋاملىق</translation>
    </message>
    <message>
        <location filename="../deepin-compressor/source/src/progress.cpp" line="411"/>
        <source>Are you sure you want to stop the extraction?</source>
        <translation>ئېلىشنى توختاتماقچى بولغانلىقىڭىزغا جەزىم قىلامسىز؟</translation>
    </message>
    <message>
        <location filename="../deepin-compressor/source/src/progress.cpp" line="148"/>
        <location filename="../deepin-compressor/source/src/progress.cpp" line="154"/>
        <location filename="../deepin-compressor/source/src/progress.cpp" line="267"/>
        <location filename="../deepin-compressor/source/src/progress.cpp" line="269"/>
        <location filename="../deepin-compressor/source/src/progress.cpp" line="271"/>
        <location filename="../deepin-compressor/source/src/progress.cpp" line="283"/>
        <location filename="../deepin-compressor/source/src/progress.cpp" line="285"/>
        <location filename="../deepin-compressor/source/src/progress.cpp" line="287"/>
        <source>Speed</source>
        <comment>compress</comment>
        <translation>پىرېسلاش سۈرئىتى</translation>
    </message>
    <message>
        <location filename="../deepin-compressor/source/src/progress.cpp" line="159"/>
        <location filename="../deepin-compressor/source/src/progress.cpp" line="307"/>
        <source>Time left</source>
        <translation>قالغان ۋاقىت</translation>
    </message>
    <message>
        <location filename="../deepin-compressor/source/src/progress.cpp" line="150"/>
        <location filename="../deepin-compressor/source/src/progress.cpp" line="291"/>
        <location filename="../deepin-compressor/source/src/progress.cpp" line="293"/>
        <location filename="../deepin-compressor/source/src/progress.cpp" line="295"/>
        <source>Speed</source>
        <comment>uncompress</comment>
        <translation>پىرېستىن يېشىش سۈرئىتى</translation>
    </message>
    <message>
        <location filename="../deepin-compressor/source/src/progress.cpp" line="338"/>
        <source>Compressing</source>
        <translation>پىرىسلاش</translation>
    </message>
    <message>
        <location filename="../deepin-compressor/source/src/progress.cpp" line="340"/>
        <source>Deleting</source>
        <translation>ئۆچۈرلىۋاتىدۇ</translation>
    </message>
    <message>
        <location filename="../deepin-compressor/source/src/progress.cpp" line="347"/>
        <source>Extracting</source>
        <translation>ئېلىش</translation>
    </message>
</context>
<context>
    <name>ProgressDialog</name>
    <message>
        <location filename="../deepin-compressor/source/src/progressdialog.cpp" line="50"/>
        <source>%1 task(s) in progress</source>
        <translation>% 1 ۋەزىپە (ۋەزىپە) داۋاملىشىۋاتىدۇ</translation>
    </message>
    <message>
        <location filename="../deepin-compressor/source/src/progressdialog.cpp" line="62"/>
        <location filename="../deepin-compressor/source/src/progressdialog.cpp" line="115"/>
        <source>Task</source>
        <translation>ۋەزىپە</translation>
    </message>
    <message>
        <location filename="../deepin-compressor/source/src/progressdialog.cpp" line="63"/>
        <location filename="../deepin-compressor/source/src/progressdialog.cpp" line="121"/>
        <location filename="../deepin-compressor/source/src/progressdialog.cpp" line="140"/>
        <source>Extracting</source>
        <translation>ئېلىش</translation>
    </message>
    <message>
        <location filename="../deepin-compressor/source/src/progressdialog.cpp" line="136"/>
        <source>Extraction successful</source>
        <translation>ئېلىش مۇۋەپپەقىيەتلىك بولدى</translation>
    </message>
    <message>
        <location filename="../deepin-compressor/source/src/progressdialog.cpp" line="136"/>
        <source>Extract to</source>
        <translation>چىقىرىڭ</translation>
    </message>
    <message>
        <location filename="../deepin-compressor/source/src/progressdialog.cpp" line="141"/>
        <source>Extraction successful</source>
        <comment>progressdialog</comment>
        <translation>ئېلىش مۇۋەپپەقىيەتلىك بولدى</translation>
    </message>
</context>
<context>
    <name>QInstaller</name>
    <message>
        <location filename="../deepin-compressor/source/src/utils.cpp" line="178"/>
        <source>B</source>
        <translation>B</translation>
    </message>
    <message>
        <location filename="../deepin-compressor/source/src/utils.cpp" line="179"/>
        <source>KB</source>
        <translation>كىكابايت</translation>
    </message>
    <message>
        <location filename="../deepin-compressor/source/src/utils.cpp" line="180"/>
        <source>MB</source>
        <translation>مىگاباىت</translation>
    </message>
    <message>
        <location filename="../deepin-compressor/source/src/utils.cpp" line="181"/>
        <source>GB</source>
        <translation>گىگابايت</translation>
    </message>
    <message>
        <location filename="../deepin-compressor/source/src/utils.cpp" line="182"/>
        <source>TB</source>
        <translation>TB</translation>
    </message>
    <message>
        <location filename="../deepin-compressor/source/src/utils.cpp" line="183"/>
        <source>PB</source>
        <translation>PB</translation>
    </message>
    <message>
        <location filename="../deepin-compressor/source/src/utils.cpp" line="184"/>
        <source>EB</source>
        <translation>EB</translation>
    </message>
    <message>
        <location filename="../deepin-compressor/source/src/utils.cpp" line="185"/>
        <source>ZB</source>
        <translation>ZB</translation>
    </message>
    <message>
        <location filename="../deepin-compressor/source/src/utils.cpp" line="186"/>
        <source>YB</source>
        <translation>YB</translation>
    </message>
</context>
<context>
    <name>QObject</name>
    <message>
        <location filename="../deepin-compressor/source/src/fileViewer.cpp" line="640"/>
        <source>Name</source>
        <translation>ئىسمى</translation>
    </message>
    <message>
        <location filename="../deepin-compressor/source/src/fileViewer.cpp" line="644"/>
        <source>Time modified</source>
        <translation>ۋاقىت ئۆزگەرتىلدى</translation>
    </message>
    <message>
        <location filename="../deepin-compressor/source/src/fileViewer.cpp" line="648"/>
        <source>Type</source>
        <translation>تىپى</translation>
    </message>
    <message>
        <location filename="../deepin-compressor/source/src/fileViewer.cpp" line="652"/>
        <source>Size</source>
        <translation>سىغىمى</translation>
    </message>
    <message>
        <location filename="../deepin-compressor/source/src/fileViewer.cpp" line="1555"/>
        <source>Update</source>
        <translation>يىڭىلاش</translation>
    </message>
    <message>
        <location filename="../deepin-compressor/source/src/settings_translation.cpp" line="5"/>
        <source>General</source>
        <translation>General</translation>
    </message>
    <message>
        <location filename="../deepin-compressor/source/src/settings_translation.cpp" line="6"/>
        <source>Extraction</source>
        <translation>ئېلىش</translation>
    </message>
    <message>
        <location filename="../deepin-compressor/source/src/settings_translation.cpp" line="7"/>
        <source>Auto create a folder for multiple extracted files</source>
        <translation>كۆپ چىقىرىۋېتىلگەن ھۆججەتلەر ئۈچۈن ئاپتوماتىك ھۆججەت قىسقۇچ قۇرالايسىز</translation>
    </message>
    <message>
        <location filename="../deepin-compressor/source/src/settings_translation.cpp" line="8"/>
        <source>Show extracted files when completed</source>
        <translation>چىقىرىۋېتىلگەن ھۆججەتلەرنى تاماملاپ بولۇپ كۆرسىتىش</translation>
    </message>
    <message>
        <location filename="../deepin-compressor/source/src/settings_translation.cpp" line="9"/>
        <source>File Management</source>
        <translation>ھۆججەت باشقۇرۇش</translation>
    </message>
    <message>
        <location filename="../deepin-compressor/source/src/settings_translation.cpp" line="10"/>
        <source>Delete files after compression</source>
        <translation>ھۆججەتلەرنى پىرىسلاپ بولۇپ يۇيۇۋىتىش</translation>
    </message>
    <message>
        <location filename="../deepin-compressor/source/src/settings_translation.cpp" line="11"/>
        <source>Files Associated</source>
        <translation>مۇناسىۋەتلىك ھۆججەتلەر</translation>
    </message>
    <message>
        <location filename="../deepin-compressor/source/src/settings_translation.cpp" line="12"/>
        <source>File Type</source>
        <translation>ھۆججەت تىپى</translation>
    </message>
    <message>
        <location filename="../deepin-compressor/source/src/myfileitem.cpp" line="50"/>
        <location filename="../deepin-compressor/source/src/myfileitem.cpp" line="51"/>
        <location filename="../deepin-compressor/source/src/myfileitem.cpp" line="53"/>
        <location filename="../deepin-compressor/source/src/myfileitem.cpp" line="54"/>
        <location filename="../deepin-compressor/source/src/myfileitem.cpp" line="56"/>
        <location filename="../deepin-compressor/source/src/myfileitem.cpp" line="57"/>
        <location filename="../deepin-compressor/source/src/myfileitem.cpp" line="59"/>
        <location filename="../deepin-compressor/source/src/myfileitem.cpp" line="61"/>
        <source>item(s)</source>
        <translation>تۈر(s)</translation>
    </message>
    <message>
        <location filename="../deepin-compressor/source/src/mainwindow.cpp" line="243"/>
        <source>Confirm</source>
        <translation>جەزىملەشتۈرۈڭ</translation>
    </message>
    <message>
        <location filename="../deepin-compressor/source/src/mainwindow.cpp" line="715"/>
        <source>%1 changed. Do you want to save changes to the archive?</source>
        <translation>% 1 ئۆزگەردى. ئارخىپقا ئۆزگەرتىش كىرگۈزمەكچىمۇ؟</translation>
    </message>
    <message>
        <location filename="../deepin-compressor/source/src/mainwindow.cpp" line="729"/>
        <source>Discard</source>
        <translation>تاشلىۋېتىش</translation>
    </message>
    <message>
        <location filename="../deepin-compressor/source/src/fileViewer.cpp" line="1554"/>
        <source>Cancel</source>
        <translation>ئەمەلدىن قالدۇرۇش</translation>
    </message>
    <message>
        <location filename="../deepin-compressor/source/src/mainwindow.cpp" line="730"/>
        <source>Save</source>
        <translation>ساقلاش</translation>
    </message>
    <message>
        <location filename="../3rdparty/interface/queries.cpp" line="188"/>
        <source>Another file with the same name already exists, replace it?</source>
        <translation>ئوخشاش ئىسىمدىكى باشقا بىر ھۆججەت بۇرۇنلا مەۋجۇت ، ئۇنى ئالماشتۇرامسىز؟</translation>
    </message>
    <message>
        <location filename="../3rdparty/interface/queries.cpp" line="190"/>
        <source>Skip</source>
        <translation>ئاتلاش</translation>
    </message>
    <message>
        <location filename="../3rdparty/interface/queries.cpp" line="191"/>
        <source>Replace</source>
        <translation>ئالماشتۇرۇڭ</translation>
    </message>
    <message>
        <location filename="../3rdparty/interface/queries.cpp" line="196"/>
        <source>Apply to all</source>
        <translation>ھەممەيلەنگە ئىلتىماس قىلىڭ</translation>
    </message>
    <message>
        <location filename="../3rdparty/interface/queries.cpp" line="367"/>
        <location filename="../3rdparty/interface/queries.cpp" line="456"/>
        <source>OK</source>
        <translation>تاماملاندى</translation>
    </message>
    <message>
        <location filename="../3rdparty/interface/queries.cpp" line="454"/>
        <source>Wrong password</source>
        <translation>مەخپىي نومۇر خاتا</translation>
    </message>
</context>
<context>
    <name>SettingDialog</name>
    <message>
        <location filename="../deepin-compressor/source/src/settingdialog.cpp" line="133"/>
        <source>Select All</source>
        <translation>ھەممىنى تاللاڭ</translation>
    </message>
    <message>
        <location filename="../deepin-compressor/source/src/settingdialog.cpp" line="172"/>
        <location filename="../deepin-compressor/source/src/settingdialog.cpp" line="201"/>
        <source>Current directory</source>
        <translation>نۆۋەتتىكى مۇندەرىجە</translation>
    </message>
    <message>
        <location filename="../deepin-compressor/source/src/settingdialog.cpp" line="134"/>
        <source>Clear All</source>
        <translation>ھەممىنى تازىلاش</translation>
    </message>
    <message>
        <location filename="../deepin-compressor/source/src/settingdialog.cpp" line="135"/>
        <source>Recommended</source>
        <translation>تەۋسىيەلىك تاللاش</translation>
    </message>
    <message>
        <location filename="../deepin-compressor/source/src/settingdialog.cpp" line="166"/>
        <source>Extract archives to</source>
        <translation>ئارخىپلارنى چىقىرىڭ</translation>
    </message>
    <message>
        <location filename="../deepin-compressor/source/src/settingdialog.cpp" line="172"/>
        <location filename="../deepin-compressor/source/src/settingdialog.cpp" line="211"/>
        <source>Other directory</source>
        <translation>باشقا مۇندەرىجە</translation>
    </message>
    <message>
        <location filename="../deepin-compressor/source/src/settingdialog.cpp" line="172"/>
        <location filename="../deepin-compressor/source/src/settingdialog.cpp" line="206"/>
        <source>Desktop</source>
        <translation>ئۈستەل يۈزى</translation>
    </message>
    <message>
        <location filename="../deepin-compressor/source/src/settingdialog.cpp" line="275"/>
        <source>Delete archives after extraction</source>
        <translation>ھۆججەتلەرنى يېشىپ بولۇپ يۇيۇۋىتىش</translation>
    </message>
    <message>
        <location filename="../deepin-compressor/source/src/settingdialog.cpp" line="281"/>
        <location filename="../deepin-compressor/source/src/settingdialog.cpp" line="302"/>
        <source>Never</source>
        <translation>ھەرگىز</translation>
    </message>
    <message>
        <location filename="../deepin-compressor/source/src/settingdialog.cpp" line="281"/>
        <location filename="../deepin-compressor/source/src/settingdialog.cpp" line="305"/>
        <source>Ask for confirmation</source>
        <translation>جەزملەشتۈرۈشنى سوراڭ</translation>
    </message>
    <message>
        <location filename="../deepin-compressor/source/src/settingdialog.cpp" line="281"/>
        <location filename="../deepin-compressor/source/src/settingdialog.cpp" line="308"/>
        <source>Always</source>
        <translation>مەڭگۈ</translation>
    </message>
    <message>
        <location filename="../deepin-compressor/source/src/settingdialog.cpp" line="355"/>
        <source>The default extraction path does not exist, please retry</source>
        <translation>سۈكۈتتىكى ئېلىش يولى مەۋجۇت ئەمەس ، قايتا سىناڭ</translation>
    </message>
</context>
<context>
    <name>UnCompressPage</name>
    <message>
        <location filename="../deepin-compressor/source/src/uncompresspage.cpp" line="53"/>
        <source>Extract</source>
        <translation>ئېرىشىش</translation>
    </message>
    <message>
        <location filename="../deepin-compressor/source/src/uncompresspage.cpp" line="59"/>
        <location filename="../deepin-compressor/source/src/uncompresspage.cpp" line="150"/>
        <location filename="../deepin-compressor/source/src/uncompresspage.cpp" line="161"/>
        <source>Extract to:</source>
        <translation>ئېرىشكەنلىرى:</translation>
    </message>
    <message>
        <location filename="../deepin-compressor/source/src/uncompresspage.cpp" line="113"/>
        <source>The default extraction path does not exist, please retry</source>
        <translation>سۈكۈتتىكى ئېلىش يولى مەۋجۇت ئەمەس ، قايتا سىناڭ</translation>
    </message>
    <message>
        <location filename="../deepin-compressor/source/src/uncompresspage.cpp" line="115"/>
        <source>You do not have permission to save files here, please change and retry</source>
        <translation>بۇ يەردە ھۆججەتلەرنى ساقلاش ھوقۇقىڭىز يوق ، ئۆزگەرتىپ قايتا سىناڭ</translation>
    </message>
    <message>
        <location filename="../deepin-compressor/source/src/uncompresspage.cpp" line="134"/>
        <source>Find directory</source>
        <translation>مۇندەرىجىنى تېپىڭ</translation>
    </message>
    <message>
        <location filename="../deepin-compressor/source/src/uncompresspage.cpp" line="187"/>
        <source>OK</source>
        <translation>تاماملاندى</translation>
    </message>
    <message>
        <location filename="../deepin-compressor/source/src/uncompresspage.cpp" line="561"/>
        <source>Cancel</source>
        <translation>ئەمەلدىن قالدۇرۇش</translation>
    </message>
    <message>
        <location filename="../deepin-compressor/source/src/uncompresspage.cpp" line="562"/>
        <source>Convert</source>
        <translation>ئايلاندۇرۇش</translation>
    </message>
    <message>
        <location filename="../deepin-compressor/source/src/uncompresspage.cpp" line="572"/>
        <source>Changes to archives in this file type are not supported. Please convert the archive format to save the changes.</source>
        <translation>بۇ ھۆججەت تىپىدىكى ئارخىپلارنى ئۆزگەرتىشنى قوللىمايدۇ. ئۆزگەرتىشلەرنى ساقلاش ئۈچۈن ئارخىپ فورماتىنى ئۆزگەرتىڭ.</translation>
    </message>
    <message>
        <location filename="../deepin-compressor/source/src/uncompresspage.cpp" line="588"/>
        <source>Convert the format to:</source>
        <translation>فورماتىنى ئۆزگەرتىش:</translation>
    </message>
</context>
<context>
    <name>fileViewer</name>
    <message>
        <location filename="../deepin-compressor/source/src/fileViewer.cpp" line="1739"/>
        <source>Extract</source>
        <translation>ئېرىشىش</translation>
    </message>
    <message>
        <location filename="../deepin-compressor/source/src/fileViewer.cpp" line="606"/>
        <location filename="../deepin-compressor/source/src/fileViewer.cpp" line="1747"/>
        <location filename="../deepin-compressor/source/src/fileViewer.cpp" line="1762"/>
        <source>Delete</source>
        <translation>ئۆچۈرۈش</translation>
    </message>
    <message>
        <location filename="../deepin-compressor/source/src/fileViewer.cpp" line="589"/>
        <location filename="../deepin-compressor/source/src/fileViewer.cpp" line="607"/>
        <location filename="../deepin-compressor/source/src/fileViewer.cpp" line="1745"/>
        <location filename="../deepin-compressor/source/src/fileViewer.cpp" line="1758"/>
        <source>Open</source>
        <translation>ئېچىش</translation>
    </message>
    <message>
        <location filename="../deepin-compressor/source/src/fileViewer.cpp" line="685"/>
        <source>item(s)</source>
        <translation>تۈر(s)</translation>
    </message>
    <message>
        <location filename="../deepin-compressor/source/src/fileViewer.cpp" line="707"/>
        <source>yyyy/MM/dd hh:mm:ss</source>
        <translation>yyyy/MM/dd hh:mm:ss</translation>
    </message>
    <message>
        <location filename="../deepin-compressor/source/src/fileViewer.cpp" line="1080"/>
        <location filename="../deepin-compressor/source/src/fileViewer.cpp" line="1346"/>
        <source>Confirm</source>
        <translation>جەزىملەشتۈرۈڭ</translation>
    </message>
    <message>
        <location filename="../deepin-compressor/source/src/fileViewer.cpp" line="1078"/>
        <location filename="../deepin-compressor/source/src/fileViewer.cpp" line="1345"/>
        <source>Cancel</source>
        <translation>ئەمەلدىن قالدۇرۇش</translation>
    </message>
    <message>
        <location filename="../deepin-compressor/source/src/fileViewer.cpp" line="590"/>
        <location filename="../deepin-compressor/source/src/fileViewer.cpp" line="1747"/>
        <source>Delete</source>
        <comment>slotDecompressRowDelete</comment>
        <translation>ئۆچۈرۈش</translation>
    </message>
    <message>
        <location filename="../deepin-compressor/source/src/fileViewer.cpp" line="592"/>
        <location filename="../deepin-compressor/source/src/fileViewer.cpp" line="609"/>
        <source>Open with</source>
        <translation>بىلەن ئېچىش</translation>
    </message>
    <message>
        <location filename="../deepin-compressor/source/src/fileViewer.cpp" line="744"/>
        <location filename="../deepin-compressor/source/src/fileViewer.cpp" line="1776"/>
        <location filename="../deepin-compressor/source/src/fileViewer.cpp" line="1796"/>
        <source>Select default program</source>
        <translation>سۈكۈتتىكى پىروگراممىنى تاللاش</translation>
    </message>
    <message>
        <location filename="../deepin-compressor/source/src/fileViewer.cpp" line="956"/>
        <location filename="../deepin-compressor/source/src/fileViewer.cpp" line="1749"/>
        <source>Do you want to delete the selected file(s)?</source>
        <translation>تاللانغان ھۆججەتلەرنى يۇيۇۋەتمەكچىمۇ؟</translation>
    </message>
    <message>
        <location filename="../deepin-compressor/source/src/fileViewer.cpp" line="1122"/>
        <source>It will permanently delete the file(s). Are you sure you want to continue?</source>
        <translation>ئۇ ھۆججەت (لەرنى) مەڭگۈلۈك ئۆچۈرۈۋېتىدۇ. راستىنلا داۋاملاشتۇرامسىز؟</translation>
    </message>
    <message>
        <location filename="../deepin-compressor/source/src/fileViewer.cpp" line="1534"/>
        <source>Files have been changed. Do you want to update the changes to %1?</source>
        <translation>ھۆججەتلەر ئۆزگەرتىلدى. ئۆزگەرتىشلەرنى% 1 گە يېڭىلاشنى خالامسىز؟</translation>
    </message>
    <message>
        <location filename="../deepin-compressor/source/src/fileViewer.cpp" line="1082"/>
        <source>Update</source>
        <translation>يىڭىلاش</translation>
    </message>
    <message>
        <location filename="../deepin-compressor/source/src/fileViewer.cpp" line="587"/>
        <location filename="../deepin-compressor/source/src/fileViewer.cpp" line="1739"/>
        <source>Extract</source>
        <comment>slotDecompressRowDoubleClicked</comment>
        <translation>ئېرىشىش</translation>
    </message>
    <message>
        <location filename="../deepin-compressor/source/src/fileViewer.cpp" line="588"/>
        <location filename="../deepin-compressor/source/src/fileViewer.cpp" line="1741"/>
        <source>Extract to current directory</source>
        <translation>ئېرىشكەن نۆۋەتتىكى مۇندەرىجە</translation>
    </message>
</context>
</TS>