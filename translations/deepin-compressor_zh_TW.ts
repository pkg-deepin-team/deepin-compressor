<?xml version="1.0" ?><!DOCTYPE TS><TS language="zh_TW" version="2.1">
<context>
    <name>ArchiveModel</name>
    <message>
        <location filename="../deepin-compressor/source/src/archivemodel.cpp" line="119"/>
        <location filename="../deepin-compressor/source/src/archivemodel.cpp" line="121"/>
        <source>item(s)</source>
        <translation>項</translation>
    </message>
    <message>
        <location filename="../deepin-compressor/source/src/archivemodel.cpp" line="132"/>
        <source>yyyy/MM/dd hh:mm:ss</source>
        <translation>yyyy/MM/dd hh:mm:ss</translation>
    </message>
    <message>
        <location filename="../deepin-compressor/source/src/archivemodel.cpp" line="200"/>
        <source>Name</source>
        <translation>名稱</translation>
    </message>
    <message>
        <location filename="../deepin-compressor/source/src/archivemodel.cpp" line="202"/>
        <source>Size</source>
        <translation>大小</translation>
    </message>
    <message>
        <location filename="../deepin-compressor/source/src/archivemodel.cpp" line="204"/>
        <source>Type</source>
        <translation>類型</translation>
    </message>
    <message>
        <location filename="../deepin-compressor/source/src/archivemodel.cpp" line="206"/>
        <source>Time modified</source>
        <translation>修改時間</translation>
    </message>
</context>
<context>
    <name>CompressPage</name>
    <message>
        <location filename="../deepin-compressor/source/src/compresspage.cpp" line="48"/>
        <source>Next</source>
        <translation>下一步</translation>
    </message>
    <message>
        <location filename="../deepin-compressor/source/src/compresspage.cpp" line="93"/>
        <source>Please add files</source>
        <translation>請添加文件</translation>
    </message>
    <message>
        <location filename="../deepin-compressor/source/src/compresspage.cpp" line="94"/>
        <location filename="../deepin-compressor/source/src/compresspage.cpp" line="116"/>
        <source>OK</source>
        <translation>確定</translation>
    </message>
    <message>
        <location filename="../deepin-compressor/source/src/compresspage.cpp" line="118"/>
        <source>Please add files to the top-level directory</source>
        <translation>請在根目錄添加文件</translation>
    </message>
</context>
<context>
    <name>CompressSetting</name>
    <message>
        <location filename="../deepin-compressor/source/src/compresssetting.cpp" line="236"/>
        <source>Compress</source>
        <translation>壓縮</translation>
    </message>
    <message>
        <location filename="../deepin-compressor/source/src/compresssetting.cpp" line="164"/>
        <source>Advanced Options</source>
        <translation>進階選項</translation>
    </message>
    <message>
        <location filename="../deepin-compressor/source/src/compresssetting.cpp" line="158"/>
        <source>Name</source>
        <translation>檔案名</translation>
    </message>
    <message>
        <location filename="../deepin-compressor/source/src/compresssetting.cpp" line="159"/>
        <source>Save to</source>
        <translation>儲存到</translation>
    </message>
    <message>
        <location filename="../deepin-compressor/source/src/compresssetting.cpp" line="170"/>
        <source>Encrypt the archive</source>
        <translation>加密文件</translation>
    </message>
    <message>
        <location filename="../deepin-compressor/source/src/compresssetting.cpp" line="171"/>
        <source>Support zip, 7z type only</source>
        <translation>僅支援zip, 7z格式</translation>
    </message>
    <message>
        <location filename="../deepin-compressor/source/src/compresssetting.cpp" line="176"/>
        <source>Password</source>
        <translation>請輸入密碼</translation>
    </message>
    <message>
        <location filename="../deepin-compressor/source/src/compresssetting.cpp" line="178"/>
        <source>Encrypt the file list too</source>
        <translation>加密文件列表</translation>
    </message>
    <message>
        <location filename="../deepin-compressor/source/src/compresssetting.cpp" line="179"/>
        <location filename="../deepin-compressor/source/src/compresssetting.cpp" line="187"/>
        <source>Support 7z type only</source>
        <translation>僅支援7z格式</translation>
    </message>
    <message>
        <location filename="../deepin-compressor/source/src/compresssetting.cpp" line="185"/>
        <source>Split to volumes</source>
        <translation>分卷壓縮</translation>
    </message>
    <message>
        <location filename="../deepin-compressor/source/src/compresssetting.cpp" line="335"/>
        <location filename="../deepin-compressor/source/src/compresssetting.cpp" line="830"/>
        <location filename="../deepin-compressor/source/src/compresssetting.cpp" line="966"/>
        <location filename="../deepin-compressor/source/src/compresssetting.cpp" line="1099"/>
        <source>%1 was changed on the disk, please import it again.</source>
        <translation>“%1”已經發生變化，請重新匯入文件。</translation>
    </message>
    <message>
        <location filename="../deepin-compressor/source/src/compresssetting.cpp" line="353"/>
        <location filename="../deepin-compressor/source/src/compresssetting.cpp" line="359"/>
        <location filename="../deepin-compressor/source/src/compresssetting.cpp" line="364"/>
        <location filename="../deepin-compressor/source/src/compresssetting.cpp" line="837"/>
        <location filename="../deepin-compressor/source/src/compresssetting.cpp" line="843"/>
        <location filename="../deepin-compressor/source/src/compresssetting.cpp" line="848"/>
        <location filename="../deepin-compressor/source/src/compresssetting.cpp" line="973"/>
        <location filename="../deepin-compressor/source/src/compresssetting.cpp" line="979"/>
        <location filename="../deepin-compressor/source/src/compresssetting.cpp" line="984"/>
        <location filename="../deepin-compressor/source/src/compresssetting.cpp" line="1106"/>
        <location filename="../deepin-compressor/source/src/compresssetting.cpp" line="1112"/>
        <location filename="../deepin-compressor/source/src/compresssetting.cpp" line="1117"/>
        <source>You do not have permission to compress %1</source>
        <translation>您沒有權限壓縮“%1”文件</translation>
    </message>
    <message>
        <location filename="../deepin-compressor/source/src/compresssetting.cpp" line="373"/>
        <location filename="../deepin-compressor/source/src/compresssetting.cpp" line="857"/>
        <location filename="../deepin-compressor/source/src/compresssetting.cpp" line="993"/>
        <source>You do not have permission to save files here, please change and retry</source>
        <translation>您沒有權限在此路徑儲存文件，請重試</translation>
    </message>
    <message>
        <location filename="../deepin-compressor/source/src/compresssetting.cpp" line="378"/>
        <source>Too many volumes, please change and retry</source>
        <translation>分卷過多，請更改後重試</translation>
    </message>
    <message>
        <location filename="../deepin-compressor/source/src/compresssetting.cpp" line="478"/>
        <source>Files that begin with &apos;@&apos; cannot be compressed</source>
        <translation>不能壓縮以@開頭的文件</translation>
    </message>
    <message>
        <location filename="../deepin-compressor/source/src/compresssetting.cpp" line="702"/>
        <source>Total size: %1</source>
        <translation>文件總大小：%1</translation>
    </message>
    <message>
        <location filename="../deepin-compressor/source/src/compresssetting.cpp" line="801"/>
        <location filename="../deepin-compressor/source/src/compresssetting.cpp" line="937"/>
        <location filename="../deepin-compressor/source/src/compresssetting.cpp" line="1070"/>
        <source>You cannot add the archive to itself</source>
        <translation>無法將壓縮文件添加到自身</translation>
    </message>
    <message>
        <location filename="../deepin-compressor/source/src/compresssetting.cpp" line="1279"/>
        <source>Another file with the same name already exists, replace it?</source>
        <translation>文件已存在，是否取代？</translation>
    </message>
    <message>
        <location filename="../deepin-compressor/source/src/compresssetting.cpp" line="800"/>
        <source>Close</source>
        <translation>關閉</translation>
    </message>
    <message>
        <location filename="../deepin-compressor/source/src/compresssetting.cpp" line="936"/>
        <location filename="../deepin-compressor/source/src/compresssetting.cpp" line="1203"/>
        <source>OK</source>
        <translation>確定</translation>
    </message>
    <message>
        <location filename="../deepin-compressor/source/src/compresssetting.cpp" line="1281"/>
        <source>Cancel</source>
        <translation>取消</translation>
    </message>
    <message>
        <location filename="../deepin-compressor/source/src/compresssetting.cpp" line="1282"/>
        <source>Replace</source>
        <translation>取代</translation>
    </message>
    <message>
        <location filename="../deepin-compressor/source/src/compresssetting.cpp" line="322"/>
        <location filename="../deepin-compressor/source/src/compresssetting.cpp" line="817"/>
        <location filename="../deepin-compressor/source/src/compresssetting.cpp" line="953"/>
        <location filename="../deepin-compressor/source/src/compresssetting.cpp" line="1086"/>
        <source>Please enter the path</source>
        <translation>請填寫儲存路徑</translation>
    </message>
    <message>
        <location filename="../deepin-compressor/source/src/compresssetting.cpp" line="149"/>
        <source>New Archive</source>
        <translation>歸檔文件</translation>
    </message>
    <message>
        <location filename="../deepin-compressor/source/src/compresssetting.cpp" line="317"/>
        <location filename="../deepin-compressor/source/src/compresssetting.cpp" line="812"/>
        <location filename="../deepin-compressor/source/src/compresssetting.cpp" line="948"/>
        <location filename="../deepin-compressor/source/src/compresssetting.cpp" line="1081"/>
        <source>Invalid file name</source>
        <translation>請輸入有效的檔案名</translation>
    </message>
    <message>
        <location filename="../deepin-compressor/source/src/compresssetting.cpp" line="327"/>
        <location filename="../deepin-compressor/source/src/compresssetting.cpp" line="822"/>
        <location filename="../deepin-compressor/source/src/compresssetting.cpp" line="958"/>
        <location filename="../deepin-compressor/source/src/compresssetting.cpp" line="1091"/>
        <source>The path does not exist, please retry</source>
        <translation>目前路徑不存在，請重試</translation>
    </message>
</context>
<context>
    <name>Compressor_Fail</name>
    <message>
        <location filename="../deepin-compressor/source/src/compressor_fail.cpp" line="35"/>
        <source>Extraction failed</source>
        <translation>解壓失敗</translation>
    </message>
    <message>
        <location filename="../deepin-compressor/source/src/compressor_fail.cpp" line="36"/>
        <source>Damaged file, unable to extract</source>
        <translation>文件損壞，無法解壓</translation>
    </message>
    <message>
        <location filename="../deepin-compressor/source/src/compressor_fail.cpp" line="61"/>
        <source>Retry</source>
        <translation>重試</translation>
    </message>
    <message>
        <location filename="../deepin-compressor/source/src/compressor_fail.cpp" line="63"/>
        <source>Back</source>
        <translation>返回</translation>
    </message>
</context>
<context>
    <name>Compressor_Success</name>
    <message>
        <location filename="../deepin-compressor/source/src/compressor_success.cpp" line="40"/>
        <source>Compression successful</source>
        <translation>壓縮成功</translation>
    </message>
    <message>
        <location filename="../deepin-compressor/source/src/compressor_success.cpp" line="58"/>
        <source>View</source>
        <translation>查看文件</translation>
    </message>
    <message>
        <location filename="../deepin-compressor/source/src/compressor_success.cpp" line="60"/>
        <source>Back</source>
        <translation>返回</translation>
    </message>
</context>
<context>
    <name>EncodingPage</name>
    <message>
        <location filename="../deepin-compressor/source/src/encodingpage.cpp" line="65"/>
        <source>Filename Encoding</source>
        <translation>文件名编码</translation>
    </message>
    <message>
        <location filename="../deepin-compressor/source/src/encodingpage.cpp" line="78"/>
        <source>Select an encoding to continue</source>
        <translation>請選擇檔案名編碼以解壓此文件</translation>
    </message>
    <message>
        <location filename="../deepin-compressor/source/src/encodingpage.cpp" line="82"/>
        <source>Cancel</source>
        <translation>取消</translation>
    </message>
    <message>
        <location filename="../deepin-compressor/source/src/encodingpage.cpp" line="83"/>
        <source>Select Character Encoding</source>
        <translation>選擇語言編碼</translation>
    </message>
</context>
<context>
    <name>EncryptionPage</name>
    <message>
        <location filename="../deepin-compressor/source/src/encryptionpage.cpp" line="47"/>
        <source>Encrypted file, please enter the password</source>
        <translation>此文件已加密，請輸入解壓密碼</translation>
    </message>
    <message>
        <location filename="../deepin-compressor/source/src/encryptionpage.cpp" line="50"/>
        <source>Next</source>
        <translation>下一步</translation>
    </message>
    <message>
        <location filename="../deepin-compressor/source/src/encryptionpage.cpp" line="55"/>
        <source>Password</source>
        <translation>請輸入密碼</translation>
    </message>
    <message>
        <location filename="../deepin-compressor/source/src/encryptionpage.cpp" line="135"/>
        <source>Wrong password</source>
        <translation>密碼錯誤</translation>
    </message>
</context>
<context>
    <name>ExtractPauseDialog</name>
    <message>
        <location filename="../deepin-compressor/source/src/extractpausedialog.cpp" line="67"/>
        <source>Are you sure you want to stop the extraction?</source>
        <translation>您確定要停止解壓文件嗎？</translation>
    </message>
    <message>
        <location filename="../deepin-compressor/source/src/extractpausedialog.cpp" line="69"/>
        <source>Cancel</source>
        <translation>取消</translation>
    </message>
    <message>
        <location filename="../deepin-compressor/source/src/extractpausedialog.cpp" line="70"/>
        <source>Confirm</source>
        <translation>確定</translation>
    </message>
</context>
<context>
    <name>HomePage</name>
    <message>
        <location filename="../deepin-compressor/source/src/homepage.cpp" line="40"/>
        <source>Drag file or folder here</source>
        <translation>拖曳文件（夾）到此</translation>
    </message>
    <message>
        <location filename="../deepin-compressor/source/src/homepage.cpp" line="42"/>
        <source>Select File</source>
        <translation>選擇文件</translation>
    </message>
</context>
<context>
    <name>LogViewHeaderView</name>
    <message>
        <location filename="../deepin-compressor/source/src/logviewheaderview.cpp" line="160"/>
        <source>Back</source>
        <translation>返回</translation>
    </message>
</context>
<context>
    <name>Main</name>
    <message>
        <location filename="../deepin-compressor/main.cpp" line="145"/>
        <location filename="../tests/UnitTest/deepin-compressor/source/src/test_main.cpp" line="150"/>
        <source>Archive Manager</source>
        <translation>歸檔管理器</translation>
    </message>
    <message>
        <location filename="../deepin-compressor/main.cpp" line="146"/>
        <location filename="../tests/UnitTest/deepin-compressor/source/src/test_main.cpp" line="151"/>
        <source>Archive Manager is a fast and lightweight application for creating and extracting archives.</source>
        <translation>歸檔管理器是一款快速、輕巧的解壓縮軟體，提供對文件解壓縮的常用功能。</translation>
    </message>
</context>
<context>
    <name>MainWindow</name>
    <message>
        <location filename="../deepin-compressor/source/src/mainwindow.cpp" line="564"/>
        <source>Close</source>
        <translation>關閉</translation>
    </message>
    <message>
        <location filename="../deepin-compressor/source/src/mainwindow.cpp" line="568"/>
        <source>Help</source>
        <translation>說明</translation>
    </message>
    <message>
        <location filename="../deepin-compressor/source/src/mainwindow.cpp" line="572"/>
        <source>Select the file</source>
        <translation>開啟</translation>
    </message>
    <message>
        <location filename="../deepin-compressor/source/src/mainwindow.cpp" line="576"/>
        <source>Delete</source>
        <translation>刪除</translation>
    </message>
    <message>
        <location filename="../deepin-compressor/source/src/mainwindow.cpp" line="596"/>
        <source>Shortcuts</source>
        <translation>快捷鍵</translation>
    </message>
    <message>
        <location filename="../deepin-compressor/source/src/mainwindow.cpp" line="781"/>
        <source>Open file</source>
        <translation>開啟文件</translation>
    </message>
    <message>
        <location filename="../deepin-compressor/source/src/mainwindow.cpp" line="822"/>
        <source>Settings</source>
        <translation>設定</translation>
    </message>
    <message>
        <location filename="../deepin-compressor/source/src/mainwindow.cpp" line="1284"/>
        <location filename="../deepin-compressor/source/src/mainwindow.cpp" line="1297"/>
        <location filename="../deepin-compressor/source/src/mainwindow.cpp" line="2961"/>
        <source>Create New Archive</source>
        <translation>建立歸檔文件</translation>
    </message>
    <message>
        <location filename="../deepin-compressor/source/src/mainwindow.cpp" line="1338"/>
        <source>Opening</source>
        <translation>正在開啟</translation>
    </message>
    <message>
        <location filename="../deepin-compressor/source/src/mainwindow.cpp" line="1366"/>
        <source>Converting</source>
        <translation>正在轉換</translation>
    </message>
    <message>
        <location filename="../deepin-compressor/source/src/mainwindow.cpp" line="1375"/>
        <source>Compression successful</source>
        <translation>壓縮成功</translation>
    </message>
    <message>
        <location filename="../deepin-compressor/source/src/mainwindow.cpp" line="5119"/>
        <source>Confirm</source>
        <translation>確定</translation>
    </message>
    <message>
        <location filename="../deepin-compressor/source/src/mainwindow.cpp" line="5122"/>
        <source>Do you want to delete the archive?</source>
        <translation>您是否要刪除此壓縮文件？</translation>
    </message>
    <message>
        <location filename="../deepin-compressor/source/src/mainwindow.cpp" line="2573"/>
        <location filename="../deepin-compressor/source/src/mainwindow.cpp" line="2669"/>
        <location filename="../deepin-compressor/source/src/mainwindow.cpp" line="2703"/>
        <source>Skip all files</source>
        <translation>跳過所有文件</translation>
    </message>
    <message>
        <location filename="../deepin-compressor/source/src/mainwindow.cpp" line="2546"/>
        <location filename="../deepin-compressor/source/src/mainwindow.cpp" line="2705"/>
        <source>Extraction successful</source>
        <translation>解壓成功</translation>
    </message>
    <message>
        <location filename="../deepin-compressor/source/src/mainwindow.cpp" line="469"/>
        <source>%1 was changed on the disk, please import it again.</source>
        <translation>“%1”已經發生變化，請重新匯入文件。</translation>
    </message>
    <message>
        <location filename="../deepin-compressor/source/src/mainwindow.cpp" line="1631"/>
        <source>Do you want to add the archive to the list or open it in new window?</source>
        <translation>添加壓縮文件到目錄或在新視窗中開啟該文件？</translation>
    </message>
    <message>
        <location filename="../deepin-compressor/source/src/mainwindow.cpp" line="584"/>
        <source>Display shortcuts</source>
        <translation>顯示快捷鍵</translation>
    </message>
    <message>
        <location filename="../deepin-compressor/source/src/mainwindow.cpp" line="242"/>
        <location filename="../deepin-compressor/source/src/mainwindow.cpp" line="1632"/>
        <location filename="../deepin-compressor/source/src/mainwindow.cpp" line="5118"/>
        <source>Cancel</source>
        <translation>取消</translation>
    </message>
    <message>
        <location filename="../deepin-compressor/source/src/mainwindow.cpp" line="114"/>
        <source>Archive Manager</source>
        <translation>歸檔管理器</translation>
    </message>
    <message>
        <location filename="../deepin-compressor/source/src/mainwindow.cpp" line="475"/>
        <location filename="../deepin-compressor/source/src/mainwindow.cpp" line="2246"/>
        <location filename="../deepin-compressor/source/src/mainwindow.cpp" line="4916"/>
        <source>OK</source>
        <translation>確定</translation>
    </message>
    <message>
        <location filename="../deepin-compressor/source/src/mainwindow.cpp" line="1316"/>
        <source>Adding files to %1</source>
        <translation>正在向%1添加文件</translation>
    </message>
    <message>
        <location filename="../deepin-compressor/source/src/mainwindow.cpp" line="1319"/>
        <source>Compressing</source>
        <translation>正在壓縮</translation>
    </message>
    <message>
        <location filename="../deepin-compressor/source/src/mainwindow.cpp" line="1340"/>
        <source>Extracting</source>
        <translation>正在壓縮</translation>
    </message>
    <message>
        <location filename="../deepin-compressor/source/src/mainwindow.cpp" line="1354"/>
        <source>Deleting</source>
        <translation>正在刪除</translation>
    </message>
    <message>
        <location filename="../deepin-compressor/source/src/mainwindow.cpp" line="1392"/>
        <source>Compression failed</source>
        <translation>壓縮失敗</translation>
    </message>
    <message>
        <location filename="../deepin-compressor/source/src/mainwindow.cpp" line="1424"/>
        <source>Extraction failed</source>
        <translation>解壓失敗</translation>
    </message>
    <message>
        <location filename="../deepin-compressor/source/src/mainwindow.cpp" line="1437"/>
        <source>Conversion successful</source>
        <translation>轉換成功</translation>
    </message>
    <message>
        <location filename="../deepin-compressor/source/src/mainwindow.cpp" line="1633"/>
        <source>Add</source>
        <translation>添加</translation>
    </message>
    <message>
        <location filename="../deepin-compressor/source/src/mainwindow.cpp" line="1634"/>
        <source>Open in new window</source>
        <translation>在新視窗中開啟</translation>
    </message>
    <message>
        <location filename="../deepin-compressor/source/src/mainwindow.cpp" line="1841"/>
        <source>Find directory</source>
        <translation>解壓到目錄</translation>
    </message>
    <message>
        <location filename="../deepin-compressor/source/src/mainwindow.cpp" line="2167"/>
        <location filename="../deepin-compressor/source/src/mainwindow.cpp" line="2598"/>
        <source>Failed to open the archive: %1</source>
        <translation>開啟壓縮文件&quot;%1&quot;失敗</translation>
    </message>
    <message>
        <location filename="../deepin-compressor/source/src/mainwindow.cpp" line="2481"/>
        <location filename="../deepin-compressor/source/src/mainwindow.cpp" line="2600"/>
        <source>Wrong password</source>
        <translation>密碼錯誤</translation>
    </message>
    <message>
        <location filename="../deepin-compressor/source/src/mainwindow.cpp" line="2687"/>
        <source>Select default program</source>
        <translation>選擇預設程式</translation>
    </message>
    <message>
        <location filename="../deepin-compressor/source/src/mainwindow.cpp" line="4918"/>
        <source>Please check the file association type in the settings of Archive Manager</source>
        <translation>請在歸檔管理器的設定中勾選此文件類型</translation>
    </message>
    <message>
        <location filename="../deepin-compressor/source/src/mainwindow.cpp" line="1982"/>
        <location filename="../deepin-compressor/source/src/mainwindow.cpp" line="2005"/>
        <location filename="../deepin-compressor/source/src/mainwindow.cpp" line="2173"/>
        <location filename="../deepin-compressor/source/src/mainwindow.cpp" line="2593"/>
        <source>Damaged file, unable to extract</source>
        <translation>文件損壞，無法解壓</translation>
    </message>
    <message>
        <location filename="../deepin-compressor/source/src/mainwindow.cpp" line="241"/>
        <source>Are you sure you want to stop the ongoing task?</source>
        <translation>您確定要停止正在進行的任務嗎？</translation>
    </message>
    <message>
        <location filename="../deepin-compressor/source/src/mainwindow.cpp" line="2245"/>
        <source>The archive was changed on the disk, please import it again.</source>
        <translation>目前壓縮文件已經發生變化，請重新匯入文件。</translation>
    </message>
    <message>
        <location filename="../deepin-compressor/source/src/mainwindow.cpp" line="2589"/>
        <location filename="../deepin-compressor/source/src/mainwindow.cpp" line="2591"/>
        <location filename="../deepin-compressor/source/src/mainwindow.cpp" line="4028"/>
        <location filename="../deepin-compressor/source/src/mainwindow.cpp" line="4034"/>
        <location filename="../deepin-compressor/source/src/mainwindow.cpp" line="4085"/>
        <location filename="../deepin-compressor/source/src/mainwindow.cpp" line="4091"/>
        <source>Insufficient space, please clear and retry</source>
        <translation>空間不足，請清理後重試</translation>
    </message>
    <message>
        <location filename="../deepin-compressor/source/src/mainwindow.cpp" line="2596"/>
        <source>File name too long, unable to extract</source>
        <translation>檔案名過長，無法解壓</translation>
    </message>
    <message>
        <location filename="../deepin-compressor/source/src/mainwindow.cpp" line="2600"/>
        <source>Unable to extract</source>
        <translation>無法解壓</translation>
    </message>
    <message>
        <location filename="../deepin-compressor/source/src/mainwindow.cpp" line="4030"/>
        <location filename="../deepin-compressor/source/src/mainwindow.cpp" line="4036"/>
        <location filename="../deepin-compressor/source/src/mainwindow.cpp" line="4087"/>
        <location filename="../deepin-compressor/source/src/mainwindow.cpp" line="4093"/>
        <source>Damaged file</source>
        <translation>原始文件已損壞</translation>
    </message>
</context>
<context>
    <name>MimeTypeDisplayManager</name>
    <message>
        <location filename="../deepin-compressor/source/src/mimetypedisplaymanager.cpp" line="46"/>
        <source>Directory</source>
        <translation>目錄</translation>
    </message>
    <message>
        <location filename="../deepin-compressor/source/src/mimetypedisplaymanager.cpp" line="47"/>
        <source>Application</source>
        <translation>應用程式</translation>
    </message>
    <message>
        <location filename="../deepin-compressor/source/src/mimetypedisplaymanager.cpp" line="48"/>
        <source>Video</source>
        <translation>影片</translation>
    </message>
    <message>
        <location filename="../deepin-compressor/source/src/mimetypedisplaymanager.cpp" line="49"/>
        <source>Audio</source>
        <translation>音訊</translation>
    </message>
    <message>
        <location filename="../deepin-compressor/source/src/mimetypedisplaymanager.cpp" line="50"/>
        <source>Image</source>
        <translation>圖片</translation>
    </message>
    <message>
        <location filename="../deepin-compressor/source/src/mimetypedisplaymanager.cpp" line="51"/>
        <source>Archive</source>
        <translation>壓縮文件</translation>
    </message>
    <message>
        <location filename="../deepin-compressor/source/src/mimetypedisplaymanager.cpp" line="53"/>
        <source>Executable</source>
        <translation>可執程式</translation>
    </message>
    <message>
        <location filename="../deepin-compressor/source/src/mimetypedisplaymanager.cpp" line="52"/>
        <source>Document</source>
        <translation>文件</translation>
    </message>
    <message>
        <location filename="../deepin-compressor/source/src/mimetypedisplaymanager.cpp" line="54"/>
        <source>Backup file</source>
        <translation>備份文件</translation>
    </message>
    <message>
        <location filename="../deepin-compressor/source/src/mimetypedisplaymanager.cpp" line="55"/>
        <source>Unknown</source>
        <translation>未知</translation>
    </message>
</context>
<context>
    <name>MyFileSystemModel</name>
    <message>
        <location filename="../deepin-compressor/source/src/myfilesystemmodel.cpp" line="63"/>
        <source>Name</source>
        <translation>名稱</translation>
    </message>
    <message>
        <location filename="../deepin-compressor/source/src/myfilesystemmodel.cpp" line="65"/>
        <source>Size</source>
        <translation>大小</translation>
    </message>
    <message>
        <location filename="../deepin-compressor/source/src/myfilesystemmodel.cpp" line="67"/>
        <source>Type</source>
        <translation>類型</translation>
    </message>
    <message>
        <location filename="../deepin-compressor/source/src/myfilesystemmodel.cpp" line="69"/>
        <source>Time modified</source>
        <translation>修改時間</translation>
    </message>
    <message>
        <location filename="../deepin-compressor/source/src/myfilesystemmodel.cpp" line="127"/>
        <source>item(s)</source>
        <translation>項</translation>
    </message>
    <message>
        <location filename="../deepin-compressor/source/src/myfilesystemmodel.cpp" line="138"/>
        <source>yyyy/MM/dd hh:mm:ss</source>
        <translation>yyyy/MM/dd hh:mm:ss</translation>
    </message>
</context>
<context>
    <name>OpenLoadingPage</name>
    <message>
        <location filename="../deepin-compressor/source/src/openloadingpage.cpp" line="52"/>
        <source>Loading, please wait...</source>
        <translation>正在載入，請稍候...</translation>
    </message>
</context>
<context>
    <name>OpenWithDialog</name>
    <message>
        <location filename="../deepin-compressor/source/src/openwithdialog/openwithdialog.cpp" line="308"/>
        <source>Open with</source>
        <translation>開啟方式</translation>
    </message>
    <message>
        <location filename="../deepin-compressor/source/src/openwithdialog/openwithdialog.cpp" line="327"/>
        <source>Add other programs</source>
        <translation>添加其他程式</translation>
    </message>
    <message>
        <location filename="../deepin-compressor/source/src/openwithdialog/openwithdialog.cpp" line="328"/>
        <source>Set as default</source>
        <translation>設定預設程式</translation>
    </message>
    <message>
        <location filename="../deepin-compressor/source/src/openwithdialog/openwithdialog.cpp" line="330"/>
        <source>Cancel</source>
        <translation>取消</translation>
    </message>
    <message>
        <location filename="../deepin-compressor/source/src/openwithdialog/openwithdialog.cpp" line="331"/>
        <source>Confirm</source>
        <translation>確定</translation>
    </message>
    <message>
        <location filename="../deepin-compressor/source/src/openwithdialog/openwithdialog.cpp" line="335"/>
        <source>Recommended Applications</source>
        <translation>推薦應用</translation>
    </message>
    <message>
        <location filename="../deepin-compressor/source/src/openwithdialog/openwithdialog.cpp" line="337"/>
        <source>Other Applications</source>
        <translation>其他應用</translation>
    </message>
</context>
<context>
    <name>PasswordNeededQuery</name>
    <message>
        <location filename="../3rdparty/interface/queries.cpp" line="359"/>
        <source>Encrypted file, please enter the password</source>
        <translation>此文件已加密，請輸入解壓密碼</translation>
    </message>
</context>
<context>
    <name>Progress</name>
    <message>
        <location filename="../deepin-compressor/source/src/progress.cpp" line="89"/>
        <location filename="../deepin-compressor/source/src/progress.cpp" line="415"/>
        <source>Cancel</source>
        <translation>取消</translation>
    </message>
    <message>
        <location filename="../deepin-compressor/source/src/progress.cpp" line="85"/>
        <location filename="../deepin-compressor/source/src/progress.cpp" line="148"/>
        <location filename="../deepin-compressor/source/src/progress.cpp" line="150"/>
        <location filename="../deepin-compressor/source/src/progress.cpp" line="152"/>
        <location filename="../deepin-compressor/source/src/progress.cpp" line="154"/>
        <location filename="../deepin-compressor/source/src/progress.cpp" line="156"/>
        <location filename="../deepin-compressor/source/src/progress.cpp" line="159"/>
        <location filename="../deepin-compressor/source/src/progress.cpp" line="442"/>
        <source>Calculating...</source>
        <translation>計算中...</translation>
    </message>
    <message>
        <location filename="../deepin-compressor/source/src/progress.cpp" line="93"/>
        <location filename="../deepin-compressor/source/src/progress.cpp" line="191"/>
        <location filename="../deepin-compressor/source/src/progress.cpp" line="468"/>
        <source>Pause</source>
        <translation>暫停</translation>
    </message>
    <message>
        <location filename="../deepin-compressor/source/src/progress.cpp" line="152"/>
        <location filename="../deepin-compressor/source/src/progress.cpp" line="275"/>
        <location filename="../deepin-compressor/source/src/progress.cpp" line="277"/>
        <source>Speed</source>
        <comment>delete</comment>
        <translation>解壓速度</translation>
    </message>
    <message>
        <location filename="../deepin-compressor/source/src/progress.cpp" line="156"/>
        <location filename="../deepin-compressor/source/src/progress.cpp" line="299"/>
        <location filename="../deepin-compressor/source/src/progress.cpp" line="301"/>
        <location filename="../deepin-compressor/source/src/progress.cpp" line="303"/>
        <source>Speed</source>
        <comment>convert</comment>
        <translation>解壓速度</translation>
    </message>
    <message>
        <location filename="../deepin-compressor/source/src/progress.cpp" line="403"/>
        <source>Are you sure you want to stop the update?</source>
        <translation>您確定要停止此次更新嗎？</translation>
    </message>
    <message>
        <location filename="../deepin-compressor/source/src/progress.cpp" line="405"/>
        <source>Are you sure you want to stop the conversion?</source>
        <translation>您確定取消格式轉換嗎？</translation>
    </message>
    <message>
        <location filename="../deepin-compressor/source/src/progress.cpp" line="342"/>
        <source>Converting</source>
        <translation>正在轉換</translation>
    </message>
    <message>
        <location filename="../deepin-compressor/source/src/progress.cpp" line="345"/>
        <source>Opening</source>
        <translation>正在開啟</translation>
    </message>
    <message>
        <location filename="../deepin-compressor/source/src/progress.cpp" line="401"/>
        <source>Are you sure you want to stop the compression?</source>
        <translation>您確定要停止壓縮文件嗎？</translation>
    </message>
    <message>
        <location filename="../deepin-compressor/source/src/progress.cpp" line="416"/>
        <source>Confirm</source>
        <translation>確定</translation>
    </message>
    <message>
        <location filename="../deepin-compressor/source/src/progress.cpp" line="462"/>
        <source>Continue</source>
        <translation>繼續</translation>
    </message>
    <message>
        <location filename="../deepin-compressor/source/src/progress.cpp" line="411"/>
        <source>Are you sure you want to stop the extraction?</source>
        <translation>您確定要停止解壓文件嗎？</translation>
    </message>
    <message>
        <location filename="../deepin-compressor/source/src/progress.cpp" line="148"/>
        <location filename="../deepin-compressor/source/src/progress.cpp" line="154"/>
        <location filename="../deepin-compressor/source/src/progress.cpp" line="267"/>
        <location filename="../deepin-compressor/source/src/progress.cpp" line="269"/>
        <location filename="../deepin-compressor/source/src/progress.cpp" line="271"/>
        <location filename="../deepin-compressor/source/src/progress.cpp" line="283"/>
        <location filename="../deepin-compressor/source/src/progress.cpp" line="285"/>
        <location filename="../deepin-compressor/source/src/progress.cpp" line="287"/>
        <source>Speed</source>
        <comment>compress</comment>
        <translation>壓縮速度</translation>
    </message>
    <message>
        <location filename="../deepin-compressor/source/src/progress.cpp" line="159"/>
        <location filename="../deepin-compressor/source/src/progress.cpp" line="307"/>
        <source>Time left</source>
        <translation>剩餘時間</translation>
    </message>
    <message>
        <location filename="../deepin-compressor/source/src/progress.cpp" line="150"/>
        <location filename="../deepin-compressor/source/src/progress.cpp" line="291"/>
        <location filename="../deepin-compressor/source/src/progress.cpp" line="293"/>
        <location filename="../deepin-compressor/source/src/progress.cpp" line="295"/>
        <source>Speed</source>
        <comment>uncompress</comment>
        <translation>解壓速度</translation>
    </message>
    <message>
        <location filename="../deepin-compressor/source/src/progress.cpp" line="338"/>
        <source>Compressing</source>
        <translation>正在壓縮</translation>
    </message>
    <message>
        <location filename="../deepin-compressor/source/src/progress.cpp" line="340"/>
        <source>Deleting</source>
        <translation>正在刪除</translation>
    </message>
    <message>
        <location filename="../deepin-compressor/source/src/progress.cpp" line="347"/>
        <source>Extracting</source>
        <translation>正在解壓</translation>
    </message>
</context>
<context>
    <name>ProgressDialog</name>
    <message>
        <location filename="../deepin-compressor/source/src/progressdialog.cpp" line="50"/>
        <source>%1 task(s) in progress</source>
        <translation>有%1個任務正在進行</translation>
    </message>
    <message>
        <location filename="../deepin-compressor/source/src/progressdialog.cpp" line="62"/>
        <location filename="../deepin-compressor/source/src/progressdialog.cpp" line="115"/>
        <source>Task</source>
        <translation>有%1個任務正在進行</translation>
    </message>
    <message>
        <location filename="../deepin-compressor/source/src/progressdialog.cpp" line="63"/>
        <location filename="../deepin-compressor/source/src/progressdialog.cpp" line="121"/>
        <location filename="../deepin-compressor/source/src/progressdialog.cpp" line="140"/>
        <source>Extracting</source>
        <translation>正在壓縮</translation>
    </message>
    <message>
        <location filename="../deepin-compressor/source/src/progressdialog.cpp" line="136"/>
        <source>Extraction successful</source>
        <translation>解壓成功</translation>
    </message>
    <message>
        <location filename="../deepin-compressor/source/src/progressdialog.cpp" line="136"/>
        <source>Extract to</source>
        <translation>解壓到</translation>
    </message>
    <message>
        <location filename="../deepin-compressor/source/src/progressdialog.cpp" line="141"/>
        <source>Extraction successful</source>
        <comment>progressdialog</comment>
        <translation>解壓成功</translation>
    </message>
</context>
<context>
    <name>QInstaller</name>
    <message>
        <location filename="../deepin-compressor/source/src/utils.cpp" line="178"/>
        <source>B</source>
        <translation>B</translation>
    </message>
    <message>
        <location filename="../deepin-compressor/source/src/utils.cpp" line="179"/>
        <source>KB</source>
        <translation>KB</translation>
    </message>
    <message>
        <location filename="../deepin-compressor/source/src/utils.cpp" line="180"/>
        <source>MB</source>
        <translation>MB</translation>
    </message>
    <message>
        <location filename="../deepin-compressor/source/src/utils.cpp" line="181"/>
        <source>GB</source>
        <translation>GB</translation>
    </message>
    <message>
        <location filename="../deepin-compressor/source/src/utils.cpp" line="182"/>
        <source>TB</source>
        <translation>TB</translation>
    </message>
    <message>
        <location filename="../deepin-compressor/source/src/utils.cpp" line="183"/>
        <source>PB</source>
        <translation>PB</translation>
    </message>
    <message>
        <location filename="../deepin-compressor/source/src/utils.cpp" line="184"/>
        <source>EB</source>
        <translation>EB</translation>
    </message>
    <message>
        <location filename="../deepin-compressor/source/src/utils.cpp" line="185"/>
        <source>ZB</source>
        <translation>ZB</translation>
    </message>
    <message>
        <location filename="../deepin-compressor/source/src/utils.cpp" line="186"/>
        <source>YB</source>
        <translation>YB</translation>
    </message>
</context>
<context>
    <name>QObject</name>
    <message>
        <location filename="../deepin-compressor/source/src/fileViewer.cpp" line="640"/>
        <source>Name</source>
        <translation>名稱</translation>
    </message>
    <message>
        <location filename="../deepin-compressor/source/src/fileViewer.cpp" line="644"/>
        <source>Time modified</source>
        <translation>修改時間</translation>
    </message>
    <message>
        <location filename="../deepin-compressor/source/src/fileViewer.cpp" line="648"/>
        <source>Type</source>
        <translation>類型</translation>
    </message>
    <message>
        <location filename="../deepin-compressor/source/src/fileViewer.cpp" line="652"/>
        <source>Size</source>
        <translation>大小</translation>
    </message>
    <message>
        <location filename="../deepin-compressor/source/src/fileViewer.cpp" line="1555"/>
        <source>Update</source>
        <translation>更新</translation>
    </message>
    <message>
        <location filename="../deepin-compressor/source/src/settings_translation.cpp" line="5"/>
        <source>General</source>
        <translation>設定</translation>
    </message>
    <message>
        <location filename="../deepin-compressor/source/src/settings_translation.cpp" line="6"/>
        <source>Extraction</source>
        <translation>解壓</translation>
    </message>
    <message>
        <location filename="../deepin-compressor/source/src/settings_translation.cpp" line="7"/>
        <source>Auto create a folder for multiple extracted files</source>
        <translation>自動建立資料夾</translation>
    </message>
    <message>
        <location filename="../deepin-compressor/source/src/settings_translation.cpp" line="8"/>
        <source>Show extracted files when completed</source>
        <translation>當解壓完成後自動開啟對應的資料夾</translation>
    </message>
    <message>
        <location filename="../deepin-compressor/source/src/settings_translation.cpp" line="9"/>
        <source>File Management</source>
        <translation>文件管理</translation>
    </message>
    <message>
        <location filename="../deepin-compressor/source/src/settings_translation.cpp" line="10"/>
        <source>Delete files after compression</source>
        <translation>壓縮後刪除原來的文件</translation>
    </message>
    <message>
        <location filename="../deepin-compressor/source/src/settings_translation.cpp" line="11"/>
        <source>Files Associated</source>
        <translation>關聯文件</translation>
    </message>
    <message>
        <location filename="../deepin-compressor/source/src/settings_translation.cpp" line="12"/>
        <source>File Type</source>
        <translation>文件類型</translation>
    </message>
    <message>
        <location filename="../deepin-compressor/source/src/myfileitem.cpp" line="50"/>
        <location filename="../deepin-compressor/source/src/myfileitem.cpp" line="51"/>
        <location filename="../deepin-compressor/source/src/myfileitem.cpp" line="53"/>
        <location filename="../deepin-compressor/source/src/myfileitem.cpp" line="54"/>
        <location filename="../deepin-compressor/source/src/myfileitem.cpp" line="56"/>
        <location filename="../deepin-compressor/source/src/myfileitem.cpp" line="57"/>
        <location filename="../deepin-compressor/source/src/myfileitem.cpp" line="59"/>
        <location filename="../deepin-compressor/source/src/myfileitem.cpp" line="61"/>
        <source>item(s)</source>
        <translation>項</translation>
    </message>
    <message>
        <location filename="../deepin-compressor/source/src/mainwindow.cpp" line="243"/>
        <source>Confirm</source>
        <translation>確定</translation>
    </message>
    <message>
        <location filename="../deepin-compressor/source/src/mainwindow.cpp" line="715"/>
        <source>%1 changed. Do you want to save changes to the archive?</source>
        <translation>文件“%1”已修改，是否將此修改更新到壓縮包？</translation>
    </message>
    <message>
        <location filename="../deepin-compressor/source/src/mainwindow.cpp" line="729"/>
        <source>Discard</source>
        <translation>不儲存</translation>
    </message>
    <message>
        <location filename="../deepin-compressor/source/src/fileViewer.cpp" line="1554"/>
        <source>Cancel</source>
        <translation>取消</translation>
    </message>
    <message>
        <location filename="../deepin-compressor/source/src/mainwindow.cpp" line="730"/>
        <source>Save</source>
        <translation>儲存</translation>
    </message>
    <message>
        <location filename="../3rdparty/interface/queries.cpp" line="188"/>
        <source>Another file with the same name already exists, replace it?</source>
        <translation>文件已存在，是否取代？</translation>
    </message>
    <message>
        <location filename="../3rdparty/interface/queries.cpp" line="190"/>
        <source>Skip</source>
        <translation>跳過</translation>
    </message>
    <message>
        <location filename="../3rdparty/interface/queries.cpp" line="191"/>
        <source>Replace</source>
        <translation>取代</translation>
    </message>
    <message>
        <location filename="../3rdparty/interface/queries.cpp" line="196"/>
        <source>Apply to all</source>
        <translation>應用程式到全部文件</translation>
    </message>
    <message>
        <location filename="../3rdparty/interface/queries.cpp" line="367"/>
        <location filename="../3rdparty/interface/queries.cpp" line="456"/>
        <source>OK</source>
        <translation>確定</translation>
    </message>
    <message>
        <location filename="../3rdparty/interface/queries.cpp" line="454"/>
        <source>Wrong password</source>
        <translation>密碼錯誤</translation>
    </message>
</context>
<context>
    <name>SettingDialog</name>
    <message>
        <location filename="../deepin-compressor/source/src/settingdialog.cpp" line="133"/>
        <source>Select All</source>
        <translation>全選</translation>
    </message>
    <message>
        <location filename="../deepin-compressor/source/src/settingdialog.cpp" line="172"/>
        <location filename="../deepin-compressor/source/src/settingdialog.cpp" line="201"/>
        <source>Current directory</source>
        <translation>目前目錄</translation>
    </message>
    <message>
        <location filename="../deepin-compressor/source/src/settingdialog.cpp" line="134"/>
        <source>Clear All</source>
        <translation>取消全選</translation>
    </message>
    <message>
        <location filename="../deepin-compressor/source/src/settingdialog.cpp" line="135"/>
        <source>Recommended</source>
        <translation>推薦選擇</translation>
    </message>
    <message>
        <location filename="../deepin-compressor/source/src/settingdialog.cpp" line="166"/>
        <source>Extract archives to</source>
        <translation>預設解壓位置</translation>
    </message>
    <message>
        <location filename="../deepin-compressor/source/src/settingdialog.cpp" line="172"/>
        <location filename="../deepin-compressor/source/src/settingdialog.cpp" line="211"/>
        <source>Other directory</source>
        <translation>其他目錄</translation>
    </message>
    <message>
        <location filename="../deepin-compressor/source/src/settingdialog.cpp" line="172"/>
        <location filename="../deepin-compressor/source/src/settingdialog.cpp" line="206"/>
        <source>Desktop</source>
        <translation>桌面</translation>
    </message>
    <message>
        <location filename="../deepin-compressor/source/src/settingdialog.cpp" line="275"/>
        <source>Delete archives after extraction</source>
        <translation>解壓後刪除壓縮文件</translation>
    </message>
    <message>
        <location filename="../deepin-compressor/source/src/settingdialog.cpp" line="281"/>
        <location filename="../deepin-compressor/source/src/settingdialog.cpp" line="302"/>
        <source>Never</source>
        <translation>從不</translation>
    </message>
    <message>
        <location filename="../deepin-compressor/source/src/settingdialog.cpp" line="281"/>
        <location filename="../deepin-compressor/source/src/settingdialog.cpp" line="305"/>
        <source>Ask for confirmation</source>
        <translation>詢問確認</translation>
    </message>
    <message>
        <location filename="../deepin-compressor/source/src/settingdialog.cpp" line="281"/>
        <location filename="../deepin-compressor/source/src/settingdialog.cpp" line="308"/>
        <source>Always</source>
        <translation>總是</translation>
    </message>
    <message>
        <location filename="../deepin-compressor/source/src/settingdialog.cpp" line="355"/>
        <source>The default extraction path does not exist, please retry</source>
        <translation>預設解壓路徑不存在，請重新輸入</translation>
    </message>
</context>
<context>
    <name>UnCompressPage</name>
    <message>
        <location filename="../deepin-compressor/source/src/uncompresspage.cpp" line="53"/>
        <source>Extract</source>
        <translation>解壓</translation>
    </message>
    <message>
        <location filename="../deepin-compressor/source/src/uncompresspage.cpp" line="59"/>
        <location filename="../deepin-compressor/source/src/uncompresspage.cpp" line="150"/>
        <location filename="../deepin-compressor/source/src/uncompresspage.cpp" line="161"/>
        <source>Extract to:</source>
        <translation>解壓到：</translation>
    </message>
    <message>
        <location filename="../deepin-compressor/source/src/uncompresspage.cpp" line="113"/>
        <source>The default extraction path does not exist, please retry</source>
        <translation>預設解壓路徑不存在，請重新輸入</translation>
    </message>
    <message>
        <location filename="../deepin-compressor/source/src/uncompresspage.cpp" line="115"/>
        <source>You do not have permission to save files here, please change and retry</source>
        <translation>您沒有權限在此路徑儲存文件，請重試</translation>
    </message>
    <message>
        <location filename="../deepin-compressor/source/src/uncompresspage.cpp" line="134"/>
        <source>Find directory</source>
        <translation>解壓到目錄</translation>
    </message>
    <message>
        <location filename="../deepin-compressor/source/src/uncompresspage.cpp" line="187"/>
        <source>OK</source>
        <translation>確定</translation>
    </message>
    <message>
        <location filename="../deepin-compressor/source/src/uncompresspage.cpp" line="561"/>
        <source>Cancel</source>
        <translation>取消</translation>
    </message>
    <message>
        <location filename="../deepin-compressor/source/src/uncompresspage.cpp" line="562"/>
        <source>Convert</source>
        <translation>轉換</translation>
    </message>
    <message>
        <location filename="../deepin-compressor/source/src/uncompresspage.cpp" line="572"/>
        <source>Changes to archives in this file type are not supported. Please convert the archive format to save the changes.</source>
        <translation>不支援對該壓縮格式的修改。為了保持對該文件的修改，建議您進行壓縮格式轉換。</translation>
    </message>
    <message>
        <location filename="../deepin-compressor/source/src/uncompresspage.cpp" line="588"/>
        <source>Convert the format to:</source>
        <translation>轉換壓縮格式為：</translation>
    </message>
</context>
<context>
    <name>fileViewer</name>
    <message>
        <location filename="../deepin-compressor/source/src/fileViewer.cpp" line="1739"/>
        <source>Extract</source>
        <translation>解壓</translation>
    </message>
    <message>
        <location filename="../deepin-compressor/source/src/fileViewer.cpp" line="606"/>
        <location filename="../deepin-compressor/source/src/fileViewer.cpp" line="1747"/>
        <location filename="../deepin-compressor/source/src/fileViewer.cpp" line="1762"/>
        <source>Delete</source>
        <translation>刪除</translation>
    </message>
    <message>
        <location filename="../deepin-compressor/source/src/fileViewer.cpp" line="589"/>
        <location filename="../deepin-compressor/source/src/fileViewer.cpp" line="607"/>
        <location filename="../deepin-compressor/source/src/fileViewer.cpp" line="1745"/>
        <location filename="../deepin-compressor/source/src/fileViewer.cpp" line="1758"/>
        <source>Open</source>
        <translation>開啟</translation>
    </message>
    <message>
        <location filename="../deepin-compressor/source/src/fileViewer.cpp" line="685"/>
        <source>item(s)</source>
        <translation>項</translation>
    </message>
    <message>
        <location filename="../deepin-compressor/source/src/fileViewer.cpp" line="707"/>
        <source>yyyy/MM/dd hh:mm:ss</source>
        <translation>yyyy/MM/dd hh:mm:ss</translation>
    </message>
    <message>
        <location filename="../deepin-compressor/source/src/fileViewer.cpp" line="1080"/>
        <location filename="../deepin-compressor/source/src/fileViewer.cpp" line="1346"/>
        <source>Confirm</source>
        <translation>確定</translation>
    </message>
    <message>
        <location filename="../deepin-compressor/source/src/fileViewer.cpp" line="1078"/>
        <location filename="../deepin-compressor/source/src/fileViewer.cpp" line="1345"/>
        <source>Cancel</source>
        <translation>取消</translation>
    </message>
    <message>
        <location filename="../deepin-compressor/source/src/fileViewer.cpp" line="590"/>
        <location filename="../deepin-compressor/source/src/fileViewer.cpp" line="1747"/>
        <source>Delete</source>
        <comment>slotDecompressRowDelete</comment>
        <translation>刪除</translation>
    </message>
    <message>
        <location filename="../deepin-compressor/source/src/fileViewer.cpp" line="592"/>
        <location filename="../deepin-compressor/source/src/fileViewer.cpp" line="609"/>
        <source>Open with</source>
        <translation>開啟方式</translation>
    </message>
    <message>
        <location filename="../deepin-compressor/source/src/fileViewer.cpp" line="744"/>
        <location filename="../deepin-compressor/source/src/fileViewer.cpp" line="1776"/>
        <location filename="../deepin-compressor/source/src/fileViewer.cpp" line="1796"/>
        <source>Select default program</source>
        <translation>選擇預設程式</translation>
    </message>
    <message>
        <location filename="../deepin-compressor/source/src/fileViewer.cpp" line="956"/>
        <location filename="../deepin-compressor/source/src/fileViewer.cpp" line="1749"/>
        <source>Do you want to delete the selected file(s)?</source>
        <translation>是否刪除已選定文件？</translation>
    </message>
    <message>
        <location filename="../deepin-compressor/source/src/fileViewer.cpp" line="1122"/>
        <source>It will permanently delete the file(s). Are you sure you want to continue?</source>
        <translation>該操作將永久刪除已選擇的文件。您確定要刪除嗎？</translation>
    </message>
    <message>
        <location filename="../deepin-compressor/source/src/fileViewer.cpp" line="1534"/>
        <source>Files have been changed. Do you want to update the changes to %1?</source>
        <translation>文件已被修改。是否更新到壓縮包&quot;%1&quot;?</translation>
    </message>
    <message>
        <location filename="../deepin-compressor/source/src/fileViewer.cpp" line="1082"/>
        <source>Update</source>
        <translation>更新</translation>
    </message>
    <message>
        <location filename="../deepin-compressor/source/src/fileViewer.cpp" line="587"/>
        <location filename="../deepin-compressor/source/src/fileViewer.cpp" line="1739"/>
        <source>Extract</source>
        <comment>slotDecompressRowDoubleClicked</comment>
        <translation>提取</translation>
    </message>
    <message>
        <location filename="../deepin-compressor/source/src/fileViewer.cpp" line="588"/>
        <location filename="../deepin-compressor/source/src/fileViewer.cpp" line="1741"/>
        <source>Extract to current directory</source>
        <translation>提取到目前資料夾</translation>
    </message>
</context>
</TS>