<?xml version="1.0" ?><!DOCTYPE TS><TS language="uk" version="2.1">
<context>
    <name>ArchiveModel</name>
    <message>
        <location filename="../deepin-compressor/source/src/archivemodel.cpp" line="119"/>
        <location filename="../deepin-compressor/source/src/archivemodel.cpp" line="121"/>
        <source>item(s)</source>
        <translation>записів</translation>
    </message>
    <message>
        <location filename="../deepin-compressor/source/src/archivemodel.cpp" line="132"/>
        <source>yyyy/MM/dd hh:mm:ss</source>
        <translation>dd-MM-yyyy hh:mm:ss</translation>
    </message>
    <message>
        <location filename="../deepin-compressor/source/src/archivemodel.cpp" line="200"/>
        <source>Name</source>
        <translation>Назва</translation>
    </message>
    <message>
        <location filename="../deepin-compressor/source/src/archivemodel.cpp" line="202"/>
        <source>Size</source>
        <translation>Розмір</translation>
    </message>
    <message>
        <location filename="../deepin-compressor/source/src/archivemodel.cpp" line="204"/>
        <source>Type</source>
        <translation>Тип</translation>
    </message>
    <message>
        <location filename="../deepin-compressor/source/src/archivemodel.cpp" line="206"/>
        <source>Time modified</source>
        <translation>Час внесення змін</translation>
    </message>
</context>
<context>
    <name>CompressPage</name>
    <message>
        <location filename="../deepin-compressor/source/src/compresspage.cpp" line="48"/>
        <source>Next</source>
        <translation>Далі</translation>
    </message>
    <message>
        <location filename="../deepin-compressor/source/src/compresspage.cpp" line="93"/>
        <source>Please add files</source>
        <translation>Будь ласка, додайте файли</translation>
    </message>
    <message>
        <location filename="../deepin-compressor/source/src/compresspage.cpp" line="94"/>
        <location filename="../deepin-compressor/source/src/compresspage.cpp" line="116"/>
        <source>OK</source>
        <translation>Гаразд</translation>
    </message>
    <message>
        <location filename="../deepin-compressor/source/src/compresspage.cpp" line="118"/>
        <source>Please add files to the top-level directory</source>
        <translation>Будь ласка, додайте файли до каталогу найвищого рівня</translation>
    </message>
</context>
<context>
    <name>CompressSetting</name>
    <message>
        <location filename="../deepin-compressor/source/src/compresssetting.cpp" line="236"/>
        <source>Compress</source>
        <translation>Стиснути</translation>
    </message>
    <message>
        <location filename="../deepin-compressor/source/src/compresssetting.cpp" line="164"/>
        <source>Advanced Options</source>
        <translation>Додаткові параметри</translation>
    </message>
    <message>
        <location filename="../deepin-compressor/source/src/compresssetting.cpp" line="158"/>
        <source>Name</source>
        <translation>Назва</translation>
    </message>
    <message>
        <location filename="../deepin-compressor/source/src/compresssetting.cpp" line="159"/>
        <source>Save to</source>
        <translation>Зберегти до</translation>
    </message>
    <message>
        <location filename="../deepin-compressor/source/src/compresssetting.cpp" line="170"/>
        <source>Encrypt the archive</source>
        <translation>Зашифрувати архів</translation>
    </message>
    <message>
        <location filename="../deepin-compressor/source/src/compresssetting.cpp" line="171"/>
        <source>Support zip, 7z type only</source>
        <translation>Передбачено підтримку лише типів zip, 7z</translation>
    </message>
    <message>
        <location filename="../deepin-compressor/source/src/compresssetting.cpp" line="176"/>
        <source>Password</source>
        <translation>Пароль</translation>
    </message>
    <message>
        <location filename="../deepin-compressor/source/src/compresssetting.cpp" line="178"/>
        <source>Encrypt the file list too</source>
        <translation>Зашифрувати список файлів</translation>
    </message>
    <message>
        <location filename="../deepin-compressor/source/src/compresssetting.cpp" line="179"/>
        <location filename="../deepin-compressor/source/src/compresssetting.cpp" line="187"/>
        <source>Support 7z type only</source>
        <translation>Передбачено підтримку лише типу 7z</translation>
    </message>
    <message>
        <location filename="../deepin-compressor/source/src/compresssetting.cpp" line="185"/>
        <source>Split to volumes</source>
        <translation>Поділити на томи</translation>
    </message>
    <message>
        <location filename="../deepin-compressor/source/src/compresssetting.cpp" line="335"/>
        <location filename="../deepin-compressor/source/src/compresssetting.cpp" line="830"/>
        <location filename="../deepin-compressor/source/src/compresssetting.cpp" line="966"/>
        <location filename="../deepin-compressor/source/src/compresssetting.cpp" line="1099"/>
        <source>%1 was changed on the disk, please import it again.</source>
        <translation>%1 було змінено на диску. Будь ласка, імпортуйте його знову.</translation>
    </message>
    <message>
        <location filename="../deepin-compressor/source/src/compresssetting.cpp" line="353"/>
        <location filename="../deepin-compressor/source/src/compresssetting.cpp" line="359"/>
        <location filename="../deepin-compressor/source/src/compresssetting.cpp" line="364"/>
        <location filename="../deepin-compressor/source/src/compresssetting.cpp" line="837"/>
        <location filename="../deepin-compressor/source/src/compresssetting.cpp" line="843"/>
        <location filename="../deepin-compressor/source/src/compresssetting.cpp" line="848"/>
        <location filename="../deepin-compressor/source/src/compresssetting.cpp" line="973"/>
        <location filename="../deepin-compressor/source/src/compresssetting.cpp" line="979"/>
        <location filename="../deepin-compressor/source/src/compresssetting.cpp" line="984"/>
        <location filename="../deepin-compressor/source/src/compresssetting.cpp" line="1106"/>
        <location filename="../deepin-compressor/source/src/compresssetting.cpp" line="1112"/>
        <location filename="../deepin-compressor/source/src/compresssetting.cpp" line="1117"/>
        <source>You do not have permission to compress %1</source>
        <translation>У вас немає прав доступу для стискання %1</translation>
    </message>
    <message>
        <location filename="../deepin-compressor/source/src/compresssetting.cpp" line="373"/>
        <location filename="../deepin-compressor/source/src/compresssetting.cpp" line="857"/>
        <location filename="../deepin-compressor/source/src/compresssetting.cpp" line="993"/>
        <source>You do not have permission to save files here, please change and retry</source>
        <translation>У вас немає прав доступу для зберігання файлів тут. Будь ласка, змініть каталог і повторіть спробу.</translation>
    </message>
    <message>
        <location filename="../deepin-compressor/source/src/compresssetting.cpp" line="378"/>
        <source>Too many volumes, please change and retry</source>
        <translation>Забагато томів. Будь ласка, змініть кількість томів і повторіть спробу.</translation>
    </message>
    <message>
        <location filename="../deepin-compressor/source/src/compresssetting.cpp" line="478"/>
        <source>Files that begin with &apos;@&apos; cannot be compressed</source>
        <translation>Файли, назви яких починаються з «@&apos;», не може бути стиснути</translation>
    </message>
    <message>
        <location filename="../deepin-compressor/source/src/compresssetting.cpp" line="702"/>
        <source>Total size: %1</source>
        <translation>Загальний розмір: %1</translation>
    </message>
    <message>
        <location filename="../deepin-compressor/source/src/compresssetting.cpp" line="801"/>
        <location filename="../deepin-compressor/source/src/compresssetting.cpp" line="937"/>
        <location filename="../deepin-compressor/source/src/compresssetting.cpp" line="1070"/>
        <source>You cannot add the archive to itself</source>
        <translation>Ви не можете додавати архів до самого себе</translation>
    </message>
    <message>
        <location filename="../deepin-compressor/source/src/compresssetting.cpp" line="1279"/>
        <source>Another file with the same name already exists, replace it?</source>
        <translation>Файл вже існує. Що робити?</translation>
    </message>
    <message>
        <location filename="../deepin-compressor/source/src/compresssetting.cpp" line="800"/>
        <source>Close</source>
        <translation>Закрити</translation>
    </message>
    <message>
        <location filename="../deepin-compressor/source/src/compresssetting.cpp" line="936"/>
        <location filename="../deepin-compressor/source/src/compresssetting.cpp" line="1203"/>
        <source>OK</source>
        <translation>Гаразд</translation>
    </message>
    <message>
        <location filename="../deepin-compressor/source/src/compresssetting.cpp" line="1281"/>
        <source>Cancel</source>
        <translation>Скасувати</translation>
    </message>
    <message>
        <location filename="../deepin-compressor/source/src/compresssetting.cpp" line="1282"/>
        <source>Replace</source>
        <translation>Замінити</translation>
    </message>
    <message>
        <location filename="../deepin-compressor/source/src/compresssetting.cpp" line="322"/>
        <location filename="../deepin-compressor/source/src/compresssetting.cpp" line="817"/>
        <location filename="../deepin-compressor/source/src/compresssetting.cpp" line="953"/>
        <location filename="../deepin-compressor/source/src/compresssetting.cpp" line="1086"/>
        <source>Please enter the path</source>
        <translation>Будь ласка, вкажіть шлях</translation>
    </message>
    <message>
        <location filename="../deepin-compressor/source/src/compresssetting.cpp" line="149"/>
        <source>New Archive</source>
        <translation>Новий архів</translation>
    </message>
    <message>
        <location filename="../deepin-compressor/source/src/compresssetting.cpp" line="317"/>
        <location filename="../deepin-compressor/source/src/compresssetting.cpp" line="812"/>
        <location filename="../deepin-compressor/source/src/compresssetting.cpp" line="948"/>
        <location filename="../deepin-compressor/source/src/compresssetting.cpp" line="1081"/>
        <source>Invalid file name</source>
        <translation>Некоректна назва файла</translation>
    </message>
    <message>
        <location filename="../deepin-compressor/source/src/compresssetting.cpp" line="327"/>
        <location filename="../deepin-compressor/source/src/compresssetting.cpp" line="822"/>
        <location filename="../deepin-compressor/source/src/compresssetting.cpp" line="958"/>
        <location filename="../deepin-compressor/source/src/compresssetting.cpp" line="1091"/>
        <source>The path does not exist, please retry</source>
        <translation>Шляху не існує. Будь ласка, повторіть спробу</translation>
    </message>
</context>
<context>
    <name>Compressor_Fail</name>
    <message>
        <location filename="../deepin-compressor/source/src/compressor_fail.cpp" line="35"/>
        <source>Extraction failed</source>
        <translation>Помилка під час розпаковування.</translation>
    </message>
    <message>
        <location filename="../deepin-compressor/source/src/compressor_fail.cpp" line="36"/>
        <source>Damaged file, unable to extract</source>
        <translation>Пошкоджений файл. Не вдалося видобути</translation>
    </message>
    <message>
        <location filename="../deepin-compressor/source/src/compressor_fail.cpp" line="61"/>
        <source>Retry</source>
        <translation>Повторити</translation>
    </message>
    <message>
        <location filename="../deepin-compressor/source/src/compressor_fail.cpp" line="63"/>
        <source>Back</source>
        <translation>Назад</translation>
    </message>
</context>
<context>
    <name>Compressor_Success</name>
    <message>
        <location filename="../deepin-compressor/source/src/compressor_success.cpp" line="40"/>
        <source>Compression successful</source>
        <translation>Успішне стискання</translation>
    </message>
    <message>
        <location filename="../deepin-compressor/source/src/compressor_success.cpp" line="58"/>
        <source>View</source>
        <translation>Перегляд</translation>
    </message>
    <message>
        <location filename="../deepin-compressor/source/src/compressor_success.cpp" line="60"/>
        <source>Back</source>
        <translation>Назад</translation>
    </message>
</context>
<context>
    <name>EncodingPage</name>
    <message>
        <location filename="../deepin-compressor/source/src/encodingpage.cpp" line="65"/>
        <source>Filename Encoding</source>
        <translation>Кодування назви файла</translation>
    </message>
    <message>
        <location filename="../deepin-compressor/source/src/encodingpage.cpp" line="78"/>
        <source>Select an encoding to continue</source>
        <translation>Виберіть кодування для продовження</translation>
    </message>
    <message>
        <location filename="../deepin-compressor/source/src/encodingpage.cpp" line="82"/>
        <source>Cancel</source>
        <translation>Скасувати</translation>
    </message>
    <message>
        <location filename="../deepin-compressor/source/src/encodingpage.cpp" line="83"/>
        <source>Select Character Encoding</source>
        <translation>Виберіть кодування символів</translation>
    </message>
</context>
<context>
    <name>EncryptionPage</name>
    <message>
        <location filename="../deepin-compressor/source/src/encryptionpage.cpp" line="47"/>
        <source>Encrypted file, please enter the password</source>
        <translation>Зашифрований файл. Будь ласка, вкажіть пароль</translation>
    </message>
    <message>
        <location filename="../deepin-compressor/source/src/encryptionpage.cpp" line="50"/>
        <source>Next</source>
        <translation>Далі</translation>
    </message>
    <message>
        <location filename="../deepin-compressor/source/src/encryptionpage.cpp" line="55"/>
        <source>Password</source>
        <translation>Пароль</translation>
    </message>
    <message>
        <location filename="../deepin-compressor/source/src/encryptionpage.cpp" line="135"/>
        <source>Wrong password</source>
        <translation>Помилковий пароль</translation>
    </message>
</context>
<context>
    <name>ExtractPauseDialog</name>
    <message>
        <location filename="../deepin-compressor/source/src/extractpausedialog.cpp" line="67"/>
        <source>Are you sure you want to stop the extraction?</source>
        <translation>Зараз виконуємо завдання з видобування даних</translation>
    </message>
    <message>
        <location filename="../deepin-compressor/source/src/extractpausedialog.cpp" line="69"/>
        <source>Cancel</source>
        <translation>Скасувати</translation>
    </message>
    <message>
        <location filename="../deepin-compressor/source/src/extractpausedialog.cpp" line="70"/>
        <source>Confirm</source>
        <translation>Продовжити стискання</translation>
    </message>
</context>
<context>
    <name>HomePage</name>
    <message>
        <location filename="../deepin-compressor/source/src/homepage.cpp" line="40"/>
        <source>Drag file or folder here</source>
        <translation>Сюди можна перетягнути файл або теку</translation>
    </message>
    <message>
        <location filename="../deepin-compressor/source/src/homepage.cpp" line="42"/>
        <source>Select File</source>
        <translation>Вибрати файл</translation>
    </message>
</context>
<context>
    <name>LogViewHeaderView</name>
    <message>
        <location filename="../deepin-compressor/source/src/logviewheaderview.cpp" line="160"/>
        <source>Back</source>
        <translation>Назад</translation>
    </message>
</context>
<context>
    <name>Main</name>
    <message>
        <location filename="../deepin-compressor/main.cpp" line="145"/>
        <location filename="../tests/UnitTest/deepin-compressor/source/src/test_main.cpp" line="150"/>
        <source>Archive Manager</source>
        <translation>Засіб для керування архівами</translation>
    </message>
    <message>
        <location filename="../deepin-compressor/main.cpp" line="146"/>
        <location filename="../tests/UnitTest/deepin-compressor/source/src/test_main.cpp" line="151"/>
        <source>Archive Manager is a fast and lightweight application for creating and extracting archives.</source>
        <translation>«Засіб для керування архівами» — програмний інструмент, який надає доступ до типових можливостей із видобування та стискання файлів</translation>
    </message>
</context>
<context>
    <name>MainWindow</name>
    <message>
        <location filename="../deepin-compressor/source/src/mainwindow.cpp" line="564"/>
        <source>Close</source>
        <translation>Закрити</translation>
    </message>
    <message>
        <location filename="../deepin-compressor/source/src/mainwindow.cpp" line="568"/>
        <source>Help</source>
        <translation>Довідка</translation>
    </message>
    <message>
        <location filename="../deepin-compressor/source/src/mainwindow.cpp" line="572"/>
        <source>Select the file</source>
        <translation>Виберіть файл</translation>
    </message>
    <message>
        <location filename="../deepin-compressor/source/src/mainwindow.cpp" line="576"/>
        <source>Delete</source>
        <translation>Вилучити</translation>
    </message>
    <message>
        <location filename="../deepin-compressor/source/src/mainwindow.cpp" line="596"/>
        <source>Shortcuts</source>
        <translation>Клавіатурні скорочення</translation>
    </message>
    <message>
        <location filename="../deepin-compressor/source/src/mainwindow.cpp" line="781"/>
        <source>Open file</source>
        <translation>Відкрити файл</translation>
    </message>
    <message>
        <location filename="../deepin-compressor/source/src/mainwindow.cpp" line="822"/>
        <source>Settings</source>
        <translation>Параметри</translation>
    </message>
    <message>
        <location filename="../deepin-compressor/source/src/mainwindow.cpp" line="1284"/>
        <location filename="../deepin-compressor/source/src/mainwindow.cpp" line="1297"/>
        <location filename="../deepin-compressor/source/src/mainwindow.cpp" line="2961"/>
        <source>Create New Archive</source>
        <translation>Створити архів</translation>
    </message>
    <message>
        <location filename="../deepin-compressor/source/src/mainwindow.cpp" line="1338"/>
        <source>Opening</source>
        <translation>Відкриваємо</translation>
    </message>
    <message>
        <location filename="../deepin-compressor/source/src/mainwindow.cpp" line="1366"/>
        <source>Converting</source>
        <translation>Перетворення</translation>
    </message>
    <message>
        <location filename="../deepin-compressor/source/src/mainwindow.cpp" line="1375"/>
        <source>Compression successful</source>
        <translation>Успішне стискання</translation>
    </message>
    <message>
        <location filename="../deepin-compressor/source/src/mainwindow.cpp" line="5119"/>
        <source>Confirm</source>
        <translation>Продовжити стискання</translation>
    </message>
    <message>
        <location filename="../deepin-compressor/source/src/mainwindow.cpp" line="5122"/>
        <source>Do you want to delete the archive?</source>
        <translation>Хочете вилучити архів?</translation>
    </message>
    <message>
        <location filename="../deepin-compressor/source/src/mainwindow.cpp" line="2573"/>
        <location filename="../deepin-compressor/source/src/mainwindow.cpp" line="2669"/>
        <location filename="../deepin-compressor/source/src/mainwindow.cpp" line="2703"/>
        <source>Skip all files</source>
        <translation>Пропустити усі файли</translation>
    </message>
    <message>
        <location filename="../deepin-compressor/source/src/mainwindow.cpp" line="2546"/>
        <location filename="../deepin-compressor/source/src/mainwindow.cpp" line="2705"/>
        <source>Extraction successful</source>
        <translation>Успішне видобування</translation>
    </message>
    <message>
        <location filename="../deepin-compressor/source/src/mainwindow.cpp" line="469"/>
        <source>%1 was changed on the disk, please import it again.</source>
        <translation>%1 було змінено на диску. Будь ласка, імпортуйте його знову.</translation>
    </message>
    <message>
        <location filename="../deepin-compressor/source/src/mainwindow.cpp" line="1631"/>
        <source>Do you want to add the archive to the list or open it in new window?</source>
        <translation>Хочете додати архів до списку чи відкрити його у новому вікні?</translation>
    </message>
    <message>
        <location filename="../deepin-compressor/source/src/mainwindow.cpp" line="584"/>
        <source>Display shortcuts</source>
        <translation>Показати клавіатурні скорочення</translation>
    </message>
    <message>
        <location filename="../deepin-compressor/source/src/mainwindow.cpp" line="242"/>
        <location filename="../deepin-compressor/source/src/mainwindow.cpp" line="1632"/>
        <location filename="../deepin-compressor/source/src/mainwindow.cpp" line="5118"/>
        <source>Cancel</source>
        <translation>Скасувати</translation>
    </message>
    <message>
        <location filename="../deepin-compressor/source/src/mainwindow.cpp" line="114"/>
        <source>Archive Manager</source>
        <translation>Керування архівами</translation>
    </message>
    <message>
        <location filename="../deepin-compressor/source/src/mainwindow.cpp" line="475"/>
        <location filename="../deepin-compressor/source/src/mainwindow.cpp" line="2246"/>
        <location filename="../deepin-compressor/source/src/mainwindow.cpp" line="4916"/>
        <source>OK</source>
        <translation>Гаразд</translation>
    </message>
    <message>
        <location filename="../deepin-compressor/source/src/mainwindow.cpp" line="1316"/>
        <source>Adding files to %1</source>
        <translation>Додаємо файли до %1</translation>
    </message>
    <message>
        <location filename="../deepin-compressor/source/src/mainwindow.cpp" line="1319"/>
        <source>Compressing</source>
        <translation>Стискання</translation>
    </message>
    <message>
        <location filename="../deepin-compressor/source/src/mainwindow.cpp" line="1340"/>
        <source>Extracting</source>
        <translation>Видобування</translation>
    </message>
    <message>
        <location filename="../deepin-compressor/source/src/mainwindow.cpp" line="1354"/>
        <source>Deleting</source>
        <translation>Вилучення</translation>
    </message>
    <message>
        <location filename="../deepin-compressor/source/src/mainwindow.cpp" line="1392"/>
        <source>Compression failed</source>
        <translation>Помилка під час стискання</translation>
    </message>
    <message>
        <location filename="../deepin-compressor/source/src/mainwindow.cpp" line="1424"/>
        <source>Extraction failed</source>
        <translation>Помилка під час розпаковування</translation>
    </message>
    <message>
        <location filename="../deepin-compressor/source/src/mainwindow.cpp" line="1437"/>
        <source>Conversion successful</source>
        <translation>Успішне перетворення</translation>
    </message>
    <message>
        <location filename="../deepin-compressor/source/src/mainwindow.cpp" line="1633"/>
        <source>Add</source>
        <translation>Додати</translation>
    </message>
    <message>
        <location filename="../deepin-compressor/source/src/mainwindow.cpp" line="1634"/>
        <source>Open in new window</source>
        <translation>Відкрити у новому вікні</translation>
    </message>
    <message>
        <location filename="../deepin-compressor/source/src/mainwindow.cpp" line="1841"/>
        <source>Find directory</source>
        <translation>Знайти каталог</translation>
    </message>
    <message>
        <location filename="../deepin-compressor/source/src/mainwindow.cpp" line="2167"/>
        <location filename="../deepin-compressor/source/src/mainwindow.cpp" line="2598"/>
        <source>Failed to open the archive: %1</source>
        <translation>Не вдалося відкрити архів: %1</translation>
    </message>
    <message>
        <location filename="../deepin-compressor/source/src/mainwindow.cpp" line="2481"/>
        <location filename="../deepin-compressor/source/src/mainwindow.cpp" line="2600"/>
        <source>Wrong password</source>
        <translation>Помилковий пароль</translation>
    </message>
    <message>
        <location filename="../deepin-compressor/source/src/mainwindow.cpp" line="2687"/>
        <source>Select default program</source>
        <translation>Виберіть типову програму</translation>
    </message>
    <message>
        <location filename="../deepin-compressor/source/src/mainwindow.cpp" line="4918"/>
        <source>Please check the file association type in the settings of Archive Manager</source>
        <translation>Будь ласка, перевірте прив&apos;язку до типу файлів у параметрах «Керування архівами»</translation>
    </message>
    <message>
        <location filename="../deepin-compressor/source/src/mainwindow.cpp" line="1982"/>
        <location filename="../deepin-compressor/source/src/mainwindow.cpp" line="2005"/>
        <location filename="../deepin-compressor/source/src/mainwindow.cpp" line="2173"/>
        <location filename="../deepin-compressor/source/src/mainwindow.cpp" line="2593"/>
        <source>Damaged file, unable to extract</source>
        <translation>Пошкоджений файл, неможливо видобути</translation>
    </message>
    <message>
        <location filename="../deepin-compressor/source/src/mainwindow.cpp" line="241"/>
        <source>Are you sure you want to stop the ongoing task?</source>
        <translation>Ви справді хочете зупинити виконання завдання, яке виконується?</translation>
    </message>
    <message>
        <location filename="../deepin-compressor/source/src/mainwindow.cpp" line="2245"/>
        <source>The archive was changed on the disk, please import it again.</source>
        <translation>Архів було змінено на диску. Будь ласка, імпортуйте його знову.</translation>
    </message>
    <message>
        <location filename="../deepin-compressor/source/src/mainwindow.cpp" line="2589"/>
        <location filename="../deepin-compressor/source/src/mainwindow.cpp" line="2591"/>
        <location filename="../deepin-compressor/source/src/mainwindow.cpp" line="4028"/>
        <location filename="../deepin-compressor/source/src/mainwindow.cpp" line="4034"/>
        <location filename="../deepin-compressor/source/src/mainwindow.cpp" line="4085"/>
        <location filename="../deepin-compressor/source/src/mainwindow.cpp" line="4091"/>
        <source>Insufficient space, please clear and retry</source>
        <translation>Недостатньо місця. Будь ласка, звільніть місце і повторіть спробу</translation>
    </message>
    <message>
        <location filename="../deepin-compressor/source/src/mainwindow.cpp" line="2596"/>
        <source>File name too long, unable to extract</source>
        <translation>Назва є надто довгою — неможливо видобути</translation>
    </message>
    <message>
        <location filename="../deepin-compressor/source/src/mainwindow.cpp" line="2600"/>
        <source>Unable to extract</source>
        <translation>Неможливо видобути</translation>
    </message>
    <message>
        <location filename="../deepin-compressor/source/src/mainwindow.cpp" line="4030"/>
        <location filename="../deepin-compressor/source/src/mainwindow.cpp" line="4036"/>
        <location filename="../deepin-compressor/source/src/mainwindow.cpp" line="4087"/>
        <location filename="../deepin-compressor/source/src/mainwindow.cpp" line="4093"/>
        <source>Damaged file</source>
        <translation>Пошкоджений файл</translation>
    </message>
</context>
<context>
    <name>MimeTypeDisplayManager</name>
    <message>
        <location filename="../deepin-compressor/source/src/mimetypedisplaymanager.cpp" line="46"/>
        <source>Directory</source>
        <translation>Каталог</translation>
    </message>
    <message>
        <location filename="../deepin-compressor/source/src/mimetypedisplaymanager.cpp" line="47"/>
        <source>Application</source>
        <translation>Програма</translation>
    </message>
    <message>
        <location filename="../deepin-compressor/source/src/mimetypedisplaymanager.cpp" line="48"/>
        <source>Video</source>
        <translation>Відео</translation>
    </message>
    <message>
        <location filename="../deepin-compressor/source/src/mimetypedisplaymanager.cpp" line="49"/>
        <source>Audio</source>
        <translation>Звук</translation>
    </message>
    <message>
        <location filename="../deepin-compressor/source/src/mimetypedisplaymanager.cpp" line="50"/>
        <source>Image</source>
        <translation>Зображення</translation>
    </message>
    <message>
        <location filename="../deepin-compressor/source/src/mimetypedisplaymanager.cpp" line="51"/>
        <source>Archive</source>
        <translation>Архівувати</translation>
    </message>
    <message>
        <location filename="../deepin-compressor/source/src/mimetypedisplaymanager.cpp" line="53"/>
        <source>Executable</source>
        <translation>Виконуваний файл</translation>
    </message>
    <message>
        <location filename="../deepin-compressor/source/src/mimetypedisplaymanager.cpp" line="52"/>
        <source>Document</source>
        <translation>Документ</translation>
    </message>
    <message>
        <location filename="../deepin-compressor/source/src/mimetypedisplaymanager.cpp" line="54"/>
        <source>Backup file</source>
        <translation>Файл резервної копії</translation>
    </message>
    <message>
        <location filename="../deepin-compressor/source/src/mimetypedisplaymanager.cpp" line="55"/>
        <source>Unknown</source>
        <translation>Невідомий</translation>
    </message>
</context>
<context>
    <name>MyFileSystemModel</name>
    <message>
        <location filename="../deepin-compressor/source/src/myfilesystemmodel.cpp" line="63"/>
        <source>Name</source>
        <translation>Назва</translation>
    </message>
    <message>
        <location filename="../deepin-compressor/source/src/myfilesystemmodel.cpp" line="65"/>
        <source>Size</source>
        <translation>Розмір</translation>
    </message>
    <message>
        <location filename="../deepin-compressor/source/src/myfilesystemmodel.cpp" line="67"/>
        <source>Type</source>
        <translation>Тип</translation>
    </message>
    <message>
        <location filename="../deepin-compressor/source/src/myfilesystemmodel.cpp" line="69"/>
        <source>Time modified</source>
        <translation>Час внесення змін</translation>
    </message>
    <message>
        <location filename="../deepin-compressor/source/src/myfilesystemmodel.cpp" line="127"/>
        <source>item(s)</source>
        <translation>записів</translation>
    </message>
    <message>
        <location filename="../deepin-compressor/source/src/myfilesystemmodel.cpp" line="138"/>
        <source>yyyy/MM/dd hh:mm:ss</source>
        <translation>dd-MM-yyyy hh:mm:ss</translation>
    </message>
</context>
<context>
    <name>OpenLoadingPage</name>
    <message>
        <location filename="../deepin-compressor/source/src/openloadingpage.cpp" line="52"/>
        <source>Loading, please wait...</source>
        <translation>Завантаження, зачекайте…</translation>
    </message>
</context>
<context>
    <name>OpenWithDialog</name>
    <message>
        <location filename="../deepin-compressor/source/src/openwithdialog/openwithdialog.cpp" line="308"/>
        <source>Open with</source>
        <translation>Відкрити за допомогою</translation>
    </message>
    <message>
        <location filename="../deepin-compressor/source/src/openwithdialog/openwithdialog.cpp" line="327"/>
        <source>Add other programs</source>
        <translation>Додати інші програми</translation>
    </message>
    <message>
        <location filename="../deepin-compressor/source/src/openwithdialog/openwithdialog.cpp" line="328"/>
        <source>Set as default</source>
        <translation>Зробити типовим</translation>
    </message>
    <message>
        <location filename="../deepin-compressor/source/src/openwithdialog/openwithdialog.cpp" line="330"/>
        <source>Cancel</source>
        <translation>Скасувати</translation>
    </message>
    <message>
        <location filename="../deepin-compressor/source/src/openwithdialog/openwithdialog.cpp" line="331"/>
        <source>Confirm</source>
        <translation>Продовжити стискання</translation>
    </message>
    <message>
        <location filename="../deepin-compressor/source/src/openwithdialog/openwithdialog.cpp" line="335"/>
        <source>Recommended Applications</source>
        <translation>Рекомендовані програми</translation>
    </message>
    <message>
        <location filename="../deepin-compressor/source/src/openwithdialog/openwithdialog.cpp" line="337"/>
        <source>Other Applications</source>
        <translation>Інші програми</translation>
    </message>
</context>
<context>
    <name>PasswordNeededQuery</name>
    <message>
        <location filename="../3rdparty/interface/queries.cpp" line="359"/>
        <source>Encrypted file, please enter the password</source>
        <translation>Зашифрований файл. Будь ласка, введіть пароль</translation>
    </message>
</context>
<context>
    <name>Progress</name>
    <message>
        <location filename="../deepin-compressor/source/src/progress.cpp" line="89"/>
        <location filename="../deepin-compressor/source/src/progress.cpp" line="415"/>
        <source>Cancel</source>
        <translation>Скасувати</translation>
    </message>
    <message>
        <location filename="../deepin-compressor/source/src/progress.cpp" line="85"/>
        <location filename="../deepin-compressor/source/src/progress.cpp" line="148"/>
        <location filename="../deepin-compressor/source/src/progress.cpp" line="150"/>
        <location filename="../deepin-compressor/source/src/progress.cpp" line="152"/>
        <location filename="../deepin-compressor/source/src/progress.cpp" line="154"/>
        <location filename="../deepin-compressor/source/src/progress.cpp" line="156"/>
        <location filename="../deepin-compressor/source/src/progress.cpp" line="159"/>
        <location filename="../deepin-compressor/source/src/progress.cpp" line="442"/>
        <source>Calculating...</source>
        <translation>Обчислення…</translation>
    </message>
    <message>
        <location filename="../deepin-compressor/source/src/progress.cpp" line="93"/>
        <location filename="../deepin-compressor/source/src/progress.cpp" line="191"/>
        <location filename="../deepin-compressor/source/src/progress.cpp" line="468"/>
        <source>Pause</source>
        <translation>Призупинити</translation>
    </message>
    <message>
        <location filename="../deepin-compressor/source/src/progress.cpp" line="152"/>
        <location filename="../deepin-compressor/source/src/progress.cpp" line="275"/>
        <location filename="../deepin-compressor/source/src/progress.cpp" line="277"/>
        <source>Speed</source>
        <comment>delete</comment>
        <translation>Швидкість</translation>
    </message>
    <message>
        <location filename="../deepin-compressor/source/src/progress.cpp" line="156"/>
        <location filename="../deepin-compressor/source/src/progress.cpp" line="299"/>
        <location filename="../deepin-compressor/source/src/progress.cpp" line="301"/>
        <location filename="../deepin-compressor/source/src/progress.cpp" line="303"/>
        <source>Speed</source>
        <comment>convert</comment>
        <translation>Швидкість</translation>
    </message>
    <message>
        <location filename="../deepin-compressor/source/src/progress.cpp" line="403"/>
        <source>Are you sure you want to stop the update?</source>
        <translation>Ви справді хочете зупинити оновлення?</translation>
    </message>
    <message>
        <location filename="../deepin-compressor/source/src/progress.cpp" line="405"/>
        <source>Are you sure you want to stop the conversion?</source>
        <translation>Ви справді хочете зупинити перетворення?</translation>
    </message>
    <message>
        <location filename="../deepin-compressor/source/src/progress.cpp" line="342"/>
        <source>Converting</source>
        <translation>Перетворення</translation>
    </message>
    <message>
        <location filename="../deepin-compressor/source/src/progress.cpp" line="345"/>
        <source>Opening</source>
        <translation>Відкриваємо</translation>
    </message>
    <message>
        <location filename="../deepin-compressor/source/src/progress.cpp" line="401"/>
        <source>Are you sure you want to stop the compression?</source>
        <translation>Ви справді хочете зупинити стискання?</translation>
    </message>
    <message>
        <location filename="../deepin-compressor/source/src/progress.cpp" line="416"/>
        <source>Confirm</source>
        <translation>Підтвердження</translation>
    </message>
    <message>
        <location filename="../deepin-compressor/source/src/progress.cpp" line="462"/>
        <source>Continue</source>
        <translation>Продовжити</translation>
    </message>
    <message>
        <location filename="../deepin-compressor/source/src/progress.cpp" line="411"/>
        <source>Are you sure you want to stop the extraction?</source>
        <translation>Ви справді хочете зупинити стискання?</translation>
    </message>
    <message>
        <location filename="../deepin-compressor/source/src/progress.cpp" line="148"/>
        <location filename="../deepin-compressor/source/src/progress.cpp" line="154"/>
        <location filename="../deepin-compressor/source/src/progress.cpp" line="267"/>
        <location filename="../deepin-compressor/source/src/progress.cpp" line="269"/>
        <location filename="../deepin-compressor/source/src/progress.cpp" line="271"/>
        <location filename="../deepin-compressor/source/src/progress.cpp" line="283"/>
        <location filename="../deepin-compressor/source/src/progress.cpp" line="285"/>
        <location filename="../deepin-compressor/source/src/progress.cpp" line="287"/>
        <source>Speed</source>
        <comment>compress</comment>
        <translation>Швидкість</translation>
    </message>
    <message>
        <location filename="../deepin-compressor/source/src/progress.cpp" line="159"/>
        <location filename="../deepin-compressor/source/src/progress.cpp" line="307"/>
        <source>Time left</source>
        <translation>Лишилося часу</translation>
    </message>
    <message>
        <location filename="../deepin-compressor/source/src/progress.cpp" line="150"/>
        <location filename="../deepin-compressor/source/src/progress.cpp" line="291"/>
        <location filename="../deepin-compressor/source/src/progress.cpp" line="293"/>
        <location filename="../deepin-compressor/source/src/progress.cpp" line="295"/>
        <source>Speed</source>
        <comment>uncompress</comment>
        <translation>Швидкість</translation>
    </message>
    <message>
        <location filename="../deepin-compressor/source/src/progress.cpp" line="338"/>
        <source>Compressing</source>
        <translation>Стискання</translation>
    </message>
    <message>
        <location filename="../deepin-compressor/source/src/progress.cpp" line="340"/>
        <source>Deleting</source>
        <translation>Вилучення</translation>
    </message>
    <message>
        <location filename="../deepin-compressor/source/src/progress.cpp" line="347"/>
        <source>Extracting</source>
        <translation>Видобування</translation>
    </message>
</context>
<context>
    <name>ProgressDialog</name>
    <message>
        <location filename="../deepin-compressor/source/src/progressdialog.cpp" line="50"/>
        <source>%1 task(s) in progress</source>
        <translation>Виконуємо %1 завдання</translation>
    </message>
    <message>
        <location filename="../deepin-compressor/source/src/progressdialog.cpp" line="62"/>
        <location filename="../deepin-compressor/source/src/progressdialog.cpp" line="115"/>
        <source>Task</source>
        <translation>Завдання</translation>
    </message>
    <message>
        <location filename="../deepin-compressor/source/src/progressdialog.cpp" line="63"/>
        <location filename="../deepin-compressor/source/src/progressdialog.cpp" line="121"/>
        <location filename="../deepin-compressor/source/src/progressdialog.cpp" line="140"/>
        <source>Extracting</source>
        <translation>Видобування</translation>
    </message>
    <message>
        <location filename="../deepin-compressor/source/src/progressdialog.cpp" line="136"/>
        <source>Extraction successful</source>
        <translation>Успішне видобування</translation>
    </message>
    <message>
        <location filename="../deepin-compressor/source/src/progressdialog.cpp" line="136"/>
        <source>Extract to</source>
        <translation>Видобути до</translation>
    </message>
    <message>
        <location filename="../deepin-compressor/source/src/progressdialog.cpp" line="141"/>
        <source>Extraction successful</source>
        <comment>progressdialog</comment>
        <translation>Успішне видобування</translation>
    </message>
</context>
<context>
    <name>QInstaller</name>
    <message>
        <location filename="../deepin-compressor/source/src/utils.cpp" line="178"/>
        <source>B</source>
        <translation>Б</translation>
    </message>
    <message>
        <location filename="../deepin-compressor/source/src/utils.cpp" line="179"/>
        <source>KB</source>
        <translation>кБ</translation>
    </message>
    <message>
        <location filename="../deepin-compressor/source/src/utils.cpp" line="180"/>
        <source>MB</source>
        <translation>МБ</translation>
    </message>
    <message>
        <location filename="../deepin-compressor/source/src/utils.cpp" line="181"/>
        <source>GB</source>
        <translation>ГБ</translation>
    </message>
    <message>
        <location filename="../deepin-compressor/source/src/utils.cpp" line="182"/>
        <source>TB</source>
        <translation>ТБ</translation>
    </message>
    <message>
        <location filename="../deepin-compressor/source/src/utils.cpp" line="183"/>
        <source>PB</source>
        <translation>ПБ</translation>
    </message>
    <message>
        <location filename="../deepin-compressor/source/src/utils.cpp" line="184"/>
        <source>EB</source>
        <translation>ЕБ</translation>
    </message>
    <message>
        <location filename="../deepin-compressor/source/src/utils.cpp" line="185"/>
        <source>ZB</source>
        <translation>ЗБ</translation>
    </message>
    <message>
        <location filename="../deepin-compressor/source/src/utils.cpp" line="186"/>
        <source>YB</source>
        <translation>ЙБ</translation>
    </message>
</context>
<context>
    <name>QObject</name>
    <message>
        <location filename="../deepin-compressor/source/src/fileViewer.cpp" line="640"/>
        <source>Name</source>
        <translation>Назва</translation>
    </message>
    <message>
        <location filename="../deepin-compressor/source/src/fileViewer.cpp" line="644"/>
        <source>Time modified</source>
        <translation>Час зміни</translation>
    </message>
    <message>
        <location filename="../deepin-compressor/source/src/fileViewer.cpp" line="648"/>
        <source>Type</source>
        <translation>Тип</translation>
    </message>
    <message>
        <location filename="../deepin-compressor/source/src/fileViewer.cpp" line="652"/>
        <source>Size</source>
        <translation>Розмір</translation>
    </message>
    <message>
        <location filename="../deepin-compressor/source/src/fileViewer.cpp" line="1555"/>
        <source>Update</source>
        <translation>Оновити</translation>
    </message>
    <message>
        <location filename="../deepin-compressor/source/src/settings_translation.cpp" line="5"/>
        <source>General</source>
        <translation>Загальне</translation>
    </message>
    <message>
        <location filename="../deepin-compressor/source/src/settings_translation.cpp" line="6"/>
        <source>Extraction</source>
        <translation>Видобування</translation>
    </message>
    <message>
        <location filename="../deepin-compressor/source/src/settings_translation.cpp" line="7"/>
        <source>Auto create a folder for multiple extracted files</source>
        <translation>Автоматично створювати теку для декількох видобутих файлів</translation>
    </message>
    <message>
        <location filename="../deepin-compressor/source/src/settings_translation.cpp" line="8"/>
        <source>Show extracted files when completed</source>
        <translation>Показати видобуті файли після видобування</translation>
    </message>
    <message>
        <location filename="../deepin-compressor/source/src/settings_translation.cpp" line="9"/>
        <source>File Management</source>
        <translation>Керування файлами</translation>
    </message>
    <message>
        <location filename="../deepin-compressor/source/src/settings_translation.cpp" line="10"/>
        <source>Delete files after compression</source>
        <translation>Вилучити файли після стискання</translation>
    </message>
    <message>
        <location filename="../deepin-compressor/source/src/settings_translation.cpp" line="11"/>
        <source>Files Associated</source>
        <translation>Пов&apos;язані файли</translation>
    </message>
    <message>
        <location filename="../deepin-compressor/source/src/settings_translation.cpp" line="12"/>
        <source>File Type</source>
        <translation>Тип файлів</translation>
    </message>
    <message>
        <location filename="../deepin-compressor/source/src/myfileitem.cpp" line="50"/>
        <location filename="../deepin-compressor/source/src/myfileitem.cpp" line="51"/>
        <location filename="../deepin-compressor/source/src/myfileitem.cpp" line="53"/>
        <location filename="../deepin-compressor/source/src/myfileitem.cpp" line="54"/>
        <location filename="../deepin-compressor/source/src/myfileitem.cpp" line="56"/>
        <location filename="../deepin-compressor/source/src/myfileitem.cpp" line="57"/>
        <location filename="../deepin-compressor/source/src/myfileitem.cpp" line="59"/>
        <location filename="../deepin-compressor/source/src/myfileitem.cpp" line="61"/>
        <source>item(s)</source>
        <translation>записів</translation>
    </message>
    <message>
        <location filename="../deepin-compressor/source/src/mainwindow.cpp" line="243"/>
        <source>Confirm</source>
        <translation>Продовжити стискання</translation>
    </message>
    <message>
        <location filename="../deepin-compressor/source/src/mainwindow.cpp" line="715"/>
        <source>%1 changed. Do you want to save changes to the archive?</source>
        <translation>%1 змінено. Хочете зберегти зміни до архіву?</translation>
    </message>
    <message>
        <location filename="../deepin-compressor/source/src/mainwindow.cpp" line="729"/>
        <source>Discard</source>
        <translation>Відкинути</translation>
    </message>
    <message>
        <location filename="../deepin-compressor/source/src/fileViewer.cpp" line="1554"/>
        <source>Cancel</source>
        <translation>Скасувати</translation>
    </message>
    <message>
        <location filename="../deepin-compressor/source/src/mainwindow.cpp" line="730"/>
        <source>Save</source>
        <translation>Зберегти</translation>
    </message>
    <message>
        <location filename="../3rdparty/interface/queries.cpp" line="188"/>
        <source>Another file with the same name already exists, replace it?</source>
        <translation>Файл вже існує. Що робити?</translation>
    </message>
    <message>
        <location filename="../3rdparty/interface/queries.cpp" line="190"/>
        <source>Skip</source>
        <translation>Пропустити</translation>
    </message>
    <message>
        <location filename="../3rdparty/interface/queries.cpp" line="191"/>
        <source>Replace</source>
        <translation>Замінити</translation>
    </message>
    <message>
        <location filename="../3rdparty/interface/queries.cpp" line="196"/>
        <source>Apply to all</source>
        <translation>Застосувати до всіх</translation>
    </message>
    <message>
        <location filename="../3rdparty/interface/queries.cpp" line="367"/>
        <location filename="../3rdparty/interface/queries.cpp" line="456"/>
        <source>OK</source>
        <translation>Гаразд</translation>
    </message>
    <message>
        <location filename="../3rdparty/interface/queries.cpp" line="454"/>
        <source>Wrong password</source>
        <translation>Помилковий пароль</translation>
    </message>
</context>
<context>
    <name>SettingDialog</name>
    <message>
        <location filename="../deepin-compressor/source/src/settingdialog.cpp" line="133"/>
        <source>Select All</source>
        <translation>Позначити все</translation>
    </message>
    <message>
        <location filename="../deepin-compressor/source/src/settingdialog.cpp" line="172"/>
        <location filename="../deepin-compressor/source/src/settingdialog.cpp" line="201"/>
        <source>Current directory</source>
        <translation>Поточний каталог</translation>
    </message>
    <message>
        <location filename="../deepin-compressor/source/src/settingdialog.cpp" line="134"/>
        <source>Clear All</source>
        <translation>Спорожнити все</translation>
    </message>
    <message>
        <location filename="../deepin-compressor/source/src/settingdialog.cpp" line="135"/>
        <source>Recommended</source>
        <translation>Рекомендовано</translation>
    </message>
    <message>
        <location filename="../deepin-compressor/source/src/settingdialog.cpp" line="166"/>
        <source>Extract archives to</source>
        <translation>Місце видобування архівів</translation>
    </message>
    <message>
        <location filename="../deepin-compressor/source/src/settingdialog.cpp" line="172"/>
        <location filename="../deepin-compressor/source/src/settingdialog.cpp" line="211"/>
        <source>Other directory</source>
        <translation>Інший каталог</translation>
    </message>
    <message>
        <location filename="../deepin-compressor/source/src/settingdialog.cpp" line="172"/>
        <location filename="../deepin-compressor/source/src/settingdialog.cpp" line="206"/>
        <source>Desktop</source>
        <translation>Стільниця</translation>
    </message>
    <message>
        <location filename="../deepin-compressor/source/src/settingdialog.cpp" line="275"/>
        <source>Delete archives after extraction</source>
        <translation>Вилучити архіви після видобування</translation>
    </message>
    <message>
        <location filename="../deepin-compressor/source/src/settingdialog.cpp" line="281"/>
        <location filename="../deepin-compressor/source/src/settingdialog.cpp" line="302"/>
        <source>Never</source>
        <translation>Ніколи</translation>
    </message>
    <message>
        <location filename="../deepin-compressor/source/src/settingdialog.cpp" line="281"/>
        <location filename="../deepin-compressor/source/src/settingdialog.cpp" line="305"/>
        <source>Ask for confirmation</source>
        <translation>Просити підтвердити</translation>
    </message>
    <message>
        <location filename="../deepin-compressor/source/src/settingdialog.cpp" line="281"/>
        <location filename="../deepin-compressor/source/src/settingdialog.cpp" line="308"/>
        <source>Always</source>
        <translation>Завжди</translation>
    </message>
    <message>
        <location filename="../deepin-compressor/source/src/settingdialog.cpp" line="355"/>
        <source>The default extraction path does not exist, please retry</source>
        <translation>Типового шляху для видобування не існує. Будь ласка, повторіть спробу</translation>
    </message>
</context>
<context>
    <name>UnCompressPage</name>
    <message>
        <location filename="../deepin-compressor/source/src/uncompresspage.cpp" line="53"/>
        <source>Extract</source>
        <translation>Видобути</translation>
    </message>
    <message>
        <location filename="../deepin-compressor/source/src/uncompresspage.cpp" line="59"/>
        <location filename="../deepin-compressor/source/src/uncompresspage.cpp" line="150"/>
        <location filename="../deepin-compressor/source/src/uncompresspage.cpp" line="161"/>
        <source>Extract to:</source>
        <translation>Видобути до:</translation>
    </message>
    <message>
        <location filename="../deepin-compressor/source/src/uncompresspage.cpp" line="113"/>
        <source>The default extraction path does not exist, please retry</source>
        <translation>Типового шляху для видобування не існує. Будь ласка, повторіть спробу</translation>
    </message>
    <message>
        <location filename="../deepin-compressor/source/src/uncompresspage.cpp" line="115"/>
        <source>You do not have permission to save files here, please change and retry</source>
        <translation>У вас немає прав доступу для зберігання файлів тут. Будь ласка, змініть каталог і повторіть спробу.</translation>
    </message>
    <message>
        <location filename="../deepin-compressor/source/src/uncompresspage.cpp" line="134"/>
        <source>Find directory</source>
        <translation>Знайти каталог</translation>
    </message>
    <message>
        <location filename="../deepin-compressor/source/src/uncompresspage.cpp" line="187"/>
        <source>OK</source>
        <translation>Гаразд</translation>
    </message>
    <message>
        <location filename="../deepin-compressor/source/src/uncompresspage.cpp" line="561"/>
        <source>Cancel</source>
        <translation>Скасувати</translation>
    </message>
    <message>
        <location filename="../deepin-compressor/source/src/uncompresspage.cpp" line="562"/>
        <source>Convert</source>
        <translation>Перетворити</translation>
    </message>
    <message>
        <location filename="../deepin-compressor/source/src/uncompresspage.cpp" line="572"/>
        <source>Changes to archives in this file type are not supported. Please convert the archive format to save the changes.</source>
        <translation>Підтримки внесення змін до цього типу архівів не передбачено. Будь ласка, змініть тип архіву, щоб зберегти зміни.</translation>
    </message>
    <message>
        <location filename="../deepin-compressor/source/src/uncompresspage.cpp" line="588"/>
        <source>Convert the format to:</source>
        <translation>Змінити формат на:</translation>
    </message>
</context>
<context>
    <name>fileViewer</name>
    <message>
        <location filename="../deepin-compressor/source/src/fileViewer.cpp" line="1739"/>
        <source>Extract</source>
        <translation>Видобути</translation>
    </message>
    <message>
        <location filename="../deepin-compressor/source/src/fileViewer.cpp" line="606"/>
        <location filename="../deepin-compressor/source/src/fileViewer.cpp" line="1747"/>
        <location filename="../deepin-compressor/source/src/fileViewer.cpp" line="1762"/>
        <source>Delete</source>
        <translation>Вилучити</translation>
    </message>
    <message>
        <location filename="../deepin-compressor/source/src/fileViewer.cpp" line="589"/>
        <location filename="../deepin-compressor/source/src/fileViewer.cpp" line="607"/>
        <location filename="../deepin-compressor/source/src/fileViewer.cpp" line="1745"/>
        <location filename="../deepin-compressor/source/src/fileViewer.cpp" line="1758"/>
        <source>Open</source>
        <translation>Відкрити</translation>
    </message>
    <message>
        <location filename="../deepin-compressor/source/src/fileViewer.cpp" line="685"/>
        <source>item(s)</source>
        <translation>записів</translation>
    </message>
    <message>
        <location filename="../deepin-compressor/source/src/fileViewer.cpp" line="707"/>
        <source>yyyy/MM/dd hh:mm:ss</source>
        <translation>dd-MM-yyyy hh:mm:ss</translation>
    </message>
    <message>
        <location filename="../deepin-compressor/source/src/fileViewer.cpp" line="1080"/>
        <location filename="../deepin-compressor/source/src/fileViewer.cpp" line="1346"/>
        <source>Confirm</source>
        <translation>Продовжити стискання</translation>
    </message>
    <message>
        <location filename="../deepin-compressor/source/src/fileViewer.cpp" line="1078"/>
        <location filename="../deepin-compressor/source/src/fileViewer.cpp" line="1345"/>
        <source>Cancel</source>
        <translation>Скасувати</translation>
    </message>
    <message>
        <location filename="../deepin-compressor/source/src/fileViewer.cpp" line="590"/>
        <location filename="../deepin-compressor/source/src/fileViewer.cpp" line="1747"/>
        <source>Delete</source>
        <comment>slotDecompressRowDelete</comment>
        <translation>Вилучити</translation>
    </message>
    <message>
        <location filename="../deepin-compressor/source/src/fileViewer.cpp" line="592"/>
        <location filename="../deepin-compressor/source/src/fileViewer.cpp" line="609"/>
        <source>Open with</source>
        <translation>Відкрити за допомогою</translation>
    </message>
    <message>
        <location filename="../deepin-compressor/source/src/fileViewer.cpp" line="744"/>
        <location filename="../deepin-compressor/source/src/fileViewer.cpp" line="1776"/>
        <location filename="../deepin-compressor/source/src/fileViewer.cpp" line="1796"/>
        <source>Select default program</source>
        <translation>Виберіть типову програму</translation>
    </message>
    <message>
        <location filename="../deepin-compressor/source/src/fileViewer.cpp" line="956"/>
        <location filename="../deepin-compressor/source/src/fileViewer.cpp" line="1749"/>
        <source>Do you want to delete the selected file(s)?</source>
        <translation>Хочете вилучити позначені файли?</translation>
    </message>
    <message>
        <location filename="../deepin-compressor/source/src/fileViewer.cpp" line="1122"/>
        <source>It will permanently delete the file(s). Are you sure you want to continue?</source>
        <translation>У результаті виконання цієї дії буде остаточно вилучено файли. Ви справді хочете її виконати?</translation>
    </message>
    <message>
        <location filename="../deepin-compressor/source/src/fileViewer.cpp" line="1534"/>
        <source>Files have been changed. Do you want to update the changes to %1?</source>
        <translation>Файли змінено. Хочете внести ці зміни до %1?</translation>
    </message>
    <message>
        <location filename="../deepin-compressor/source/src/fileViewer.cpp" line="1082"/>
        <source>Update</source>
        <translation>Оновити</translation>
    </message>
    <message>
        <location filename="../deepin-compressor/source/src/fileViewer.cpp" line="587"/>
        <location filename="../deepin-compressor/source/src/fileViewer.cpp" line="1739"/>
        <source>Extract</source>
        <comment>slotDecompressRowDoubleClicked</comment>
        <translation>Видобуто</translation>
    </message>
    <message>
        <location filename="../deepin-compressor/source/src/fileViewer.cpp" line="588"/>
        <location filename="../deepin-compressor/source/src/fileViewer.cpp" line="1741"/>
        <source>Extract to current directory</source>
        <translation>Видобути до поточного каталогу</translation>
    </message>
</context>
</TS>